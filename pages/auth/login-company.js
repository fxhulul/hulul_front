
import React, { useState } from "react"
import Login from "@/container/auth";
import { Input, InputIcon, InputCountry, InputPhone, InputCheck } from "@/form"
import useTranslation from 'next-translate/useTranslation'
import { Sms, Lock, Eye, EyeSlash } from 'iconsax-react';
import { ButtonTheme, Loading } from "@/ui"
import { Formik } from "formik";
import Link from "next/link"
import * as Yup from "yup";
import { useRouter } from 'next/router'
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import { loginCompany } from "utils/apiHandle"
import { successLogin } from "utils/funcs"

import validate from "utils/validate";

export default function LoginCompany() {
  const [passwordType, setPasswordType] = useState(true)
  const { t, lang } = useTranslation("auth")
  const [loadingButton, setLoadingButton] = useState(false)
  const router = useRouter()
  const { isLoading, user } = useAuth({ middleware: 'guest' })
  const onSubmit = (values) => {
    setLoadingButton(true);
    loginCompany({
      values: values,
      success: () => {
        setLoadingButton(false);
        successLogin(response, lang)
        // router.push("/dashboard");
      },
      error: () => setLoadingButton(false)
    })
  }
  if (isLoading || user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("sign_in")} | {t("common:website_name")}</title>
      </Head>
      <Login slider>
        <span className="block mt-8 mb-2 text-gray-400 text-md">{t('welcom_to_us')}</span>
        <h1 className="mb-8 font-bold leading-none text-h2">{t('welcome_back_again')}</h1>
        <Formik initialValues={{ email: "", password: "", remember: false }} onSubmit={onSubmit} validationSchema={() => Yup.object().shape({
          email: validate.email,
          password: validate.password
        })}>
          {(props) => (
            <form onSubmit={props.handleSubmit}>
              <InputIcon icon={<Sms className="[color:rgb(var(--primary-color))] " />}>
                <Input name="email" type="email" placeholder={t('e_mail')} />
              </InputIcon>
              <InputIcon icon={<Lock className="[color:rgb(var(--primary-color))] " />}>
                <span role="button" className="absolute transform top-4 rtl:left-4 ltr:right-4 rtl:md:left-3 ltr:md:right-3 " onClick={() => setPasswordType(!passwordType)}>
                  {passwordType ? <Eye className="text-black dark:text-white" /> : <EyeSlash className="text-black dark:text-white" />}
                </span>
                <Input name="password" type={passwordType ? "password" : "text"} placeholder={t('password')} dir={lang === "ar" ? "rtl" : "ltr"} className="password" />
              </InputIcon>
              <div className="flex items-center justify-between -mt-4">
                <InputCheck name="remember" text={t('remember_me')} >
                </InputCheck>
                <Link href="/auth/forget-password">
                  <a className="flex flex-row-reverse -mt-2 [color:rgb(var(--primary-color))] " >{t('i_forgot_the_password?')}</a>
                </Link>
              </div>
              <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-6 text-center xs:my-4" loading={loadingButton}>
                {t('sign_in')}
              </ButtonTheme>
            </form>
          )}
        </Formik>
        <ButtonTheme color="primary" outline as="link" href="/auth/register-company" size="xs" block className="mt-6 text-center xs:mt-4" >
          {t('you_do_not_have_an_account_create_an_account')}
        </ButtonTheme>
      </Login>
    </React.Fragment>
  )
}
LoginCompany.getLayout = function PageLayout(page) {
  return <React.Fragment>
    {page}
  </React.Fragment>
}

