import React from 'react';
import Login from "@/container/auth";
import useTranslation from 'next-translate/useTranslation'
import ButtonTheme from "@/ui/ButtonTheme"
import Head from 'next/head'

export default function SuccessEmailConfirm() {
    const { t, lang } = useTranslation("auth")
    return (
        <React.Fragment>
            <Head>
                <title>{t('the_email_was_successfully_confirmed')} | {t("common:website_name")}</title>
            </Head>
            <Login noLinksButton contactUs>
                <h1 className="mb-8  text-4xl md:text-[1.7rem]  text-center block mt-14 text-success">
                    {t('the_email_was_successfully_confirmed')}</h1>
                <ButtonTheme color="primary" as="link" href="/" outline className="block mx-auto my-8  xs:my-8 w-max " size="xs">
                    {t('back_to_the_home_page')}
                </ButtonTheme>
            </Login>
        </React.Fragment>
    )
}
SuccessEmailConfirm.getLayout = function PageLayout(page) {
    return <React.Fragment>
        {page}
    </React.Fragment>
}

