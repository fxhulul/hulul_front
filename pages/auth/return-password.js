
import React, { useState, useEffect } from "react"
import Login from "@/container/auth";
import { Input, InputIcon, InputCountry, InputPhone } from "@/form"
import useTranslation from 'next-translate/useTranslation'
import { Profile, Courthouse, Sms, Lock, Eye, EyeSlash, Flag, Call } from 'iconsax-react';
import ButtonTheme from "@/ui/ButtonTheme"
import { Formik } from "formik";
import * as Yup from "yup";
import { returnPasswordByPhone, returnPasswordByEmail, forgetPasswordByEmail, forgetPasswordByPhone } from "utils/apiHandle"
import { useRouter } from 'next/router'
import Head from 'next/head'
import validate from "utils/validate";
import toast from "react-hot-toast";

export default function ReturnPassword() {
  const { t, lang } = useTranslation("auth")
  const [passwordType, setPasswordType] = useState(true)
  const [loadingButton, setLoadingButton] = useState(false)
  const router = useRouter()
  const [token, setToken] = useState("")
  const [email, setEmail] = useState("");
  useEffect(() => {
    setToken(router.query.token);
    setEmail(router.query.email);

  }, [token, router])
  const onSubmit = (values) => {
    setLoadingButton(true);
    if (router.query.type === "phone") {
      returnPasswordByPhone({
        values: { password: values.password, password_confirmation: values.password_confirmation, phone_number: router.query.phone, verification_code: router.query.verification_code },
        success: () => {
          setLoadingButton(false);
          toast.success(t("errToast:the_word_has_been_successfully_changed"))
          router.push("/auth/register-all");
        },
        error: (err) => {
          setLoadingButton(false);
          if (err.response.status === 500) {
            setTimeout(() => {
              forgetPasswordByPhone({
                success: () => { },
                error: () => { },
                phone: router.query.phone
              })
            }, 2000)
          }
        },
      })
    } else {
      returnPasswordByEmail({
        values: { password: values.password, password_confirmation: values.password_confirmation, token: token, email: email },
        success: () => {
          setLoadingButton(false);
          toast.success(t("errToast:the_word_has_been_successfully_changed"))
          router.push("/auth/register-all");
        },
        error: (err) => {
          setLoadingButton(false);
          if (err.response.status === 500) {
            setTimeout(() => {
              forgetPasswordByEmail({
                values: { email: email },
                success: () => {
                  toast.success(t("errToast:we_have_emailed_your_password_reset_link"))

                },
                error: () => { }
              })
            }, 2000)
          }
        },
      })
    }
  }

  return (
    <React.Fragment>
      <Head>
        <title>{t("reset_a_new_password")} | {t("common:website_name")}</title>
      </Head>
      <Login noLinksButton className="mb-0">
        <h1 className="block mt-0 mb-0 font-bold text-h2">{t('reset_a_new_password')}</h1>
        <span className="block mb-8 text-gray-400 text-md ">{t('make_the_password_consist_of_letters_and_numbers_and_be_easy_to_remember')}</span>
        <Formik initialValues={router.query.type === "phone" ? { password_confirmation: "", password: "" } : { password_confirmation: "", password: "" }} onSubmit={onSubmit} validationSchema={
          () => Yup.object().shape(
            router.query.type === "phone" ? {
              password: validate.password,
              password_confirmation: validate.password_confirmation,
            }
              :
              {
                password: validate.password,
                password_confirmation: validate.password_confirmation,
              }
          )}>
          {(props) => (
            <form onSubmit={props.handleSubmit}>
              <InputIcon icon={<Lock className="[color:rgb(var(--primary-color))] " />}>
                <span role="button" className="absolute transform top-4 rtl:left-4 ltr:right-4 rtl:md:left-3 ltr:md:right-3 " onClick={() => setPasswordType(!passwordType)}>
                  {passwordType ? <Eye className="text-black dark:text-white" /> : <EyeSlash className="text-black dark:text-white" />}
                </span>
                <Input name="password" type={passwordType ? "password" : "text"} placeholder={t('new_password')} dir={lang === "ar" ? "rtl" : "ltr"} className="password" />
              </InputIcon>
              <InputIcon icon={<Lock className="[color:rgb(var(--primary-color))] " />}>
                <Input name="password_confirmation" type="password" placeholder={t('repeat_the_new_password')} dir={lang === "ar" ? "rtl" : "ltr"} className="password" />
              </InputIcon>
              <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-6 text-center xs:my-4" loading={loadingButton}>
                {t('reset')}
              </ButtonTheme>
            </form>
          )}
        </Formik>
      </Login>
    </React.Fragment>
  )
}
ReturnPassword.getLayout = function PageLayout(page) {
  return <React.Fragment>
    {page}
  </React.Fragment>
}


