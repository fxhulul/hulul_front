
import React, { useState } from "react"
import Login from "@/container/auth";
import { Input, InputIcon, InputCountry, InputPhone, InputCheck, InputRecaptcha } from "@/form"
import useTranslation from 'next-translate/useTranslation'
import { Profile, Courthouse, Sms, Lock, Eye, EyeSlash, Flag, Call, Google, Facebook } from 'iconsax-react';
import { ButtonTheme, Loading, LoginFacebookAndGmail } from "@/ui"
import { Formik } from "formik";
import * as Yup from "yup";
import { useRouter } from 'next/router'
import { register, getPhoneCode } from "utils/apiHandle"
import toast from "react-hot-toast";
import Head from 'next/head'
import validate from "utils/validate";
import useAuth from 'hooks/useAuth'
export default function RegisterUser() {
  const { t, lang } = useTranslation("auth")
  const router = useRouter()
  const [passwordType, setPasswordType] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);
  const { isLoading, user } = useAuth({ middleware: 'guest' })

  const onSubmit = (values) => {
    setLoadingButton(true);
    register({
      values: values,
      success: () => {
        setLoadingButton(false);
        getPhoneCode({
          success: () => { router.push(`/auth/enter-phone-code?phone=${values.phone}&verify=true`); },
          error: () => { },
          phone: `${values.phone}`
        })
      },
      error: () => setLoadingButton(false)

    })


  }
  if (isLoading || user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("create_a_account")} | {t("common:website_name")}</title>
      </Head>
      <Login slider>
        <span className="block mt-8 mb-2 text-gray-400 text-md">{t('start_now_for_free')}</span>
        <h1 className="mb-8 font-bold leading-none text-h2">{t('create_your_account_now')}</h1>
        <LoginFacebookAndGmail />

        <Formik initialValues={{ name: "", phone: "", country: "", password: "", agree: false, type: "user", recaptcha: "" }} onSubmit={onSubmit} validationSchema={() => Yup.object().shape({
          name: validate.name,
          phone: validate.phone,
          country: validate.country,
          password: validate.password,
          agree: validate.agree,
          recaptcha: validate.recaptcha
        })}>
          {(props) => (
            <form onSubmit={props.handleSubmit}>
              <InputIcon icon={<Profile className="[color:rgb(var(--primary-color))] " />}>
                <Input name="name" type="text" placeholder={t('full_name')} />
              </InputIcon>
              <InputIcon icon={<Flag className="[color:rgb(var(--primary-color))] " />}>
                <InputCountry name="country" type="text" placeholder={t('residence')} />
              </InputIcon>
              <InputIcon icon={<Call className="[color:rgb(var(--primary-color))] " />}>
                <InputPhone name="phone" type="tel" placeholder={t('phone_number')} />
              </InputIcon>
              <InputIcon icon={<Lock className="[color:rgb(var(--primary-color))] " />}>
                <span role="button" className="absolute transform top-4 rtl:left-4 ltr:right-4 rtl:md:left-3 ltr:md:right-3 " onClick={() => setPasswordType(!passwordType)}>
                  {passwordType ? <Eye className="text-black dark:text-white" /> : <EyeSlash className="text-black dark:text-white" />}
                </span>
                <Input name="password" type={passwordType ? "password" : "text"} placeholder={t('password')} dir={lang === "ar" ? "rtl" : "ltr"} className="password" />
              </InputIcon>
              <InputCheck name="agree" text={<span className="text-xs">{t('by_clicking_on_the_box_i_acknowledge_that_i_read_the_work_agreement_the_privacy_policy_and_the_conditions_of_the_company_and_i_agree_with_it_and_this_is_considered_an_electronic_signature_by_me')}</span>} >
              </InputCheck>
              <InputRecaptcha name="recaptcha" />

              <ButtonTheme color="primary" as="button" type="submit" size="md" block className="mt-3 text-center xs:my-4" loading={loadingButton}>
                {t('create_account')}
              </ButtonTheme>
            </form>
          )}
        </Formik>
        <ButtonTheme color="primary" outline as="link" href="/auth/login-user" block size="xs" className="mt-6 text-center xs:my-4">
          {t('i_have_an_account_log_in')}
        </ButtonTheme>
      </Login>
    </React.Fragment>

  )
}
RegisterUser.getLayout = function PageLayout(page) {
  return <React.Fragment>
    {page}
  </React.Fragment>
}


