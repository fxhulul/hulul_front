import React, { useEffect } from 'react';
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/effect-fade";
import Script from 'next/script';
import { motion } from "framer-motion";
import 'react-phone-input-2/lib/style.css'
import "swiper/css/effect-cards";
import 'styles/globals.css'
import toast, { Toaster, useToasterStore } from "react-hot-toast";
import { SWRConfig } from 'swr'
import axios from "utils/axios";
import Router from 'next/router'
import NProgress from 'nprogress'
import { CustomProvider } from "rsuite";
import Layout from "@/container/Layout"
Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())
NProgress.configure({ showSpinner: true })
import useTranslation from 'next-translate/useTranslation'
import { GlobalContextProvider } from "store/global-context";
import { Provider } from 'react-redux';
import store from "store/index";
const TOAST_LIMIT = 3;

function MyApp({ Component, pageProps }) {
// function MyApp({ Component, pageProps, router }) {
    const { t, lang } = useTranslation("aside");
    const { toasts } = useToasterStore();
    useEffect(() => {
        toasts
            .filter((t) => t.visible) // Only consider visible toasts
            .filter((_, i) => i >= TOAST_LIMIT) // Is toast index over limit
            .forEach((t) => toast.dismiss(t.id)); // Dismiss – Use toast.remove(t.id) removal without animation
    }, [toasts]);
    return (
        <React.Fragment>
            {/* for ANALYTICS */}
            <Script strategy="lazyOnload" src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`} />
            <Script strategy="lazyOnload" id='id'>
                {`
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());
                    gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}', {
                    page_path: window.location.pathname,
                    });
                `}
            </Script>
            {/* for ANALYTICS */}
                    {/* <motion.div
                        key={router.route}
                        initial="initial"
                        animate="animate"
                        variants={{
                            initial: {
                                opacity: 0,
                                scale: 0.95
                            },
                            animate: {
                                opacity: 1,
                                scale: 1
                            },
                        }}
                    > */}
            <Provider store={store}>
                <GlobalContextProvider>
                    <SWRConfig
                        revalidateOnMount={false}
                        revalidateOnFocus={false}
                        revalidateOnReconnect={false}
                        revalidateIfStale={false}
                        refreshInterval={0}
                        value={{
                            fetcher: url => axios.get(url).then(res => res.data.data)
                        }}
                    >

                        <CustomProvider rtl={lang === "ar" ? true : false} >
                            <Toaster position="bottom-right" gutter={15} toastOptions={{
                                custom: {
                                    duration: 10000,
                                },
                                style: {
                                    border: `1px solid rgb(${typeof window !== 'undefined' ? getComputedStyle(document.documentElement).getPropertyValue('--primary-color') : '0,0,0'})`,
                                    padding: '16px',
                                    background: `rgb(${typeof window !== 'undefined' ? getComputedStyle(document.documentElement).getPropertyValue('--dark-white-color') : '0,0,0'})`,
                                    color: `rgb(${typeof window !== 'undefined' ? getComputedStyle(document.documentElement).getPropertyValue('--primary-color') : '0,0,0'})`,
                                },
                            }} />
                                {Component.getLayout ?
                                    <Component {...pageProps} />
                                    :
                                    <Layout>
                                        <Component {...pageProps} />
                                    </Layout>
                                }
                        </CustomProvider>
                    </SWRConfig>
                </GlobalContextProvider>
            </Provider>
                            {/* </motion.div> */}
        </React.Fragment>
    )
}
export default MyApp
