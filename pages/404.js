import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { NotFound404 } from "public/svg"
import Head from 'next/head'
import { ButtonTheme, Loading } from "@/ui"

export default function NotFound() {
  const { t, lang } = useTranslation("common")

  return (
    <React.Fragment>
      <Head>
        <title>{t("page_not_found")} | {t("common:website_name")}</title>
      </Head>
      <div className="text-center h-full min-h-[calc(100vh_-_250px)] flex items-center justify-center">
        <div>
          <NotFound404 className="block mx-auto my-8" />
          <h1 className="text-black font-bold dark:text-white text-3xl mb-4">{t("unfortunately_the_page_does_not_exist")}</h1>
          <p className="text-gray-400">{t("the_page_you_are_trying_to_search_for_is_not_present_or_the_page_course_has_been_changed")}</p>
          <p className="text-gray-400">{t("in_the_event_of_a_problem_of_communicating_with_us")}</p>
          <ButtonTheme color="primary" as="link" href="/dashboard" size="xs" className="mt-20 block w-max mx-auto" >
            {t('homepage')}
          </ButtonTheme>
        </div>
      </div>
    </React.Fragment>
  )
}

