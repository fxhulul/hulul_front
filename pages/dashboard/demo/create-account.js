import React, { useState } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Add, ArrowLeft } from 'iconsax-react';
import Link from "next/link"
import { ButtonTheme, Loading } from "@/ui"
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Lock, Eye, EyeSlash } from 'iconsax-react';
import { Input, InputIcon, CustumnCheckbox, SelectWIthHead, CustomnCheckColors, CustomnBalance } from "@/form"
import { Trider4, Trider5, Correct, CTrider } from "public/svg"
import { useRouter } from 'next/router';
import { createDemoAccount } from "utils/apiHandle"
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import validate from 'utils/validate';
export default function CreateDemo() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const router = useRouter();
  const { t, lang } = useTranslation("dashboard")
  const [loadingButton, setLoadingButton] = useState(false)
  const [data, setData] = useState({
    platform: "",
    currency: "",
    leverage: "",
    Balance: "",
    password: "",
    color: ""
  });
  const [currentStep, setCurrentStep] = useState(0);
  const makeRequest = (formData) => {

    setLoadingButton(true);
    createDemoAccount({
      values: formData,
      success: (response) => { setLoadingButton(false); router.push(`/dashboard/demo/${response.data.account_id}/account-information`); },
      error: () => setLoadingButton(false),
    })

  };
  const handleNextStep = (newData, final = false) => {
    setData((prev) => ({ ...prev, ...newData }));

    if (final) {
      makeRequest(newData);
      return;
    }

    setCurrentStep((prev) => prev + 1);
  };
  const handlePrevStep = (newData) => {
    setData((prev) => ({ ...prev, ...newData }));
    setCurrentStep((prev) => prev - 1);
  };
  const steps = [
    <StepOne next={handleNextStep} data={data} key="1" />,
    <StepTwo next={handleNextStep} prev={handlePrevStep} data={data} key="2" loadingButton={loadingButton} />
  ];
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("create_an_demo_account")} | {t("common:website_name")}</title>
      </Head>
      <div className="p-3 bg-white rounded-lg lg:p-8 sm:p-4 dark:bg-dark-white md:rounded-xl">
        <div className="flex items-center justify-between mb-6">
          <div className="flex items-center gap-2">
            <div className=" icon-container">
              <Add className="w-4 h-4 [color:rgb(var(--primary-color))] -400 lg:w-8 lg:h-8" />
            </div>
            <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white ">{t("create_an_demo_account")}</h1>
          </div>
          {currentStep == 0 ?
            <Link href="/dashboard" >
              <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
              </a>
            </Link>
            :
            <button onClick={() => setCurrentStep(0)} className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl" >
              <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
            </button>
          }
        </div>
        <div className="mx-auto py-4 w-[31.25rem] max-w-full">
          {/* start steps numbers*/}
          <div className={`mb-8 flex justify-center items-center text-center gap-12 text-black dark:text-white relative after:top-[calc(50%-1rem)] after:rtl:right-[calc(50%_+_1rem)] after:ltr:left-[calc(50%_+_0.5rem)] after:w-[5.5rem] after:transform after:rtl:translate-x-1/2 after:ltr:-translate-x-1/2 after:-translate-y-1/2  ${currentStep === 0 ? "after:bg-gray-200" : "after:[background:rgba(var(--primary-color),1)]"} after:h-1 after:absolute`}>
            <button className={`mb-4 z-2 `} onClick={() => setCurrentStep(0)}>
              <span className={`${currentStep === 0 ? "[background:rgba(var(--primary-color),1)]" : "[background:rgba(var(--primary-color),1)]"} [background:rgba(var(--primary-color),1)] rounded-full text-white lg:w-16 w-10  lg:h-16 h-10 lg:text-3xl text-xl font-bold flex justify-center items-center mx-auto`}>
                {currentStep === 0 ? "1" : <Correct />}
              </span>
              {t("basic_information")}
            </button>
            <button className={`mb-4 z-2`} onClick={() => { }}>
              <span className={`${currentStep === 1 ? "[background:rgba(var(--primary-color),1)]" : "[background:rgba(var(--primary-color),1)]"}  rounded-full text-white lg:w-16 w-10  lg:h-16 h-10 lg:text-3xl text-xl  font-bold flex justify-center items-center mx-auto`}>2</span>
              {t("the_deposit")}
            </button>
          </div>
          {/* end steps numbers*/}
          {steps[currentStep]}
        </div>
      </div>
    </React.Fragment>
  )
}

const StepOne = (props) => {
  const { t, lang } = useTranslation("dashboard")
  const [passwordType, setPasswordType] = useState(true)

  const handleSubmit = (values) => {
    props.next(values);
  };
  const stepOneValidationSchema = Yup.object({
    platform: validate.platform,
    currency: validate.currency,
    leverage: validate.leverage,
    password: validate.password,
    color: validate.color
  });
  return (
    <Formik
      validationSchema={stepOneValidationSchema}
      initialValues={props.data}
      onSubmit={handleSubmit}
    >
      {(props) => (
        <Form>
          <h2 className="mb-2 text-lg text-gray-500">{t("platform")}</h2>
          <div className="grid grid-cols-3 items-center justify-between gap-4 mb-4 md:flex-row ">
            <CustumnCheckbox className="w-full" name="platform" value="4" text={<div className="flex items-center gap-2"><span className="text-xs grow w-max sm:block hidden">{t('meta_trader')}4</span><Trider4 /></div>} type="radio" />
            <CustumnCheckbox className="w-full" name="platform" value="5" text={<div className="flex items-center gap-2"><span className="text-xs grow w-max sm:block hidden">{t('meta_trader')}5</span><Trider5 /></div>} type="radio" />
            <CustumnCheckbox className="w-full" name="platform" value="6" text={<div className="flex items-center gap-2"><span className="text-xs grow w-max sm:block hidden">{t('c_trader')}</span><CTrider /></div>} type="radio" />
          </div>
          <ErrorMessage name="platform" component="span" className="text-danger" />

          <SelectWIthHead name="currency" head={t("the_trading_currency")} options="currency" defaultValue={props.values.currency} />
          <SelectWIthHead name="leverage" head={t("current_leverage")} options="leverage" defaultValue={props.values.leverage} />
          <InputIcon icon={<Lock className="[color:rgb(var(--primary-color))] " />}>
            <span role="button" className="absolute transform top-4 rtl:left-4 ltr:right-4 rtl:md:left-3 ltr:md:right-3 " onClick={() => setPasswordType(!passwordType)}>
              {passwordType ? <Eye className="text-black dark:text-white" /> : <EyeSlash className="text-black dark:text-white" />}
            </span>
            <Input name="password" type={passwordType ? "password" : "text"} placeholder={t('password')} dir={lang === "ar" ? "rtl" : "ltr"} className="password" />
          </InputIcon>

          <CustomnCheckColors name="color" />
          <ButtonTheme as='button' type="submit" color="primary" block size="xs" className="my-8"><span className="flex items-center justify-center gap-2">{t("next")} <ArrowLeft className={`text-white ${lang === " ar" ? "" : "transform rotate-180"}`} size="15" /></span></ButtonTheme>

        </Form>
      )}
    </Formik>
  );
};

const StepTwo = (props) => {
  const { t, lang } = useTranslation("dashboard")

  const handleSubmit = (values) => {
    props.next(values, true);
  };
  const stepTwoValidationSchema = Yup.object({
    Balance: validate.Balance
  });
  return (
    <Formik
      validationSchema={stepTwoValidationSchema}
      initialValues={props.data}
      onSubmit={handleSubmit}
    >
      {({ values }) => (
        <Form>
          <h2 className="mb-2 text-lg text-black dark:text-white">{t("the_first_deposit_amount")} <span className="text-gray-400">{t("usd_usd")}</span></h2>
          <CustomnBalance name="Balance" />
          <ButtonTheme as='button' type="submit" color="primary" block size="xs" className="my-8" loading={props.loadingButton} >{t("create_now")}</ButtonTheme>

        </Form>
      )}
    </Formik>
  );
};


