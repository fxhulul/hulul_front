import React from 'react';
import useTranslation from 'next-translate/useTranslation'
import Head from 'next/head'
import { ConvertToContainer } from 'components/container'
import {  userDemoAccountWithoutPagination } from "utils/apiHandle"
import useSWR from 'swr'
import {Loading } from "@/ui"
import useAuth from 'hooks/useAuth'

export default function ConvertTo() {
    const { t, lang } = useTranslation("dashboard")
    const { user, isLoading } = useAuth({ middleware: 'auth' })
    const { data, error } = useSWR(userDemoAccountWithoutPagination())
    if (isLoading || !user || !data) {
        return <Loading page={true} />
    }
    return (
        <React.Fragment>
            <Head>
                <title>{t("convert_between_demo_accounts")} | {t("common:website_name")}</title>
            </Head>
            <ConvertToContainer type='demo' data={data.demo_accounts_Informations} error={error} />
        </React.Fragment>
    )
}