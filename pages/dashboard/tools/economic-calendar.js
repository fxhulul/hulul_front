import React, { useState, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { CalendarSearch, ArrowLeft } from 'iconsax-react';
import Link from "next/link"
import { ButtonTheme, Loading } from "@/ui"
import { Correct } from "public/svg"
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Lock, Eye, EyeSlash } from 'iconsax-react';
import { Input, InputIcon, CustumnCheckbox, SelectWIthHead, CustomnCheckColors, CustomnBalance } from "@/form"
import { Trider4, Trider5 } from "public/svg"
import { useRouter } from 'next/router';
import { createDemoAccount } from "utils/apiHandle"
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import { useEffect } from 'react';
import { set } from 'nprogress';
import globalContext from "store/global-context";

export default function EconomicCalendar() {
    const { user, isLoading } = useAuth({ middleware: 'auth' });
    const ctx = useContext(globalContext);

    const router = useRouter();
    const { t, lang } = useTranslation("dashboard");
    useEffect(() => {

    }, [])
    if (isLoading || !user) {
        return <Loading page={true} />
    }
    return (
        <React.Fragment>
            <Head>
                <title>{t("economic_calendar")} | {t("common:website_name")}</title>
            </Head>
            <div className="p-3 bg-white rounded-lg lg:p-8 sm:p-4 dark:bg-dark-white md:rounded-xl">
                <div className="flex items-center justify-between mb-6">
                    <div className="flex items-center gap-2">
                        <div className=" icon-container">
                            <CalendarSearch className="w-4 h-4 [color:rgb(var(--primary-color))] -400 lg:w-8 lg:h-8" />
                        </div>
                        <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white ">{t("economic_calendar")}</h1>
                    </div>
                    <Link href="/dashboard" >
                        <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                            <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
                        </a>
                    </Link>
                </div>
                <div className="mx-auto py-4 max-w-full  w-full min-h-[calc(100vh_-_1rem)] overflow-auto">
                    <div className='mx-auto  economic-calender min-w-[700px]  w-full overflow-auto bg-gray-200 rounded '>
                        <iframe scrolling="no" frameBorder={0} src={`https://www.tradays.com/${lang}/economic-calendar/widget?mode=2&dateFormat=DMY&theme=${ctx.theme === 'dark' ? '1' : '0'}`} style={{ boxSizing: 'border-box', height: '600px', width: '100%' }} />
                        {/* <iframe scrolling="no" allowTransparency="true" frameBorder={0} src={`https://www.tradingview-widget.com/embed-widget/events/?locale=${lang === 'ar' ? 'ar_AE' : 'en'}#%7B%22colorTheme%22%3A%22light%22%2C%22isTransparent%22%3Atrue%2C%22width%22%3A%22510%22%2C%22height%22%3A%22600%22%2C%22importanceFilter%22%3A%22-1%2C0%2C1%22%2C%22utm_source%22%3A%22hululfx.cf%22%2C%22utm_medium%22%3A%22widget%22%2C%22utm_campaign%22%3A%22events%22%7D`} style={{ boxSizing: 'border-box', height: '600px', width: '100%' }} /> */}
                        {/* <iframe src="https://sslecal2.investing.com?ecoDayBackground=%000000&columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&features=datepicker,timezone&countries=25,34,32,6,37,72,71,22,17,51,39,14,33,10,35,43,60,38,36,110,11,26,9,12,4,5&calType=week&timeZone=15&lang=3" width={650} height={500} frameBorder={0} allowTransparency="true" marginWidth={0} marginHeight={0} className='mx-auto bg-primary' /> */}
                        {/* arabic 3 */}
                        {/* english 3 */}
                    </div>

                    {/* https://uk.investing.com/webmaster-tools/economic-calendar */}
                </div>
            </div>
        </React.Fragment>
    )
}

