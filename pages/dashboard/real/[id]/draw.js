import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { MoneyRecive, ArrowLeft, Information, Wallet3 } from 'iconsax-react';
import Link from "next/link"
import { CardAccountTop, Slider, Slider2, ButtonTheme, FiveSteps, LastActivity, Error, Loading, NoData } from "@/ui"
import { Input } from "@/form"
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import Head from 'next/head'
import { changeRealAccountSetting, userRealAccountWithoutPagination } from "utils/apiHandle"
import useSWR from 'swr'
import { useRouter } from "next/router"
import useAuth from 'hooks/useAuth'
import useWindowSize from "hooks/use-window";
import validate from 'utils/validate';
export default function Draw() {
  const size = useWindowSize();
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("dashboard")
  const handleSubmit = (values) => {

  };
  const router = useRouter();
  const [currentAccount, setCurrentAccount] = useState()
  const [currentId, setCurrentId] = useState()
  const [allAccounts, setAllAccounts] = useState()
  const { data, error } = useSWR(userRealAccountWithoutPagination())
  
  useEffect(() => {
    if (data) {
      setCurrentId(router.query.account ? router.query.account : router.query.id)
      setCurrentAccount(data.demo_accounts_Informations.filter((account) => +account.id === +(router.query.account ? router.query.account : router.query.id))[0] || { error: "there is no account" })
      setAllAccounts(data.demo_accounts_Informations)
    }
  }, [currentId, data, router])

  const handleChooseIndexSlide = (i) => {
    router.push({
      pathname: router.asPath.split("?")[0],
      query: { account: data.real_accounts_Informations[i].id },

    }, undefined, { scroll: false })
    setCurrentAccount(data.real_accounts_Informations[i])
    setCurrentId(data.real_accounts_Informations[i].id)
  }
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("draw_to_the_portfolio")} | {t("common:website_name")}</title>
      </Head>
      <div className="grid lg:grid-cols-7 grid-cols-1  gap-10 ">
        <div className="lg:p-8  sm:p-4 p-3 bg-white dark:bg-dark-white rounded-lg md:rounded-xl col-span-5">
          <div className="flex items-center justify-between mb-6">
            <div className="flex items-center gap-2 ">
              <div className=" icon-container">
                <MoneyRecive className="[color:rgb(var(--primary-color))] -400 lg:w-8 w-4 lg:h-8 h-4" />
              </div>
              <h1 className="block lg:text-3xl text-lg font-bold text-black dark:text-white">{t("draw_to_the_portfolio")}</h1>
            </div>
            <Link href="/dashboard" >
              <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl"><ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
              </a>
            </Link>
          </div>
          <div className="py-4 w-[31.25rem] max-w-full mx-auto">
            <div className="mb-4 ">
              {error ? <Error apiMessage={error} />
                : (!data || !currentAccount) ? <div className="flex justify-center items-center min-h-[calc(100vh_-_20rem)]">< Loading /></div>
                  : currentAccount.error === "there is no account" ? <NoData text={t("sorry_the_account_is_not_present")} />
                    :
                    <React.Fragment>
                      <h2 className="mb-4 text-lg text-gray-500">{t("the_draw_for_calculating")}</h2>
                      <div className=" mb-8 lg:mb-0">
                        {size.width > process.env.md ?
                          <Slider data={allAccounts} currentAccount={currentAccount} type="real" chooseSlide={allAccounts.indexOf(currentAccount)} handleChooseIndexSlide={handleChooseIndexSlide} />
                          :
                          <Slider2 data={allAccounts} currentAccount={currentAccount} type="real" chooseSlide={allAccounts.indexOf(currentAccount)} handleChooseIndexSlide={handleChooseIndexSlide} />
                        }
                      </div>

                      <div className=" ">
                        <h2 className="mb-2 text-lg text-gray-500 ">{t("enter_the_draw_amount")}</h2>
                        <Formik
                          validationSchema={() => Yup.object().shape({
                            draw: validate.draw,
                          })}
                          initialValues={{ draw: "" }}
                          onSubmit={handleSubmit}
                        >
                          {({ values }) => (
                            <Form>
                              <Input name="draw" type="number" />
                              <p className="text-danger mb-4 flex gap-2 items-center"><Information size="15" /> {t("the_balance_in_your_wallet_is_not_enough_to_draw")}</p>
                              <ButtonTheme as='button' color="primary" block size="md" fontSize="md" type="submit" className="mb-10">{t("draw_now")}</ButtonTheme>
                            </Form>
                          )}
                        </Formik>
                        <ButtonTheme color="primary" outline size="xs" fontSize="xs" className="w-max block mx-auto my-4" as="link" href={`/dashboard/draw/choose-method?account=${currentId}`}><span className="mx-6">{t("draw_an_amount_directly")}</span></ButtonTheme>
                      </div>
                    </React.Fragment>
              }
            </div>

          </div>
        </div >
        <div className="col-span-2">
          {/* start wallet */}
          <section className="p-8 mb-6 bg-white dark:bg-dark-white rounded-lg md:rounded-xl">
            <div className="flex items-center gap-2">
              <div className="icon-container">
                <Wallet3 size="18" className="[color:rgb(var(--primary-color))] " />
              </div>
              <h2 className="m-0 text-sm text-gray-500">{t('current_balance')}</h2>
            </div>
            <strong className="block mt-4 mb-6 text-4xl text-black dark:text-white"><bdi>$</bdi>000</strong>
            {true && <ButtonTheme color="primary" block size="xs" className="flex items-center justify-center gap-2 " as="link" href="/dashboard/draw"><MoneyRecive size="25" className="text-white" /><span>{t('draw_a_new_amount')}</span></ButtonTheme>}
          </section>
          {/* end wallet */}
          <FiveSteps />
          <LastActivity />

        </div>
      </div>
    </React.Fragment>
  )
}
