import React, { useState, useEffect , useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Setting4, ArrowLeft, EyeSlash, Eye } from 'iconsax-react';
import { WarningModal, DoneModal, WaitModal } from "@/modals"
import Link from "next/link"
import { ButtonTheme, Slider, Slider2, Error, Loading, NoData } from "@/ui"
import { SelectWIthHead, CustomnCheckColors, InputShow } from "@/form"
import * as Yup from "yup";
import { Formik, Field, ErrorMessage } from "formik";
import { changeRealAccountSetting, userRealAccountWithoutPagination } from "utils/apiHandle"
import useSWR from 'swr'
import Image from "next/image"
import Head from 'next/head'
import { useRouter } from "next/router"
import useAuth from 'hooks/useAuth'
import useWindowSize from "hooks/use-window";
import validate from 'utils/validate';
import globalContext from "store/global-context";



export default function AccountInformation() {
  const size = useWindowSize();
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("dashboard", "auth")
  const [passwordType, setPasswordType] = useState(true)
  const router = useRouter();
  const [changeLeverage, setChangeLeverage] = useState(false)
  const [loadingButton, setLoadingButton] = useState(false)
  const [currentAccount, setCurrentAccount] = useState()
  const [currentId, setCurrentId] = useState()
  const [allAccounts, setAllAccounts] = useState()
  const { data, error } = useSWR(userRealAccountWithoutPagination())
  const [notCompleteModal, setNotCompleteModal] = useState(false)
  const ctx = useContext(globalContext);

  useEffect(() => {
    if (ctx.notComplete) {
      setNotCompleteModal(true)
    }
  }, [ctx])

  useEffect(() => {
    if (data) {
      setCurrentId(router.query.account ? router.query.account : router.query.id)
      setCurrentAccount(data.demo_accounts_Informations.filter((account) => +account.id === +(router.query.account ? router.query.account : router.query.id))[0] || { error: "there is no account" })
      setAllAccounts(data.demo_accounts_Informations)
    }
  }, [currentId, data, router])



  const onSubmit = (values) => {
    if (!ctx.notComplete) {
      setLoadingButton(true);
      changeRealAccountSetting({
        values: values,
        success: (response) => {
          setLoadingButton(false);
          setAllAccounts(allAccounts.map((account) => +account.id === +currentId ? response.data.accountInfo : account))
          if (values.leverage != currentAccount.leverage) {
            setChangeLeverage(true)
          }
        },
        error: () => setLoadingButton(false)
      })
    } else {
      setNotCompleteModal(true)
    }
  }
  const handleChooseIndexSlide = (i) => {
    router.push({
      pathname: router.asPath.split("?")[0],
      query: { account: data.real_accounts_Informations[i].id },

    }, undefined, { scroll: false })
    setCurrentAccount(data.real_accounts_Informations[i])
    setCurrentId(data.real_accounts_Informations[i].id)
  }
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("account_settings_information")} | {t("common:website_name")}</title>
      </Head>
      <div className="lg:p-8  sm:p-4 p-3 bg-white dark:bg-dark-white rounded-lg md:rounded-xl">
        <div className="flex items-center justify-between mb-6">
          <div className="flex items-center gap-2">
            <div className=" icon-container">
              <Setting4 className="[color:rgb(var(--primary-color))] -400 lg:w-8 w-4 lg:h-8 h-4" />
            </div>
            <h1 className="block lg:text-3xl text-lg font-bold text-black dark:text-white">{t("account_settings_information")}</h1>
          </div>
          <Link href="/dashboard" >
            <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl"><ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
            </a>
          </Link>
        </div>
        <div className="max-w-full py-4 mx-auto">
          {(error || ctx.notComplete) ? <Error apiMessage={error} />
            : (!data || !currentAccount) ? <div className="flex justify-center items-center min-h-[calc(100vh_-_20rem)]">< Loading /></div>
              : currentAccount.error === "there is no account" ? <NoData text={t("sorry_the_account_is_not_present")} />
                : <div className="grid lg:grid-cols-7 grid-cols-5 gap-16">
                  <div className="lg:col-span-4 col-span-5 ">
                    <div className=" mb-8 lg:mb-0">
                      {size.width > process.env.md ?
                        <Slider data={allAccounts} currentAccount={currentAccount} type="real" chooseSlide={allAccounts.indexOf(currentAccount)} handleChooseIndexSlide={handleChooseIndexSlide} />
                        :
                        <Slider2 data={allAccounts} currentAccount={currentAccount} type="real" chooseSlide={allAccounts.indexOf(currentAccount)} handleChooseIndexSlide={handleChooseIndexSlide} />
                      }
                    </div>
                    <Formik
                      validationSchema={() => Yup.object().shape({
                        leverage: validate.leverage,
                        password: validate.password

                      })}
                      enableReinitialize
                      initialValues={
                        {

                          leverage: currentAccount.leverage,
                          color: currentAccount.color,
                          account_id: currentId,
                          password: currentAccount.password
                        }
                      }
                      onSubmit={onSubmit}
                    >
                      {(props) => {
                        return (
                          <form onSubmit={props.handleSubmit}>
                            <SelectWIthHead name="leverage" head={t("leverage")} options="leverage" defaultValue={props.values.leverage} />

                            <InputShow title={t("account_number")} clip={currentAccount.login} gray />
                            <div className="flex justify-between p-2  mb-4 bg-secondary dark:bg-dark-secondary  rounded-xl font-bold items-center gap-4 rtl:pr-4 ltr:pl-4 text-black dark:text-white">
                              {t("the_password_of_the_account")}
                              <div className="flex relative grow">
                                <Field type={passwordType ? "password" : "text"} className={`block w-full  p-2 rounded-md bg-white dark:bg-dark-white `} name="password" />
                                <span role="button" className="absolute transform top-2 rtl:left-4 ltr:right-4 rtl:md:left-3 ltr:md:right-3 rtl:border-r  ltr:border-l rtl:pr-2 ltr:pl-2" onClick={() => setPasswordType(!passwordType)}>
                                  {passwordType ? <Eye className="[color:rgb(var(--primary-color))] " /> : <EyeSlash className="[color:rgb(var(--primary-color))] " />}
                                </span>
                              </div>
                            </div>
                            <ErrorMessage name="password" component="span" className="text-danger mb-4 block" />
                            <InputShow title={t("server_name")} clip="hululfx9_demo" gray />
                            <CustomnCheckColors name="color" />

                            <ButtonTheme as='button' color="primary" type="submit" block loading={loadingButton} disabled={!props.dirty} className="p-4">{t("saving_changes")}</ButtonTheme>
                          </form>
                        )
                      }}
                    </Formik>
                  </div>
                  <div className="lg:col-span-3 col-span-5">
                    <h2 className="mb-2 text-lg text-gray-500">{t("the_joint_package_in_it")}</h2>
                    <div className="px-8 py-6 my-4 bg-secondary dark:bg-dark-secondary  rounded-xl">
                      <div className="py-8 flex justify-center gap-2">
                        <div className="flex items-center justify-center w-12 h-12 p-1 rounded-full [background:rgba(var(--primary-color),1)] aspect-square">
                          <Image alt={currentAccount.account_name} src={`${process.env.hostImage}/${currentAccount.account_type_image}`} width="30" height="30" />
                        </div>
                        <h3 className="mt-1 text-3xl text-black dark:text-white capitalize text-center">{currentAccount.account_type_name}<br /><span className="block text-xs text-center text-gray-400 ">{currentAccount.account_type_commission === "yes" ? t("there_is_a_commission") : t("there_is_no_commission")}</span></h3>
                      </div>

                      <ul className="mb-12 rtl:mr-4 ltr:ml-4 text-gray-500">
                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                          <span>{t("starting_from")}</span>
                          <span className="font-bold text-black dark:text-white"><bdi><span>{currentAccount.account_type_pips}</span>&nbsp;pips</bdi></span>
                        </li>
                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                          <span>{t("the_lowest_deposit_amount")}</span>
                          <span className="font-bold text-black dark:text-white">{currentAccount.account_type_min_deposit}</span>
                        </li>
                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                          <span>EA</span>
                          <span className="font-bold text-black dark:text-white">{currentAccount.account_type_EA === "yes" ? t("yes") : t("no")}</span>
                        </li>
                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                          <span>{t("less_trading_volume")}</span>
                          <span className="font-bold text-black dark:text-white">{currentAccount.account_type_Min_trading_volume}</span>
                        </li>
                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                          <span>{t("islamic_account")}</span>
                          <span className="font-bold text-black dark:text-white">{currentAccount.account_type_Islamic_account === "yes" ? t("yes") : t("no")}</span>
                        </li>

                      </ul>
                      <div className={`relative`}>
                        <ButtonTheme color="primary" block size="md" as="link" href={`/dashboard/real/${currentAccount.id}/change-type-account?account_type=${currentAccount.account_type}`}>
                          {t("change_the_account_type")}
                        </ButtonTheme>
                      </div >

                    </div>
                  </div>
                </div>}
        </div>
      </div >
      <WaitModal open={changeLeverage} onClose={() => setChangeLeverage(false)} message={
        <React.Fragment>
          <p className="mb-4 font-bold text-black dark:text-white ">{t("your_request_has_been_registered_to_change_the_leverage")}</p>
          <p className="mb-4 text-gray-500 mx-auto max-w-7/10">{t("the_addict_will_be_approved_in_a_very_short_time_you_will_appear_notice_if_the_customer_succeeds_or_not")}</p>
        </React.Fragment>
      } />
      <WarningModal closeButton={false} backdrop="static" open={notCompleteModal} size="lg" onClose={() => { }} message={
        <React.Fragment>
          <p className="mb-2 text-xl font-bold text-black dark:text-white">{t("dashboard:you_must_complete_your_profile_information")}</p>
          <ButtonTheme as="link" href="/dashboard/profile/personal/profile-personally" color="primary" className="block px-4 py-2 mx-auto my-8 w-max">{t("dashboard:profile")}</ButtonTheme>
        </React.Fragment>
      } />

    </React.Fragment>
  )
}

