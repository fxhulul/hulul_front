import React, { useState, useEffect, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Add, ArrowLeft } from 'iconsax-react';
import Link from "next/link"
import { ButtonTheme, RealTypesAccounts, Loading, NoData } from "@/ui"
import { allAccountsTypes } from "utils/apiHandle"
import useSWR from 'swr'
import { useRouter } from 'next/router';
import { changeRealAccountSetting, userRealAccountWithoutPagination } from "utils/apiHandle"
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import { WarningModal } from "@/modals"
import globalContext from "store/global-context";
import { Formik, Form, Field, ErrorMessage } from "formik";


export default function ChangeTypeAccount() {
    const { user, isLoading } = useAuth({ middleware: 'auth' })
    const router = useRouter();
    const { t, lang } = useTranslation("dashboard")
    const [loadingButton, setLoadingButton] = useState(false)
    const [change, setChange] = useState(1)
    const [currentAccount, setCurrentAccount] = useState([])
    const [notCompleteModal, setNotCompleteModal] = useState(false)
    const [currentId, setCurrentId] = useState(router.query.account ? router.query.account : router.query.id)
    const ctx = useContext(globalContext);

    const { data: accounts, error: errorAccounts } = useSWR(userRealAccountWithoutPagination())
    useEffect(() => {
        if (accounts) {
            setCurrentAccount(accounts.real_accounts_Informations.filter((account) => +account.id === +currentId)[0] || { error: "there is no account" })
        }
    }, [accounts, currentId])
    useEffect(() => {
        if (ctx.notComplete) {
            setNotCompleteModal(true)
        }
    }, [ctx])


    const handleSubmit = (values) => {
        if (!ctx.notComplete) {
            setLoadingButton(true);
            changeRealAccountSetting(
                {
                    values: values,
                    success: () => { setLoadingButton(false); },
                    error: () => setLoadingButton(false)
                })
        } else {
            setNotCompleteModal(true)
        }
    };
    const { data, error } = useSWR(allAccountsTypes())
    if (isLoading || !user || !accounts || !data) {
        return <Loading page={true} />
    }

    return (
        <React.Fragment>
            <Head>
                <title>{t("modify_the_type_of_account")} | {t("common:website_name")}</title>
            </Head>
            <div className="lg:p-8  sm:p-4 p-3 bg-white dark:bg-dark-white rounded-lg md:rounded-xl">
                <div className="flex items-center justify-between mb-6">
                    <div className="flex items-center gap-2 ">
                        <div className=" icon-container">
                            <Add className="[color:rgb(var(--primary-color))] -400 lg:w-8 w-4 lg:h-8 h-4" />
                        </div>
                        <h1 className="block lg:text-3xl text-lg font-bold text-black dark:text-white">{t("modify_the_type_of_account")}</h1>
                    </div>

                    <Link href="/dashboard" >
                        <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                            <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
                        </a>
                    </Link>

                </div>
                {currentAccount.error === "there is no account" ? <NoData text={t("sorry_the_account_is_not_present")} /> : <div className="py-4 ">
                    <Formik
                        initialValues={{ account_id: router.query.id, account_type: router.query.account_type }}
                        onSubmit={handleSubmit}
                        enableReinitialize
                    >
                        {(props) => (
                            <Form>
                                <div className="mx-auto  w-[56.25rem] max-w-full">
                                    <h2 className="mb-0 text-lg text-gray-600 ">{t("account_type")}</h2>
                                    <RealTypesAccounts error={error || errorAccounts || ctx.notComplete} data={data} name='account_type' change={change} setChange={setChange} />
                                    <div className="mx-auto  w-[31.25rem] max-w-full">
                                        <ButtonTheme as='button' type="submit" loading={loadingButton} disabled={change === 1} color="primary" block size="xs" >{t("saving_changes")}</ButtonTheme>
                                    </div>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>}
            </div>
            <WarningModal closeButton={false} backdrop="static" open={notCompleteModal} size="lg" onClose={() => { }} message={
                <React.Fragment>
                    <p className="mb-2 text-xl font-bold text-black dark:text-white">{t("dashboard:you_must_complete_your_profile_information")}</p>
                    <ButtonTheme as="link" href="/dashboard/profile/personal/profile-personally" color="primary" className="block px-4 py-2 mx-auto my-8 w-max">{t("dashboard:profile")}</ButtonTheme>
                </React.Fragment>
            } />
        </React.Fragment>
    )
}

