import React, { useState, useEffect, useRef , useContext} from 'react'
import { Wallet3, MoneyRecive, MoneySend } from 'iconsax-react';
import useTranslation from 'next-translate/useTranslation'
import { CardAccount, FiveSteps, ButtonTheme, LastActivity, CreateAccount, Error, Loading, NoData } from "@/ui"
import useSWR, { useSWRConfig } from 'swr'
import { userDemoAccount, userRealAccount } from "utils/apiHandle"
import { WarningModal } from "@/modals"
import { Pagination } from 'rsuite';
import { useRouter } from 'next/router';
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import globalContext from "store/global-context";

export default function Dashboard() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("dashboard")
  const router = useRouter()
  const { mutate } = useSWRConfig()
  const [allTrue, setAllTrue] = useState(false)
  const perPage = 8;
  const [paginationDemoPage, setPaginationDemoPage] = useState(1)
  const [paginationRealPage, setPaginationRealPage] = useState(1)
  const [tab, setTab] = useState(+router.query.tab)
  const [notCompleteModal, setNotCompleteModal] = useState({ show: false, deposit: false, draw: false, real: false })
  const ctx = useContext(globalContext);
  useEffect(() => {
    if (router) {
      setTab(+router.query.tab === 2 ? 2 : 1)
      setPaginationDemoPage(+router.query.tab === 1 ? +router.query.page : 1)
      setPaginationRealPage(+router.query.tab === 2 ? +router.query.page : 1)
    }
  }, [router])

  useEffect(() => {
    setAllTrue(!ctx.notComplete)
   
  }, [ctx])

  const { data: demo, error: errorDemo } = useSWR(userDemoAccount({ perPage: perPage, page: paginationDemoPage }))
  const { data: real, error: errorReal } = useSWR(userRealAccount({ perPage: perPage, page: paginationRealPage }))
  const handleDemoPagination = (page) => {
    router.push({ query: { page: page, tab: 1 } })
    setPaginationDemoPage(page);
    mutate(userDemoAccount({ perPage: perPage, page: page }))
  }
  const handleRealPagination = (page) => {
    router.push({ query: { page: page, tab: 2 } })
    setPaginationRealPage(page);
    mutate(userRealAccount({ perPage: perPage, page: paginationRealPage }))
  }
  const handleRealFixed = () => {
    mutate(userRealAccount({ perPage: perPage, page: paginationRealPage }))
  }
  const handleDemoFixed = () => {
    mutate(userDemoAccount({ perPage: perPage, page: paginationDemoPage }))
  }
  const handleDeleteDone2 = () => {
    mutate(userDemoAccount({ perPage: perPage, page: paginationDemoPage }))
  }
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("dashboard")} | {t("common:website_name")}</title>
      </Head>
      <div className="grid grid-cols-3 gap-2 lg:gap-6">
        <div className="order-2 col-span-3 lg:col-span-2 lg:order-1">
          <h1 className="hidden">{t("dashboard")}</h1>
          {/* start main */}
          <section className="h-full p-2 bg-white rounded-lg dark:bg-dark-white md:rounded-xl ">
            <div className="relative flex justify-between mb-5 md:mb-10 ">
              <span className={`absolute [background:rgba(var(--primary-color),1)] text-white h-full  md:w-2/5 w-1/2  rounded-xl transition-all duration-150  ease-linear	delay-150  ${tab === 1 ? "rtl:right-0 ltr:left-0" : "ltr:left-full rtl:right-full rtl:transform-[right] ltr:transform-[left] rtl:translate-x-full ltr:-translate-x-full"}`}></span>
              <button className={` rounded-xl md:p-6 p-4 z-1 text-center md:w-2/5 w-1/2 ${tab === 1 ? "text-white dark:text-white/100" : "text-balck dark:text-white/70"}`} onClick={() => { setTab(1); router.push({ query: { page: paginationDemoPage, tab: 1 } }, undefined, { scroll: false }) }}>{t("my_demo_accounts")}</button>
              <button className={` rounded-xl md:p-6 p-4 z-1 text-center md:w-2/5 w-1/2 ${tab === 2 ? "text-white dark:text-white/100" : "text-balck dark:text-white/70"}`} onClick={() => { setTab(2); router.push({ query: { page: paginationRealPage, tab: 2 } }, undefined, { scroll: false }) }}>{t("my_trading_accounts")}</button>
            </div>

            {/* start cards */}
            {tab === 1 ? <React.Fragment>
              <CreateAccount text={t("create_an_demo_account")} href="/dashboard/demo/create-account" type="demo" />
              {/* demo */}
              <div className="grid grid-cols-1 gap-4 mb-10 lg:grid-cols-2 min-h-[calc(100%_-_25rem)]">
                {errorDemo ? <Error apiMessage={errorDemo} /> :
                  !demo ? <Loading className="lg:col-span-2" /> :
                    demo.demo_accounts_Informations.data.length ? demo.demo_accounts_Informations.data.map((data, index) => (
                      <CardAccount type="demo" data={data} key={index} handleDeleteDone={handleDeleteDone2} handelFixed={handleDemoFixed} />
                    ))
                      :
                      <NoData text={t("there_are_no_demo_accounts_yet")} account={true} />
                }
              </div>
              {(demo && !errorDemo && (demo.demo_accounts_Informations.total > perPage)) && <Pagination
                prev
                next
                maxButtons={4}
                size="lg"
                total={+demo.demo_accounts_Informations.total}
                ellipsis={true}
                activePage={paginationDemoPage}
                limit={perPage}
                onChangePage={handleDemoPagination}
              />}
            </React.Fragment>
              :
              <React.Fragment>
                <CreateAccount text={t("create_a_trading_account")} href="/dashboard/real/create-account" type="real" handleOpenModal={() => setNotCompleteModal({ show: true, deposit: false, draw: false, real: true })} allTrue={allTrue} />
                {/* real */}
                <div className="grid grid-cols-1 gap-4 mb-10 lg:grid-cols-2 min-h-[calc(100%_-_25rem)]">
                  {errorReal ? <Error apiMessage={errorReal} /> :
                    !real ? <Loading className="lg:col-span-2" /> :
                      real.real_accounts_Informations.data.length ? real.real_accounts_Informations.data.map((data, index) => (
                        <CardAccount type="real" data={data} key={index} handleDeleteDone={handleDeleteDone2} handelFixed={handleRealFixed} />
                      ))
                        :
                        <NoData text={t("there_are_no_trading_accounts_yet")} account={true} />
                  }
                </div>
                {(real && !errorReal && (real.real_accounts_Informations.total > perPage)) && <Pagination
                  prev
                  next
                  maxButtons={4}
                  size="lg"
                  total={+real.real_accounts_Informations.total}
                  ellipsis={true}
                  activePage={paginationRealPage}
                  limit={perPage}
                  onChangePage={handleRealPagination}
                />}

              </React.Fragment>
            }
            {/* end cards */}
          </section>
          {/* end main */}
        </div >
        <div className="order-1 col-span-3 lg:col-span-1 lg:order-1 ">
          {/* start wallet */}
          <section className="p-6 mb-6 bg-white rounded-lg md:p-8 dark:bg-dark-white md:rounded-xl" >
            <div className="flex items-center gap-2">
              <div className="icon-container">
                <Wallet3 size="18" className="[color:rgb(var(--primary-color))] " />
              </div>
              <h2 className="m-0 text-sm text-gray-500">{t('current_balance')}</h2>
            </div>
            <strong className="block mt-4 mb-6 text-4xl text-black dark:text-white"><bdi>$</bdi>00</strong>
            <ButtonTheme as='button' color="primary" block size="xs" className="flex items-center justify-center gap-2 mb-4 text-xs-important" onClick={() => { allTrue ? router.push("/dashboard/deposit") : setNotCompleteModal({ show: true, deposit: true, draw: false, real: false }) }}><MoneyRecive size="25" className="text-inherit" /><span>{t('deposit_a_new_amount')}</span></ButtonTheme>
            <ButtonTheme as='button' color="primary" outline block size="xs" className="flex items-center justify-center gap-2 text-xs-important " onClick={() => { allTrue ? router.push("/dashboard/draw") : setNotCompleteModal({ show: true, deposit: false, draw: true, real: false }) }}><MoneySend size="25" className="text-inherit" /><span>{t('draw')}</span></ButtonTheme>
          </section>
          {/* end wallet */}
          <div >
            <FiveSteps />
          </div>
          <LastActivity />
        </div>
      </div >
      <WarningModal open={notCompleteModal.show} size="lg" onClose={() => setNotCompleteModal({ show: false, deposit: false, draw: false, real: false })} message={
        <React.Fragment>
          <p className="mb-2 text-xl font-bold text-black dark:text-white">
            {notCompleteModal.real && t("excuse_me_you_cannot_create_a_real_account")}
            {notCompleteModal.deposit && t("excuse_me_you_cannot_deposit_a_new_amount")}
            {notCompleteModal.draw && t("excuse_me_you_cannot_draw_a_new_amount")}
          </p>
          <p className="mb-2 text-xl font-bold text-black dark:text-white">{t("you_must_complete_your_profile_information")}</p>
          <ButtonTheme as="link" href="/dashboard/profile/personal/profile-personally" color="primary" className="block px-4 py-2 mx-auto my-8 w-max">{t("profile")}</ButtonTheme>
        </React.Fragment>
      } />
    </React.Fragment>
  )
}


