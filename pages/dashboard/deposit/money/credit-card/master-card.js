import React from 'react'
import CreditCardDepositDrawId from "@/container/deposit-and-draw/money/credit-card/CreditCardDepositDrawId"
export default function CreditCardDepositMaster() {
    return (
       <CreditCardDepositDrawId type="deposit"/>
    )
}
