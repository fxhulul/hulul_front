import React from 'react'
import ElectronicDepositDraw from "@/container/deposit-and-draw/money/electronic-portfolio/ElectronicDepositDraw"
export default function NetellerDeposit() {
    return (
       <ElectronicDepositDraw type="deposit" electronic="neteller"/>
    )
}
