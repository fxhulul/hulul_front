import React from 'react'
import DepositDrawMoney from "@/container/deposit-and-draw/DepositDrawMoney"
export default function DepositMoney() {
    return (
        <DepositDrawMoney type="deposit" />
    )
}