import React, { useState } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { Location, ClipboardText, Lock1, Call } from 'iconsax-react'
import { Formik } from "formik";
import { UploadImage, InputIcon, Input, InputCountry, InputDate, SelectWIthHead } from "@/form"
import { ButtonTheme, Error, Loading } from "@/ui"
import * as Yup from "yup";
import useSWR from 'swr'
import { profileBankAccount, bankAccountDetails } from "utils/apiHandle"
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import validate from 'utils/validate';
import toast from "react-hot-toast";

export default function DetailsOfTheBankAccount() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("profile")
  const [loadingButton, setLoadingButton] = useState(false);
  const { data, error } = useSWR(bankAccountDetails())

  const onSubmitForm = (values) => {
    setLoadingButton(true);
    profileBankAccount({
      values: values,
      success: () => {
        setLoadingButton(false);
        toast.success(t("common:the_data_has_been_successfully_saved"))

      },
      error: () => setLoadingButton(false)
    })
  }
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("details_of_the_bank_account")} | {t("common:website_name")}</title>
      </Head>
      <ProfileContainer tab={"bankAccount"}>
        <div className="w-[43.75rem] max-w-full mx-auto ">
          <h2 className="mt-8 mb-2 lg:text-2xl text-sm font-bold text-center text-black dark:text-white">{t("we_need_you_to_fill_the_details_of_your_bank_account")}</h2>
          <span className="block text-center text-gray-400">{t("do_not_worry_information_that_will_be_safe_with_us_no_one_but_you_will_see_it")}</span>
          {error ? <Error apiMessage={error} />
            : !data ? <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit"><Loading /></div>
              :<Formik initialValues={{
                Beneficiary_Name: data.Beneficiary_Name, Bank_name: data.Bank_name, Bank_account_number: data.Bank_account_number, SWIFTnumber: data.SWIFTnumber, bank_country: data.bank_country, street: data.street, Neighborhood: data.Neighborhood,
              }} validationSchema={() => Yup.object().shape({
                Beneficiary_Name: validate.Beneficiary_Name,
                Bank_name: validate.Bank_name,
                Bank_account_number: validate.Bank_account_number,
                SWIFTnumber: validate.SWIFTnumber,
                bank_country: validate.bank_country,
                street: validate.street,
                Neighborhood: validate.Neighborhood,

              })} onSubmit={onSubmitForm}>
                {(props) => {
                  return (
                    <form onSubmit={props.handleSubmit}>
                      <div className="grid grid-cols-2 mt-8 lg:gap-x-6 gap-4">
                        <Input name="Beneficiary_Name" type="text" placeholder={t('beneficiary_name')} />
                        <Input name="Bank_name" type="text" placeholder={t('bank_name')} />
                        <Input name="Bank_account_number" type="number" placeholder={t('bank_account_number')} />
                        <Input name="SWIFTnumber" type="number" placeholder={t('swift_digit')} />
                        <InputIcon icon={<Location className="[color:rgb(var(--primary-color))] " />} className="col-span-2">
                          <InputCountry name="bank_country" type="text" placeholder={t('the_bank_is_the_bank')} defaultValue={props.values.bank_country} />
                        </InputIcon>
                        <Input name="street" type="text" placeholder={t('street')} />
                        <Input name="Neighborhood" type="text" placeholder={t('neighborhood')} />
                      </div>
                      <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-8 text-center xs:my-4" loading={loadingButton} disabled={!props.dirty}>
                        {t('save')}
                      </ButtonTheme>
                    </form>
                  )
                }}
              </Formik>}
        </div>
      </ProfileContainer>
    </React.Fragment>
  )
}
