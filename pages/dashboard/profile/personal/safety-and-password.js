import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { UploadImage, InputIcon, Input, InputCountry, InputDate, SelectWIthHead, InputPhone } from "@/form"
import { ButtonTheme, Error, Loading } from "@/ui"
import { DoneModal } from "@/modals"
import { Formik } from "formik";
import { Sms, Lock1, Eye, EyeSlash, Profile, Star1, Location, Courthouse, MedalStar, Call, Whatsapp, CallAdd } from 'iconsax-react';
import { profilePersonalProfileChangePass } from "utils/apiHandle"
import * as Yup from "yup";
import Link from "next/link"
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import validate from 'utils/validate'

export default function SafetyAndPassword() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("profile");
  const [loadingButton, setLoadingButton] = useState(false)
  const [openSuccessChange, setOpenSuccessChange] = useState(false)
  const [passwordType, setPasswordType] = useState(true)
  const [role, setRole] = useState()
  useEffect(() => {
    if (typeof window !== "undefined") {
      setRole(localStorage.userType);
    }
  }, [role]);
  const onSubmit = (values) => {
    setLoadingButton(true);
    profilePersonalProfileChangePass({
      values: values,
      success: () => {
        setLoadingButton(false);
        setOpenSuccessChange(true);

      },
      error: () => { setLoadingButton(false); }
    })
  }
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("safety_and_password")} | {t("common:website_name")}</title>
      </Head>
      <ProfileContainer tab={"personal"} >
        <div className="w-[31.25rem] max-w-full mx-auto">
          <Formik initialValues={
            { current_password: "", new_password: "", new_password_confirmation: "" }
          }
            validationSchema={() => Yup.object().shape({
              current_password: validate.password,
              new_password: validate.new_password,
              new_password_confirmation: validate.new_password_confirmation,
            })}
            onSubmit={onSubmit} >
            {(props) => {
              return (
                <form onSubmit={props.handleSubmit}>
                  <InputIcon icon={<Lock1 className="[color:rgb(var(--primary-color))] " />} className=" mt-8">
                    <Input name="current_password" type="password" placeholder={t('old_password')} dir={lang === "ar" ? "rtl" : "ltr"} />
                  </InputIcon>
                  <Link href="/auth/forget-password">
                    <a className="flex flex-row-reverse -mt-2 [color:rgb(var(--primary-color))]mb-4" >{t('i_forgot_the_password')}</a>
                  </Link>
                  <div className="mt-6">
                    <InputIcon icon={<Lock1 className="[color:rgb(var(--primary-color))] " />}>
                      <span role="button" className="absolute transform top-4 rtl:left-4 ltr:right-4 rtl:md:left-3 ltr:md:right-3 " onClick={() => setPasswordType(!passwordType)}>
                        {passwordType ? <Eye className="text-black dark:text-white" /> : <EyeSlash className="text-black dark:text-white" />}
                      </span>
                      <Input name="new_password" type={passwordType ? "password" : "text"} placeholder={t('new_password')} dir={lang === "ar" ? "rtl" : "ltr"} />
                    </InputIcon>
                  </div>
                  <InputIcon icon={<Lock1 className="[color:rgb(var(--primary-color))] " />}>
                    <Input name="new_password_confirmation" type="password" placeholder={t('repeat_the_new_password')} dir={lang === "ar" ? "rtl" : "ltr"} />
                  </InputIcon>

                  <ButtonTheme color="primary" as="button" type="submit" size="md" bLock1 className="my-12 text-center xs:my-4" loading={loadingButton} block disabled={!props.dirty}>
                    {t('save')}
                  </ButtonTheme>
                </form>
              )
            }}
          </Formik>
        </div>
      </ProfileContainer>
      <DoneModal open={openSuccessChange} onClose={() => setOpenSuccessChange(false)} message={<p className="font-bold">{t("the_password_has_been_successfully_changed")}</p>} />
    </React.Fragment>
  )
}
