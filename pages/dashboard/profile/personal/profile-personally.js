import React, { useState, useEffect, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { UploadImage, InputIcon, Input, InputCountry, InputDate2, InputDate, SelectWIthHead } from "@/form"
import { ButtonTheme, Error, Loading } from "@/ui"
import { Formik } from "formik";
import { Profile, Star1, Location, Courthouse, MedalStar } from 'iconsax-react';
import {
  profilePersonalProfileUserPersonly, profilePersonalProfileCompanyPersonly, userPersonalProfile, companyPersonalProfile
} from "utils/apiHandle"
import Head from 'next/head'
import useSWR from 'swr'
import globalContext from "store/global-context";
import useAuth from 'hooks/useAuth'
import * as Yup from "yup";
import validate from "utils/validate";
import toast from "react-hot-toast";
import { getUserData } from 'store/userDataSlice';
import { useDispatch } from 'react-redux';

export default function ProfilePersonly() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const ctx = useContext(globalContext);
  const dispatch = useDispatch();
  const { t, lang } = useTranslation("profile");
  const [role, setRole] = useState()
  useEffect(() => {
    if (typeof window !== "undefined") {
      setRole(localStorage.userType);
    }
  }, [role]);
  const [loadingButton, setLoadingButton] = useState(false)
  const onSubmitUser = (values) => {
    setLoadingButton(true);
    profilePersonalProfileUserPersonly({
      values: values,
      success: () => {
        setLoadingButton(false);
        dispatch(getUserData());
        toast.success(t("common:the_data_has_been_successfully_saved"))

      },
      error: () => setLoadingButton(false)
    })
  }
  const onSubmitCompany = (values) => {
    setLoadingButton(true);
    profilePersonalProfileCompanyPersonly({
      values: values,
      success: () => {
        setLoadingButton(false);
        dispatch(getUserData());
        toast.success(t("common:the_data_has_been_successfully_saved"))
      },
      error: () => setLoadingButton(false)
    })
  }
  // const { data, error } = useSWR(role === "user" ? userPersonalProfile() : companyPersonalProfile())
  const { data, error } = useSWR(userPersonalProfile())

  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("profile_personly")} | {t("common:website_name")}</title>
      </Head>
      <ProfileContainer tab={"personal"} >
        <div className="w-[31.25rem] max-w-full mx-auto min-h-inherit">
          {error ? <Error apiMessage={error} />
            : !data ? <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit"><Loading /></div>
              : role === "user" ?
                <Formik initialValues={
                  { user_img: "", full_name: data.data.full_name ? data.data.full_name : ctx.userInfo.name, Birth_date: data.data.Birth_date, Birth_location: data.data.Birth_location }}
                  onSubmit={onSubmitUser}
                  validationSchema={() => Yup.object().shape({
                    full_name: validate.full_name,
                    Birth_date: validate.Birth_date,
                    Birth_location: validate.Birth_location,
                    user_img: !(data && data.data.user_img) && validate.user_img,
                  })}

                >
                  {(props) => {
                    return (
                      <form onSubmit={props.handleSubmit}>
                        <UploadImage name="user_img" defaultImg={data && data.data.user_img} />
                        <InputIcon icon={<Profile className="[color:rgb(var(--primary-color))] " />}>
                          <Input name="full_name" type="text" placeholder={t('full_name')} />
                        </InputIcon>
                        <InputIcon icon={<Star1 className="[color:rgb(var(--primary-color))] " />}>
                          <InputDate2 name="Birth_date" type="text" placeholder={t('date_of_birth')} defaultValue={(data && data.data.Birth_date) && new Date(data.data.Birth_date)} />
                        </InputIcon>
                        <InputIcon icon={<Location className="[color:rgb(var(--primary-color))] " />}>
                          <InputCountry name="Birth_location" type="text" placeholder={t('nationality')} defaultValue={props.values.Birth_location} />
                        </InputIcon>
                        <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-12 text-center xs:my-4" loading={loadingButton} disabled={!props.dirty}>
                          {t('save')}
                        </ButtonTheme>
                      </form>
                    )
                  }}
                </Formik>
                :
                <Formik initialValues={{}

                  // data.data.company_info ? { company_img: "", company_name: data.data.company_info.company_name, representative_name: data.data.company_info.representative_name, representative_position: data.data.company_info.representative_position, Created_date: data.data.company_info.Created_date } :
                  // { company_img: "", company_name: "", representative_name: "", representative_position: "", Created_date: "" }
                } onSubmit={onSubmitCompany}>
                  {(props) => {
                    return (
                      <form onSubmit={props.handleSubmit}>
                        <UploadImage name="company_img" defaultImg={data.data.company_info && data.data.company_info.company_img} />
                        <InputIcon icon={<Courthouse className="[color:rgb(var(--primary-color))] " />}>
                          <Input name="company_name" type="text" placeholder={t('the_company_name')} />
                        </InputIcon>
                        <InputIcon icon={<Profile className="[color:rgb(var(--primary-color))] " />}>
                          <Input name="representative_name" type="text" placeholder={t('the_name_of_the_company_representative')} />
                        </InputIcon>
                        <SelectWIthHead name="representative_position" head={<MedalStar className="[color:rgb(var(--primary-color))] " />} options="representative_position" defaultValue={props.values.representative_position} />
                        <InputIcon icon={<Location className="[color:rgb(var(--primary-color))] " />}>
                          <InputDate name="Created_date" type="text" placeholder={t('the_date_of_the_establishment_of_the_company')} defaultValue={(data.data.company_info && data.data.company_info.Created_date) && new Date(data.data.company_info.Created_date)} />
                        </InputIcon>
                        <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-12 text-center xs:my-4" loading={loadingButton} disabled={!props.dirty}>
                          {t('save')}
                        </ButtonTheme>
                      </form>
                    )
                  }}
                </Formik>
          }
        </div>
      </ProfileContainer>
    </React.Fragment>
  )
}
