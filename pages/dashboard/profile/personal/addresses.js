import React, { useState, useEffect, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { UploadImage, InputIcon, Input, InputCountry, InputDate, SelectWIthHead } from "@/form"
import { ButtonTheme, Error, Loading } from "@/ui"
import { Formik } from "formik";
import { Sms, Lock, Eye, EyeSlash, Profile, Star1, Location, Courthouse, MedalStar } from 'iconsax-react';
import { profilePersonalProfileUserAddresses, profilePersonalProfileCompanyAddresses, profilePersonalProfileCompanyPersonly, userPersonalProfile, companyPersonalProfile } from "utils/apiHandle"
import Head from 'next/head'
import globalContext from "store/global-context";
import useSWR from 'swr'
import useAuth from 'hooks/useAuth'
import * as Yup from "yup";
import validate from "utils/validate";
import toast from "react-hot-toast";
import { getUserData } from 'store/userDataSlice';
import { useDispatch } from 'react-redux';

export default function Addresses() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const dispatch = useDispatch();
  const { t, lang } = useTranslation("profile");
  const ctx = useContext(globalContext);
  const [role, setRole] = useState()
  useEffect(() => {
    if (typeof window !== "undefined") {
      setRole(localStorage.userType);
    }
  }, [role]);
  const [loadingButton, setLoadingButton] = useState(false)
  const onSubmitUser = (values) => {
    setLoadingButton(true);
    profilePersonalProfileUserAddresses({
      values: values,
      success: () => {
        setLoadingButton(false);
        dispatch(getUserData());
        toast.success(t("common:the_data_has_been_successfully_saved"))
      },
      error: () => setLoadingButton(false)
    })
  }
  const onSubmitCompany = (values) => {
    setLoadingButton(true);
    profilePersonalProfileCompanyAddresses({
      values: values,
      success: () => {
        setLoadingButton(false);
        dispatch(getUserData());
        toast.success(t("common:the_data_has_been_successfully_saved"))


      },
      error: () => setLoadingButton(false)
    })
  }
  // const { data, error } = useSWR(role === "user" ? userPersonalProfile() : companyPersonalProfile())
  const { data, error } = useSWR(userPersonalProfile())
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <React.Fragment>
      <Head>
        <title>{t("addresses")} | {t("common:website_name")}</title>
      </Head>
      <ProfileContainer tab={"personal"} >
        <div className="w-[31.25rem] max-w-full mx-auto min-h-inherit">
          {error ? <Error apiMessage={error} />
            : !data ? <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit"><Loading /></div>
              : <Formik initialValues={
                role === "user" ?
                  { citizenship: data.data.citizenship ? data.data.citizenship : ctx.userInfo.country ? ctx.userInfo.country : '', city: data.data.city ? data.data.city : '', state: data.data.state ? data.data.state : '', adders: data.data.adders ? data.data.adders : '', zip_code: data.data.zip_code ? data.data.zip_code : '' }
                  :
                  // !todo change in company
                  // { citizenship: data.data.citizenship ? data.data.citizenship : ctx.userInfo.country, city: data.data.city, state: data.data.state, adders: data.data.adders, zip_code: data.data.zip_code }
                  {}
              }
                validationSchema={() => Yup.object().shape({
                  citizenship: validate.citizenship,
                  city: validate.city,
                  state: validate.state,
                  adders: validate.adders,
                  zip_code: validate.zip_code,
                })}
                onSubmit={role === "user" ? onSubmitUser : onSubmitCompany}>
                {(props) => {
                  return (
                    <form onSubmit={props.handleSubmit}>
                      <InputIcon icon={<Location className="[color:rgb(var(--primary-color))] " />} className="mb-12 mt-8">
                        <InputCountry name="citizenship" type="text" placeholder={t('residence')} defaultValue={props.values.citizenship} />
                      </InputIcon>
                      <div className="grid-cols-2 grid gap-4">
                        <Input name="city" type="text" placeholder={t('city')} />
                        <Input name="state" type="text" placeholder={t('neighborhood')} />
                        <Input name="adders" type="text" placeholder={t('street_number')} />
                        <Input name="zip_code" type="text" placeholder={t('postal_code')} />
                      </div>
                      <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-12 text-center xs:my-4" loading={loadingButton} disabled={!props.dirty}>
                        {t('save')}
                      </ButtonTheme>
                    </form>
                  )
                }}
              </Formik>}
        </div>
      </ProfileContainer>
    </React.Fragment>
  )
}
