import React, { useState, useEffect, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { UploadImage, InputIcon, Input, InputCountry, InputDate, SelectWIthHead, InputPhone } from "@/form"
import { ButtonTheme, Error, Loading } from "@/ui"
import { Formik } from "formik";
import { Sms, Lock, Eye, EyeSlash, Profile, Star1, Location, Courthouse, MedalStar, Call, Whatsapp, CallAdd } from 'iconsax-react';
import { profilePersonalUserContactInformation, profilePersonalCompanyContactInformationprofilePersonalProfileCompanyPersonly, userPersonalProfile, companyPersonalProfile } from "utils/apiHandle"
import useSWR from 'swr'
import {  getEmailCode, getPhoneCode } from "utils/apiHandle"
import Head from 'next/head'
import globalContext from "store/global-context";
import useAuth from 'hooks/useAuth'
import { useRouter } from 'next/router'
import { Loader } from 'rsuite';
import * as Yup from "yup";
import validate from "utils/validate";
import toast from "react-hot-toast";
import { getUserData } from 'store/userDataSlice';
import { useDispatch } from 'react-redux';

export default function ContactInformation() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("profile");
  const dispatch = useDispatch();
  const ctx = useContext(globalContext);
  const [loadingButton, setLoadingButton] = useState(false)
  const [sendEmail, setSendEmail] = useState(false)
  const [sendPhone, setSendPhone] = useState(false)
  const [role, setRole] = useState()
  const router = useRouter()
  useEffect(() => {
    if (typeof window !== "undefined") {
      setRole(localStorage.userType);
    }
  }, [role]);
  const onSubmitUser = (values) => {
    setLoadingButton(true);
    profilePersonalUserContactInformation({
      values: values,
      success: () => {
        setLoadingButton(false);
        dispatch(getUserData());
        toast.success(t("common:the_data_has_been_successfully_saved"))

      },
      error: () => setLoadingButton(false)
    })
  }
  const onSubmitCompany = (values) => {
    setLoadingButton(true);
    profilePersonalCompanyContactInformation({
      values: values,
      success: () => {
        setLoadingButton(false);
        dispatch(getUserData());
        toast.success(t("common:the_data_has_been_successfully_saved"))

      },
      error: () => setLoadingButton(false)
    })
  }
  // const { data, error } = useSWR(role === "user" ? userPersonalProfile() : companyPersonalProfile())
  const { data, error } = useSWR(userPersonalProfile())
  if (isLoading || !user) {
    return <Loading page={true} />
  }

  return (
    <React.Fragment>
      <Head>
        <title>{t("contact_information")} | {t("common:website_name")}</title>
      </Head>
      <ProfileContainer tab={"personal"} >
        <div className="w-[31.25rem] max-w-full mx-auto min-h-inherit">
          {error ? <Error apiMessage={error} />
            : !data ? <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit"><Loading /></div>
              : <Formik
                enableReinitialize
                initialValues=
                {role === "user" ?
                  {
                    email: data.data.email ? data.data.email : ctx.userInfo.email_field ? ctx.userInfo.email_field : '',
                    phone: data.data.phone ? data.data.phone : ctx.userInfo.phone_field ? ctx.userInfo.phone_field : '',
                    whatsapp_number: data.data.whatsapp_number,
                    second_phone: data.data.second_phone
                  } :
                  // {
                  //   email: data.data.company_info.email ? data.data.company_info.email : ctx.userInfo.email_field,
                  //   phone: data.data.company_info.phone ? data.data.company_info.phone : ctx.userInfo.phone_field,
                  //   whatsapp_number: data.data.company_info.whatsapp_number,
                  //   second_phone: data.data.company_info.second_phone
                  // }

                  {}

                }
                validationSchema={() => Yup.object().shape({
                  email: validate.email,
                  phone: validate.phone,
                  whatsapp_number: validate.whatsapp_number,
                  second_phone: validate.second_phone,
                })}
                onSubmit={role === "user" ? onSubmitUser : onSubmitCompany} >
                {(props) => {
                  return (
                    <form onSubmit={props.handleSubmit}>
                      <InputIcon icon={<Sms className="[color:rgb(var(--primary-color))] " />} className="mb-4 mt-8">
                        <Input name="email" type="email" placeholder={t('email')} />
                      </InputIcon>
                      <div className="flex items-center justify-between mb-12">
                        {(props.values.email && !ctx.userInfo.email) &&
                          <span className={`flex flex-row-reverse -mt-2 gap-4 cursor-pointer ${sendEmail ? "text-gray-400 events-none" : "[color:rgb(var(--primary-color))]"}`}
                            onClick={() => {
                              setSendEmail(true)
                              getEmailCode({
                                success: () => { router.push(`/auth/email-confirm?email=${props.values.email}`) },
                                error: () => { setSendEmail(false) },
                                email: `${props.values.email}`
                              })
                            }}>
                            <span>{t("email_confirmation")}</span>
                            <span>{sendEmail && <Loader size="xs" />}</span>
                          </span>}
                      </div>
                      <InputIcon icon={<Call className="[color:rgb(var(--primary-color))] " />}>
                        <InputPhone name="phone" type="tel" placeholder={t('phone_number')} defaultValue={props.values.phone} />
                      </InputIcon>
                      <div className="flex items-center justify-between mb-6">
                        {(props.values.phone && !ctx.userInfo.phone) && <span
                          className={`flex flex-row-reverse -mt-2 gap-4  cursor-pointer ${sendPhone ? "text-gray-400 events-none" : "[color:rgb(var(--primary-color))]"}`}
                          onClick={
                            () => {
                              setSendPhone(true)
                              getPhoneCode({
                                success: () => { router.push(`/auth/enter-phone-code?phone=${props.values.phone}&verify=true`) },
                                error: () => { setSendPhone(false) },
                                phone: `${props.values.phone}`
                              })
                            }
                          }
                        >
                          <span>
                            {t("confirm_the_phone_number")}
                          </span>
                          <span>
                            {sendPhone && <Loader size="xs" />}
                          </span>

                        </span>}
                      </div>

                      <InputIcon icon={<Whatsapp className="[color:rgb(var(--primary-color))] " />}>
                        <InputPhone name="whatsapp_number" type="tel" placeholder={t('whats_app_number')} defaultValue={props.values.whatsapp_number} />
                      </InputIcon>
                      <InputIcon icon={<CallAdd className="[color:rgb(var(--primary-color))] " />}>
                        <InputPhone name="second_phone" type="tel" placeholder={t('add_another_number')} defaultValue={props.values.second_phone} />
                      </InputIcon>

                      <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-12 text-center xs:my-4" loading={loadingButton} disabled={!props.dirty}>
                        {t('save')}
                      </ButtonTheme>
                    </form>
                  )
                }}
              </Formik>}
        </div>
      </ProfileContainer >
    </React.Fragment>
  )
}
