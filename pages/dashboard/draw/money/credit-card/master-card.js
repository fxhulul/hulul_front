import React from 'react'
import CreditCardDepositDrawId from "@/container/deposit-and-draw/money/credit-card/CreditCardDepositDrawId"
export default function CreditCardDrawMaster() {
    return (
       <CreditCardDepositDrawId type="draw"/>
    )
}
