import React from 'react'
import ElectronicDepositDraw from "@/container/deposit-and-draw/money/electronic-portfolio/ElectronicDepositDraw"
export default function SkrillDraw() {
    return (
       <ElectronicDepositDraw type="draw" electronic="skrill"/>
    )
}
