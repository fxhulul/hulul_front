import React from 'react'
import ElectronicDepositDraw from "@/container/deposit-and-draw/money/electronic-portfolio/ElectronicDepositDraw"
export default function NetellerDraw() {
    return (
       <ElectronicDepositDraw type="draw" electronic="neteller"/>
    )
}
