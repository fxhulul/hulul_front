import React from 'react'
import BrokerDepositDraw from "@/container/deposit-and-draw/money/other/BrokerDepositDraw"
export default function BrokerDraw() {
    return (
       <BrokerDepositDraw type="draw" />
    )
}
