import React from 'react'
import DepositDrawChooseMethod from "@/container/deposit-and-draw/DepositDrawChooseMethod"
export default function ChooseDepositMethod() {
    return (
       <DepositDrawChooseMethod type="deposit"/>
    )
}
