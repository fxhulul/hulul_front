import React, { useState, useEffect, useRef } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Refresh2, SearchNormal1, ImportCurve, Calendar, Refresh, More } from 'iconsax-react';
import { RecordCard, CustomDateRangePicker, ButtonTheme, Error, Loading } from "@/ui"
import { InputIcon } from "@/form"
import { Table, Whisper, Pagination, IconButton, Tooltip, Dropdown } from 'rsuite';
import useSWR, { useSWRConfig } from 'swr'
import { recordClosedDeals, filterCloseDeals } from "utils/apiHandle"
import { AccountCell, ProfitCell, CustomCell, TimeCell, OperationNumberCell, TypeCell, PriceCell, ActionColumn, CellPriceAndTime } from "@/table"
import { useRouter } from 'next/router'
import Head from 'next/head'
import useAuth from 'hooks/useAuth'
import { optionAction, ExportCSV, closedDeals, getData, iconPrint, remTopx } from "@/record"
import { useReactToPrint } from 'react-to-print';
import useWindowSize from "hooks/use-window";
import jsPDF from 'jspdf';
import "jspdf-autotable";
import "public/fonts/Amiri-normal";

export default function CloseDeals() {
  const size = useWindowSize();
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("record")
  const { mutate } = useSWRConfig()
  const router = useRouter()
  const [dataClose, setDataClose] = useState([]);
  const [date, setDate] = useState("");
  const [dateRange, setDateRange] = useState('');
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [sortData, setSortData] = useState([]);
  const [search, setSearch,] = useState("");
  const [lastLastData, setLastLastData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchKeys, setSearchKeys] = useState({ text: "", action: "All", account: "All" });
  var paginationPage = 1;
  const [paginationPage2, setPaginationPage2] = useState(1);
  const perPage = 8;
  const { data, error } = useSWR(recordClosedDeals("All"))
  const componentRef = useRef();
  useEffect(() => {
    setSearchKeys({ text: "", action: "All", account: router.query.login ? router.query.login : "All" })
  }, [router])
  const cards = [
    { title: t("current_balance"), number: data && data.Trading_volume.toFixed(2) + '$', class: "[background:rgba(var(--primary-color),1)] text-white border [border-color:rgba(var(--primary-color),1)]" },
    { title: t("the_total_profit"), number: data && data.ProfitSum.toFixed(2) + '$', class: "bg-[#4FB853] text-white border border-[#4FB853]" },
    { title: t("the_total_loss"), number: data && data.totalLoss.toFixed(2) + '$', class: "bg-[#B84F4F] text-white border border-[#B84F4F]" },
    { title: t("net_profit_and_loss"), number: data && data.netProfitAndLoss.toFixed(2) + '$', class: "bg-white dark:bg-dark-white [color:rgb(var(--primary-color))] border [border-color:rgba(var(--primary-color),1)]" }
  ];


  const lastLastDataForExcel = (data) => {
    let dataExcel = [];
    for (var i = 0; i < data.length; i++) {
      dataExcel.push({
        Login: data[i].Login,
        PositionID: data[i].PositionID,
        Action: data[i].Action === 0 ? t("sale") : t("buy"),
        Symbol: data[i].Symbol,
        Time: new Date(data[i].Time * 1000).toLocaleTimeString(['en-US'], {
          year: "numeric",
          month: "2-digit",
          day: "2-digit",
          hour: "2-digit",
          minute: "2-digit"
        })
        ,
        PricePosition: data[i].PricePosition,
        Price: data[i].Price,
        Profit: data[i].Profit,
      })
    }
    return dataExcel
  }

  const handleSearchChange = (e) => {
    setSearch(e.target.value);
    handleSearchData(e.target.value)
  }

  useEffect(() => {
    if (searchKeys && data) {
      let searchObjs = searchFunc();
      setSortData(searchObjs);
      handlePagination(searchObjs, paginationPage)
    }
  }, [searchKeys, data, dataClose])
  useEffect(() => {
    if (data) {
      setDataClose(data.allCloseTrades)
    }
  }, [data])

  const handleSearchData = (text) => {
    setSearchKeys({ text: text.trim() ? text.trim() : " ", action: searchKeys.action, account: searchKeys.account })
  }
  const searchFunc = () => {
    // text and account and action
    let searchObjs = [];
    for (var i = 0; i < dataClose.length; i++) {
      if (
        (searchKeys.text.trim() ?
          (dataClose[i].Symbol.toUpperCase().includes(searchKeys.text.toUpperCase()))
          ||
          (dataClose[i].PositionID.toString().includes(searchKeys.text) || `#${dataClose[i].PositionID}`.toString().includes(`${searchKeys.text}`))
          ||
          (dataClose[i].Login.toString().includes(searchKeys.text))
          :
          true
        )
        &&
        (
          searchKeys.action ?
            (searchKeys.action == "sale" && dataClose[i].Action == 0) || (searchKeys.action == "buy" && dataClose[i].Action == 1) || (searchKeys.action == "All")
            :
            true
        )
        &&
        (searchKeys.account != "All" ?
          (dataClose[i].Login.toString() == searchKeys.account)
          :
          true
        )

      ) {
        searchObjs.push(dataClose[i])
      }
    }
    return searchObjs
  }

  const handlePaginationTable = (page) => {
    handlePagination(sortData, page)
  }

  const handlePagination = (data, page) => {
    // router.push({ query: { page: page } }, undefined, { scroll: false })
    setLoading(true);
    setTimeout(() => {
      setLastLastData(data.slice(+page == 1 ? 0 : +perPage * (+page - 1), +perPage * +page), page)
      setLoading(false);
    }, 500);
  }


  const handleSortColumn = (sortColumn, sortType) => {
    setSortColumn(sortColumn);
    setSortType(sortType);
    handlePagination(getData(sortColumn, sortType, sortData), paginationPage);
  };

  const handleRefreash = () => {
    setLoading(true)
    mutate(recordClosedDeals("All"))
  }

  const handleActionFilter = (sortType, sortColumn, filter) => {
    handleSortColumn('Action', sortType);
    if (!sortType) {
      if (filter) {
        setSearchKeys({ text: searchKeys.text, action: filter, account: searchKeys.account })
      }
    }
  }

  const handleAccountFilter = (sortType, sortColumn, filter) => {
    handleSortColumn('Login', sortType);
    if (filter && !sortType) {
      setSearchKeys({ text: searchKeys.text, action: searchKeys.action, account: filter })
    }
  }



  const optionAccounts = () => {
    let accounts = [{
      "label": t("all"),
      "value": "All",
    }]
    let dataLoop = [...new Set(data.accountInfo)]
    for (let i = 0; i < dataLoop.length; i++) {
      accounts.push({
        "label": dataLoop[i].login,
        "value": dataLoop[i].login,
      })
    }

    return accounts
  }
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
    documentTitle: `${t("closed_deals")}.pdf`,
    copyStyles: true,
  });
  const exportPDF = () => {
    const unit = "pt";
    const orientation = "portrait"; // portrait or landscape
    const doc = new jsPDF(orientation, unit, "A4");

    doc.setFontSize(15);
    const headers = document.dir === 'rtl' ? [Object.values(Heading[0]).reverse()] : [Object.values(Heading[0])];
    const data = document.dir === 'rtl' ? lastLastDataForExcel(lastLastData).map(row => Object.values(row).reverse()) : lastLastDataForExcel(lastLastData).map(row => Object.values(row).reverse());
    let content = {
      startY: 50,
      head: headers,
      body: data,
      headStyles: {
        fillColor: `rgb(${typeof window !== 'undefined' ? getComputedStyle(document.documentElement).getPropertyValue('--primary-color'):'0,0,0'} )`, textColor: '#fff', fontStyle: "Amiri", font: "Amiri", overflow: 'visible'
      },
      styles: { fillColor: 'rgb(200,200,200)', textColor: '#ffff', fontStyle: "Amiri", font: "Amiri", overflow: 'visible', halign: `${document.dir === 'rtl' ? 'right' : 'left'}` },
    };
    doc.autoTable(content);
    doc.save(`${t("closed_deals")}.pdf`)
  }

  // for search
  const onSubmit = (value1, value2) => {
    setLoading(true);
    filterCloseDeals({
      date: value1 && value1,
      dateRange: value2 && value2,
      success: (res) => {
        setLoading(false);
        setDataClose(res.data['Close_deals '])
        // change data here
      },
      error: (err) => { setLoading(false); }
    })
  }
  const Heading = [
    {
      Login: t("the_account"),
      PositionID: t("operation_number"),
      Action: t("the_type"),
      Symbol: t("symbol"),
      Time: t("the_opening"),
      PricePosition: t("the_opening_price"),
      Price: t("the_closure_price"),
      Profit: t("profit"),

    }
  ];
  const dayOptions = [
    { value: "", label: t("filter_according_to") },
    { value: "today", label: t("the_today") },
    { value: "week", label: t("the_week") },
    { value: "month", label: t("the_month") },
    { value: "year", label: t("the_year") },
  ]

  if (error) return <Error apiMessage={error} />
  if (isLoading || !user || !data) {
    return <Loading page={true} />
  }

  return (
    <React.Fragment>
      <Head>
        <title>{t("record")} - {t("closed_deals")}  | {t("common:website_name")} </title>
      </Head>
      <div className="p-3 overflow-hidden bg-white rounded-lg lg:p-8 sm:p-4 dark:bg-dark-white md:rounded-xl">
        <div className="flex items-center justify-between h-full">
          <div className="flex items-center h-full gap-2 mb-8 ">
            <div className=" icon-container">
              <Refresh2 className="w-4 h-4 [color:rgb(var(--primary-color))] -400 lg:w-8 lg:h-8" />
            </div>
            <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white">{t("record")}</h1>
          </div>

        </div>
        {/* start top cards */}
        <div className="grid grid-cols-2 gap-4 mb-8 lg:grid-cols-4 lg:gap-6">
          {cards.map((card, index) => (
            <RecordCard key={index} card={card} />
          ))}
        </div>
        {/* end top cards */}
        {/* start search */}

        <div>
          <div className="grid items-center grid-cols-4 gap-4 mb-8 lg:flex">
            <div className="col-span-4  grow lg:col-span-2 ">
              <div className="">
                <InputIcon icon={
                  <Whisper followCursor speaker={<Tooltip>{t("you_can_search_for_the_account_the_operation_number_or_the_type_of_currency")}</Tooltip>}>
                    <SearchNormal1 className="text-gray-400 rounded-lg " />
                  </Whisper>
                }>
                  <input type="search" placeholder={t('search')} value={search} onChange={handleSearchChange} className={`block w-full  px-4 py-4  rounded-md bg-secondary dark:bg-dark-secondary  focus:outline-0 `} />
                </InputIcon>
              </div>
            </div>
            <div className="flex  lg:flex-row flex-col  lg:col-span-2 items-center gap-4 col-span-4 ">
              <div className="lg:w-[16.5625rem] w-full px-[0.625rem] py-[0.3125rem] bg-secondary dark:bg-dark-secondary  rounded-md">
                <select name="date" className="lg:w-[15.625rem] w-full block px-4 py-4  rounded-md  bg-secondary dark:bg-dark-secondary focus:outline-0 "
                  value={date}
                  onChange={(e) => { setDate(e.target.value); onSubmit(e.target.value, false); setDateRange("") }} >
                  {dayOptions.map((option, index) => (
                    <option key={index} value={option.value}>{option.label}</option>
                  ))}
                </select>
              </div>
              <CustomDateRangePicker value={dateRange} onChange={(value1, value2) => { onSubmit(false, value2); setDate(""); setDateRange(value1) }} className="w-full flex justify-between" />
            </div>
            <div className="flex lg:col-span-2 gap-2 col-span-4 ">
              <ButtonTheme color="primary" as="button" size="xs" className="p-4-important " onClick={handleRefreash}>
                <Refresh className="w-[1.875rem] h-[1.875rem]" />
              </ButtonTheme>
              <Dropdown renderToggle={iconPrint} className="text-center">
                <Dropdown.Item>
                  <ExportCSV
                    csvData={lastLastDataForExcel(lastLastData)}
                    fileName={t("closed_deals")}
                    wscols={closedDeals.wscols(lastLastData)}
                    Heading={Heading}
                    header={closedDeals.header}
                  />
                </Dropdown.Item>
                <Dropdown.Item onClick={exportPDF}>pdf</Dropdown.Item>
                <Dropdown.Item onClick={handlePrint} > {t("print")} </Dropdown.Item>
              </Dropdown>
            </div>
          </div>
        </div>
        {/* end search */}
        <div className='overflow-auto w-full'>
          <Table
            height={500}
            locale={{ loading: t("loading"), emptyMessage: t("there_are_no_data") }}
            data={lastLastData}
            fillHeight={true}
            sortColumn={sortColumn}
            sortType={sortType}
            onSortColumn={handleSortColumn}
            loading={loading}
            rowClassName="rounded-xl py-4"
            rowHeight={100}
            hover={false}
            onRowClick={data => {
            }}

          // renderEmpty={"d"}
          >

            <Table.Column width={130} align="center" >
              <ActionColumn label={Heading[0].Login} onfilter={handleAccountFilter} column="account" accountInfo={data.accountInfo} options={optionAccounts()}></ActionColumn>
              <AccountCell dataKey="Login" accountInfo={data.accountInfo} />
            </Table.Column>

            <Table.Column width={130} sortable >
              <Table.HeaderCell>{Heading[0].PositionID}</Table.HeaderCell>
              <OperationNumberCell className="[color:rgb(var(--primary-color))] " dataKey="PositionID" />
            </Table.Column>

            <Table.Column width={100}  >
              <ActionColumn label={Heading[0].Action} onfilter={handleActionFilter} column="action" options={optionAction}></ActionColumn>
              <TypeCell className="[color:rgb(var(--primary-color))] " dataKey="Action" />
            </Table.Column>

            <Table.Column width={100} sortable align="center">
              <Table.HeaderCell>{Heading[0].Symbol}</Table.HeaderCell>
              <CustomCell dataKey="Symbol" className="justify-center" />
            </Table.Column>
            {/* <Table.Column width={remTopx(12, size)} sortable align="center">
            <Table.HeaderCell>{t("the_opening_price")}</Table.HeaderCell>
            <CellPriceAndTime dataKey="PricePosition" className="justify-center" />
          </Table.Column> */}
            <Table.Column width={100} sortable align="center" flexGrow={1} minWidth={150}>
              <Table.HeaderCell>{Heading[0].Time}</Table.HeaderCell>
              <TimeCell dataKey="Time" className="justify-center" />
            </Table.Column>
            <Table.Column width={130} sortable align="center">
              <Table.HeaderCell>{Heading[0].PricePosition}</Table.HeaderCell>
              <CustomCell dataKey="PricePosition" className="justify-center" />
            </Table.Column>

            <Table.Column width={130} sortable align="center">
              <Table.HeaderCell>{Heading[0].Price}</Table.HeaderCell>
              <CustomCell dataKey="Price" className="justify-center" />
            </Table.Column>

            <Table.Column width={100} sortable >
              <Table.HeaderCell>{Heading[0].Profit}</Table.HeaderCell>
              <ProfitCell dataKey="Profit" />
            </Table.Column>
          </Table>
        </div>

        {(data && sortData) && sortData.length > perPage && <Pagination
          className="py-6"
          prev
          next
          maxButtons={4}
          size={size.width > process.env.lg ? "lg" : "sm"}
          total={sortData.length}
          ellipsis={true}
          activePage={paginationPage2}
          limit={perPage}
          onChangePage={(page) => { handlePaginationTable(page); setPaginationPage2(page) }}
        />}
      </div >
      <div className="w-full text-black opacity-0 hidden"><ComponentToPrint ref={componentRef} data={lastLastData} header={Heading} dir={lang === "ar" ? "rtl" : "ltr"} t={t} /></div>

    </React.Fragment>
  )
}

export class ComponentToPrint extends React.PureComponent {
  render() {
    return (
      <div className="h-full m-4 print-table">
        <table className={` ${this.props.dir} overflow-scroll lg:text-base text-xs`}>
          <thead>
          <tr>
            <th>{this.props.header[0].Login}</th>
            <th>{this.props.header[0].PositionID}</th>
            <th>{this.props.header[0].Action}</th>
            <th>{this.props.header[0].Symbol}</th>
            <th>{this.props.header[0].Time}</th>
            <th>{this.props.header[0].PricePosition}</th>
            <th>{this.props.header[0].Price}</th>
            <th>{this.props.header[0].Profit}</th>
          </tr>
          </thead>
          <tbody>
          {this.props.data.map((column, index) => (
            <tr key={index}>
              <td>{column.Login}</td>
              <td>{column.PositionID}</td>
              <td>{column.Action === 0 ? this.props.t("sale") : this.props.t("buy")}</td>
              <td>{column.Symbol}</td>
              <td>{new Date(column.Time * 1000).toLocaleTimeString(['en-US'], {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit"
              })}</td>
              <td>{column.PricePosition}</td>
              <td>{column.Price}</td>
              <td>{column.Profit > "0" ? " + " : column.Profit === "0" ? "" : " - "}{Math.abs(column.Profit)}</td>
            </tr>
          ))}
          </tbody>
          {/* <br /> */}
          <tfoot>
          <tr >
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}
