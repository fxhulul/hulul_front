import useSWR from 'swr'
import axios from "utils/axios";
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

export default function useAuth({ middleware } = {}) {
    const router = useRouter()
    const [isLoading, setIsLoading] = useState(true);
    useEffect(() => {
        if (user || error) {
            setIsLoading(false);
        }

        if (middleware == 'guest' && user) router.push('/dashboard')
        if (middleware == 'auth' && !user && error) router.push('/auth/register-all')
    })

    const { data: user, error, mutate } = useSWR('/get-user-basic-information',
        () => axios.get('/get-user-basic-information').then(response => response.data)
    )

    async function logout ({success,error}) {
        
        await axios.post('/logout')
        .then(res=>{
            mutate(null)
            localStorage.setItem("userType", "");
            localStorage.setItem("token", "");
            window.location.href = `/${router.locale}/auth/register-all`
            
            success()
        })
        .catch(err=>{
            error()
        })

    }

    return {
        user,
        logout,
        isLoading
    }
}
