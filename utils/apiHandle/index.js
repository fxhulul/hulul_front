import axios from "utils/axios";
import toast from "react-hot-toast";
import Trans from 'next-translate/Trans'

export const sendRequest = (url, values, success, error) => {
    axios.post(url, values)
        .then(function (response) {
            success(response.data);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
                if (err.message === "Network Error") {
                    toast.error(<Trans i18nKey="errToast:sorry_there_is_a_problem_with_connecting_to_the_internet" />)
                } else {
                    if(err.response.data.message){
                        const message = typeof err.response.data.message === 'string' ? err.response.data.message : Object.values(err.response.data.message)[0]
                        toast.error(
                            message
                        )
                    }else{
                         toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
                    }
                }
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
}
export const putRequest = (url, values, success, error) => {
    axios.put(url, values)
        .then(function (response) {
            success(response.data);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
                if (err.message === "Network Error") {
                    toast.error(<Trans i18nKey="errToast:sorry_there_is_a_problem_with_connecting_to_the_internet" />)
                } else {
                    if(err.response.data.message){
                        const message = typeof err.response.data.message === 'string' ? err.response.data.message : Object.values(err.response.data.message)[0]
                        toast.error(
                            message
                        )
                    }else{
                         toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
                    }
                }
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
}
export const getRequest = (url, success, error, params) => {
    axios.get(url, { params: params })
        .then(function (response) {
            success(response.data);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
                if (err.message === "Network Error") {
                    toast.error(<Trans i18nKey="errToast:sorry_there_is_a_problem_with_connecting_to_the_internet" />)
                } else {
                    if(err.response.data.message){
                        const message = typeof err.response.data.message === 'string' ? err.response.data.message : Object.values(err.response.data.message)[0]
                        toast.error(
                            message
                        )
                    }else{
                         toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
                    }
                }
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
        
}
export const deleteRequest = (url, success, error) => {
    axios.delete(url)
        .then(function (response) {
            success(response.data);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
                if (err.message === "Network Error") {
                    toast.error(<Trans i18nKey="errToast:sorry_there_is_a_problem_with_connecting_to_the_internet" />)
                } else {
                    if(err.response.data.message){
                        const message = typeof err.response.data.message === 'string' ? err.response.data.message : Object.values(err.response.data.message)[0]
                        toast.error(
                            message
                        )
                    }else{
                         toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
                    }
                }
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
}
// /////////////////////////////////////////////
// post 
// ////////////////////////////////////////////
export const register = ({ values, success, error }) => sendRequest(`/register`, values, res => success(res), err => error(err))
export const loginUser = ({ values, success, error }) => sendRequest(`/user-login`, values, res => success(res), (err) => error(err))
export const forgetPasswordByEmail = ({ values, success, error }) => sendRequest(`/password/forgot-password`, values, res => success(res), (err) => error(err))
export const returnPasswordByEmail = ({ values, success, error }) => sendRequest(`/password/reset`, values, res => success(res), (err) => error(err))
export const returnPasswordByPhone = ({ values, success, error }) => sendRequest(`/phone/reset/password`, values, res => success(res), (err) => error(err))
export const enterCodeNumber = ({ values, success, error }) => sendRequest(`/phoneVerifyCode`, values, res => success(res), (err) => error(err))
// ??done
export const createDemoAccount = ({ values, success, error }) => sendRequest(`/create-demo`, values, res => success(res), (err) => error(err))
export const profilePersonalProfileUserPersonly = ({ values, success, error }) => sendRequest(`/store-user-Personal-Profile-Info`, values, res => success(res), (err) => error(err))
export const profilePersonalUserContactInformation = ({ values, success, error }) => sendRequest(`/store-user-store-Contact-Information`, values, res => success(res), (err) => error(err))
export const profilePersonalProfileUserAddresses = ({ values, success, error }) => sendRequest(`/store-user-store-Addresses-Information`, values, res => success(res), (err) => error(err))
export const profilePersonalProfileChangePass = ({ values, success, error }) => sendRequest(`/change-user-password`, values, res => success(res), (err) => error(err))
export const profilePersonalIdentificationConfirmation = ({ values, success, error }) => sendRequest(`/store-Documents`, values, res => success(res), (err) => error(err))
export const profilePersonalFinancialInformation = ({ values, success, error }) => sendRequest(`/store-user-financial-profile-info`, values, res => success(res), (err) => error(err))
export const profileBankAccount = ({ values, success, error }) => sendRequest(`/store-Bank-account-details`, values, res => success(res), (err) => error(err))
export const changeRealAccountSetting = ({ values, success, error }) => sendRequest(`/Edit-Real-account`, values, res => success(res), (err) => error(err))
// ??done
export const changeDemoAccountSetting = ({ values, success, error }) => sendRequest(`/Edit-account`, values, res => success(res), (err) => error(err))
export const createRealAccount = ({ values, success, error }) => sendRequest(`/Api-store-real-account`, values, res => success(res), (err) => error(err))
export const depositDrawRealAccount = ({ values, success, error }) => sendRequest(`/deposit&withdraw-request`, values, res => success(res), (err) => error(err))
// **later
export const loginCompany = ({ values, success, error }) => sendRequest(`/company-login`, values, res => success(res), (err) => error(err))
export const profilePersonalProfileCompanyPersonly = ({ values, success, error }) => sendRequest(`/store-company-Personal-Profile-Info`, values, res => success(res), (err) => error(err))
export const profilePersonalCompanyContactInformation = ({ values, success, error }) => sendRequest(`/store-company-store-Contact-Information`, values, res => success(res), (err) => error(err))
export const profilePersonalProfileCompanyAddresses = ({ values, success, error }) => sendRequest(`/store-company-store-Addresses-Information`, values, res => success(res), (err) => error(err))
/////////////////////////////////
// put
//////////////////////////////// 
export const depositDemoAccount=({ login, values, success, error })=> putRequest(`/demos/balance/updateApi/${login}`, values, res => success(res), err => error(err))
/////////////////////////////////
// get
////////////////////////////////
export const forgetPasswordByPhone = ({ success, error, phone }) => getRequest(`/send-phone-code?phone_number=${phone}`, res => success(res), err => error(err));
// ??done
export const convertNotificationToRead = ({ success, error }) => getRequest(`/make-Unread-notification-read`, res => success(res), err => error(err));
// !!check
export const convertAccountToFixed = ({ id, success, error }) => getRequest(`/ChangeFixedStatus?account_id=${id}`, res => success(res), err => error(err));
export const filterCloseDeals = ({ date, dateRange, success, error }) => getRequest('/closeDeals/filter?login=All', res => success(res), err => error(err), { 'dateRange': dateRange, 'date': date });
// ??done
export const getAllNotification = ({ success, error, page }) => getRequest(`/get-user-notification-Desc?page=${page}`, res => success(res), err => error(err));
// ??done
export const getAllNotificationAsc = ({ success, error, page }) => getRequest(`/get-user-notification-Asc?page=${page}`, res => success(res), err => error(err));
// ??done
export const getCurrentCountry = ({ success, error }) => getRequest(`/Get-current-country`, res => success(res), err => error(err));
// !!check
export const getPhoneCode = ({ success, error, phone }) => getRequest(`/verifyPhone?phone_number=${phone}`, res => success(res), err => error(err));

export const getEmailCode = ({ success, error, email }) => getRequest(`/send-Verification-Email?email=${email}`, res => success(res), err => error(err));
// ??done
export const loginGoogle1 = ({ success, error }) => getRequest(`/auth/google`, res => success(res), err => error(err));
// ??done
export const loginFacebook1 = ({ success, error }) => getRequest(`/facebook/auth`, res => success(res), err => error(err));
export const loginGoogle2 = ({ params, success, error }) => getRequest(`/auth/google/callback?${params}`, res => success(res), err => error(err));
export const loginFacebook2 = ({ params, success, error }) => getRequest(`/auth/facebook/callback?${params}`, res => success(res), err => error(err));
// ///////////////////////////////////
//  swr
// ////////////////////////////////////

// !!check
export const userPersonalProfile = () => `/get-user-basic-information`
// !!check
export const companyPersonalProfile = () => `/get-company-basic-information`
// !!check
export const profileIdentCheck = () => `/check-identityConfirmation-documents`
// !!check
export const profileDocs = () => `/get-user-Documents`
// !!check
export const profileAddressCheck = () => `/check-AddressConfirmation-documents`
// !!check
export const profileFinancialInformation = () => `/check-user-financial-profile-info`
// ??done
export const bankAccountDetails = () => `/Bank-account-details`
export const recordClosedDeals = (login) => `/get-record-for-close-Deals?login=${login}`
// ??done
export const recordOpenDeals = (login) => `/get-record-for-open-Deals?login=${login}`
// !!check
export const userDemoAccount = ({ perPage, page }) => `/getUserDemoAccounts-WithPagination?perPage=${perPage}&page=${page}`
// !!check
export const userDemoAccountWithoutPagination = () => `/getUserDemoAccounts-WithOutPagination`
// !!check
export const userRealAccount = ({ perPage, page }) => `/getUserRealAccounts?&perPage=${perPage}&page=${page}`
// !!check
export const userRealAccountWithoutPagination = () => `/getUserRealAccounts-WithOutPagination`
// !!check
export const allAccountsTypes = () => `/getAllAccountsTypes`
// !!check
export const registerData = () => `/get-user-register-data`
// !!check
export const CheckUserData = () => `/Check-User-Data`

export const notificationNumberUrl = () => `/get-Unread-notification-number`
// !!check

export const getActiveBonus = () => `/get-active-bonus`
// / /////////////////////////////
// delete 
// /////////////////////////////
// ??done
export const deleteDemoAccount = ({ login, success, error }) => deleteRequest(`/delete-account/${login}`, res => success(res), (err) => error(err))
