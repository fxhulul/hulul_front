import axios from "utils/axios";
import toast from "react-hot-toast";
import Trans from 'next-translate/Trans'

const sendRequest = (url, values, success, error) => {
    axios.post(url, values)
        .then(function (response) {
            success(response);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
                if (err.message === "Network Error") {
                    toast.error(<Trans i18nKey="errToast:sorry_there_is_a_problem_with_connecting_to_the_internet" />)
                } else {
                    // ! for test
                    // toast.error(err.message);
                    // toast.error(
                    //     <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                    // )
                }
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
}
const putRequest = (url, values, success, error) => {
    axios.put(url, values)
        .then(function (response) {
            // ! for test
            toast.success("success");
            success(response);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
                if (err.message === "Network Error") {
                    toast.error(<Trans i18nKey="errToast:sorry_there_is_a_problem_with_connecting_to_the_internet" />)
                } else {
                    // ! for test
                    toast.error(err.message);
                    toast.error(
                        <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                    )
                }
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
}
const getRequest = (url, success, error, params) => {
    axios.get(url, { params: params })
        .then(function (response) {
            success(response);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
}
const deleteRequest = (url, success, error) => {
    axios.delete(url)
        .then(function (response) {
            success(response);
        })
        .catch(function (err) {
            if (err.response) {
                error(err);
            } else {
                toast.error(
                    <Trans i18nKey="errToast:sorry_a_problem_occurred" />
                )
            }
        })
}
// /////////////////////////////////////////////
// post 
const register = ({ values, success, error }) => {
    sendRequest(`/register`, values, success, (err) => {
        if (err.response.data.phone) {
            toast.error(
                <Trans i18nKey="errToast:sorry_phone_previously_used" />
            )
        }
        if (err.response.data.email) {
            toast.error(
                <Trans i18nKey="errToast:sorry_email_previously_used" />
            )
        }
        error();
    })
}
function loginUser({ values, success, error }) {
    sendRequest(`/user-login`, values,
        (response) => {
            success(response);
        }, (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:please_enter_the_correct_phone_number" />
                )
            }
            if (err.response.status === 402) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_wrong_password" />
                )
            }
            error();
        })
}
function loginCompany({ values, success, error }) {
    sendRequest(`/company-login`, values,
        (response) => {
            success(response);
        }, (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_email_is_not_true" />
                )
            }
            if (err.response.status === 402) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_wrong_password" />
                )
            }
            error();
        })
}
function forgetPasswordByEmail({ values, success, error }) {
    sendRequest(`/password/forgot-password`, values,
        () => {
            success();
           
        }, (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_e_mail_is_not_used" />
                )
            }
            if (err.response.status === 400) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_old_password_cannot_be_used" />
                )
            }
            error(err);
        })
}
function forgetPasswordByPhone({ success, error, phone }) {
    getRequest(`/send-phone-code?phone_number=${phone}`,
        () => {
            success();
        }, (err) => {
            if (err.response.status === 400) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_old_password_cannot_be_used" />
                )
            }
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:please_enter_the_correct_phone_number" />
                )
            }
            error();
        })
}
function returnPasswordByEmail({ values, success, error }) {
    sendRequest(`/password/reset`, values,
        () => {
            success();
        },
        (err) => {
            if (err.response.status === 401) { toast.error(<Trans i18nKey="errToast:sorry_the_e_mail_is_not_used" />); }
            if (err.response.status === 500) { toast.error(<Trans i18nKey="errToast:sorry_the_problem_of_what_will_be_re_request_will_be_re_appointed" />); }
            error(err);
        })
}
function returnPasswordByPhone({ values, success, error }) {
    sendRequest(`/phone/reset/password`, values,
        () => {
            success();

            
        },
        (err) => {
            if (err.response.status === 401) { toast.error(<Trans i18nKey="errToast:sorry_the_phone_is_not_used" />); }
            if (err.response.status === 500) { toast.error(<Trans i18nKey="errToast:sorry_the_problem_of_what_will_be_re_request_will_be_re_appointed" />); }
            error(err);
        })
}
function enterCodeNumber({ values, success, error, verify }) {
    sendRequest(verify ? `/phoneVerifyCode` : `/check-phone-code`, values,
        (response) => { success(response); },
        (err) => {
            if (err.response.status === 400) { toast.error(<Trans i18nKey="errToast:sorry_the_account_was_confirmed_in_advance" />) }
            if (err.response.status === 401) { toast.error(<Trans i18nKey="errToast:sorry_the_code_is_wrong" />) }
            error();
        })
}
function createDemoAccount({ values, success, error }) {
    sendRequest(`/create-demo`, values,
        (response) => {
            success(response);
        },
        (err) => {
            error();
        })
}
function profilePersonalProfileUserPersonly({ values, success, error }) {
    sendRequest(`/store-user-Personal-Profile-Info`, values,
        () => {
            success();
        },
        (err) => {
            error();
        })
}
function profilePersonalProfileCompanyPersonly({ values, success, error }) {
    sendRequest(`/store-company-Personal-Profile-Info`, values,
        () => { success();  },
        (err) => {
            error();
        })
}
function profilePersonalUserContactInformation({ values, success, error }) {
    sendRequest(`/store-user-store-Contact-Information`, values,
        () => { success();  },
        (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_email_previously_used" />
                )
            }
            error();

        })
}
function profilePersonalCompanyContactInformation({ values, success, error }) {
    sendRequest(`/store-company-store-Contact-Information`, values,
        () => { success();  },
        (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_email_previously_used" />
                )
            }
            error();
        })
}
function profilePersonalProfileUserHeadLines({ values, success, error }) {
    sendRequest(`/store-user-store-Addresses-Information`, values,
        () => { success();
        (err) => {
            error();
        })
}
function profilePersonalProfileCompanyHeadLines({ values, success, error }) {
    sendRequest(`/store-company-store-Addresses-Information`, values,
        () => { success();},
        (err) => {
            error();
        })
}
function profilePersonalProfileChangePass({ values, success, error }) {
    sendRequest(`/change-user-password`, values,
        () => { success();  },
        (err) => {
            if (err.response.data.error === "Your current password does not matches with the password.") {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_wrong_password" />
                )
            }
            if (err.response.data.error === "New Password cannot be same as your current password.") {
                toast.error(
                    <Trans i18nKey="errToast:new_password_cannot_be_same_as_your_current_password" />
                )
            }
            error();
        })
}
function profilePersonalIdentificationConfirmation({ values, success, error }) {
    sendRequest(`/store-Documents`, values,
        () => { success(); },
        (err) => { error(); })
}
function profilePersonalFinancialInformation({ values, success, error }) {
    sendRequest(`/store-user-financial-profile-info`, values,
        () => { success(); },
        (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_you_cant_fill_your_financial_profile_info_more_than_one_time" />
                )
            }
            error();
        })
}
function profileBankAccount({ values, success, error }) {
    sendRequest(`/store-Bank-account-details`, values,
        () => { success(); },
        (err) => {

            error();
        })
}
function changeRealAccountSetting({ values, success, error }) {
    sendRequest(`/Edit-Real-account`, values,
        (res) => { success(res) },
        (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_account_is_not_present" />
                )
            }
            error();
        })
}
function changeDemoAccountSetting({ values, success, error }) {
    sendRequest(`/Edit-account`, values,
        (response) => {
            success(response);
        },
        (err) => {
            if (err.response.status === 401) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_account_is_not_present" />
                )
            }
            error();
        })
}
function createRealAccount({ values, success, error }) {
    sendRequest(`/Api-store-real-account`, values,
        () => {
            success();
            // toast.success(<Trans i18nKey="errToast:the_data_has_been_successfully_saved" />)
        },
        (err) => {
            error(err);
        })
}
function depositDrawRealAccount({ values, success, error }) {
    sendRequest(`/deposit&withdraw-request`, values,
        () => { success(); },
        (err) => {
            error(err);
        })
}
function depositDemoAccount({ login, values, success, error }) {
    putRequest(`/demos/balance/updateApi/${login}`, values,
        () => {
           
            success();
        },
        (err) => {
            if (err.response.status === 404) {
                toast.error(
                    <Trans i18nKey="errToast:sorry_the_account_is_not_present" />
                )
            }
            error();
        })
}
function convertAccountToFixed({ id, success, error }) {
    getRequest(`/ChangeFixedStatus?account_id=${id}`,
        () => { success() },
        (err) => { error(); })
}
function filterCloseDeals({ date, dateRange, success, error }) {
    // getRequest(`/closeDeals/filter?dateRange=${date}&login=All`,
    getRequest('/closeDeals/filter?login=All',
        (res) => { success(res) },
        (err) => { error(err); },
        { 'dateRange': dateRange, 'date': date }
    )
}

function getAllNotification({ success, error, page }) {
    getRequest(`/get-user-notification-Desc?page=${page}`,
        (res) => { success(res) },
        (err) => { error(err); })
}
function getAllNotificationAsc({ success, error, page }) {
    getRequest(`/get-user-notification-Asc?page=${page}`,
        (res) => { success(res) },
        (err) => { error(err); })
}
function convertNotificationToRead({ success, error }) {
    getRequest(`/make-Unread-notification-read`,
        (res) => { success(res) },
        (err) => { error(err); })
}
// const logout = () => { }

/////////////////////////////////
// get
////////////////////////////////
function getCurrentCountry({ success, error }) {
    getRequest(`/Get-current-country`, success, error)
}
function getPhoneCode({ success, error, phone }) {
    getRequest(`/verifyPhone?phone_number=${phone}`,
        success,
        (err) => {
            if (err.response.status === 400) { toast.error(<Trans i18nKey="errToast:sorry_the_phone_is_not_used" />); } else {
                toast.error(<Trans i18nKey="errToast:sorry_there_was_an_error_in_sending_the_code_please_return_later" />);
            }
            error(err)
        }
    )
}
function getEmailCode({ success, error, email }) {
    getRequest(`/send-Verification-Email?email=${email}`,
        (res) => {
            success(res)

        }
        , (err) => {
            if (err.response.status === 400) { toast.error(<Trans i18nKey="errToast:sorry_the_account_was_confirmed_in_advance" />) }
            else {
                toast.error(<Trans i18nKey="errToast:sorry_there_was_an_error_in_sending_the_code_please_return_later" />)
            }
            error(err)
        })
}
function loginGoogle1({ success, error }) {
    getRequest(`/auth/google`, success, error)
}
function loginFacebook1({ success, error }) {
    getRequest(`/facebook/auth`, success, error)
}
function loginGoogle2({ params, success, error }) {
    getRequest(`/auth/google/callback?${params}`, success, error)
}
function loginFacebook2({ params, success, error }) {
    getRequest(`/auth/facebook/callback?${params}`, success, error)
}



// ///////////////////////////////////
//  swr
const userPersonalProfile = () => `/get-user-basic-information`
const companyPersonalProfile = () => `/get-company-basic-information`
const profileIdentCheck = () => `/check-identityConfirmation-documents`
const profileDocs = () => `/get-user-Documents`
const profileAddressCheck = () => `/check-AddressConfirmation-documents`
const profileFinancialInformation = () => `/check-user-financial-profile-info`
const recordClosedDeals = (login) => `/get-record-for-close-Deals?login=${login}`
const recordOpenDeals = (login) => `/get-record-for-open-Deals?login=${login}`
const userDemoAccount = ({ perPage, page }) => `/getUserDemoAccounts-WithPagination?perPage=${perPage}&page=${page}`
const userDemoAccountWithoutPagination = () => `/getUserDemoAccounts-WithOutPagination`
const userRealAccount = ({ perPage, page }) => `/getUserRealAccounts?&perPage=${perPage}&page=${page}`
const userRealAccountWithoutPagination = () => `/getUserRealAccounts-WithOutPagination`
const allAccountsTypes = () => `/getAllAccountsTypes`
const registerData = () => `/get-user-register-data`
const CheckUserData = () => `/Check-User-Data`
const getNotificationNumber = () => `/get-Unread-notification-number`
const getActiveBonus = () => `/get-active-bonus`

// / /////////////////////////////

// delete 
function deleteDemoAccount({ login, success, error }) {
    deleteRequest(`/delete-account/${login}`,
        (response) => { success(response); },
        (err) => { error(err); })
}
const successLogin = (response) => {
    localStorage.setItem('token', response.data.access_token)
    localStorage.setItem("userType", response.data.user_ifo.type);
    localStorage.setItem("userId", response.data.user_ifo.id);
    window.location.href = "/dashboard"
}
export {
    register, forgetPasswordByEmail, forgetPasswordByPhone, returnPasswordByPhone, returnPasswordByEmail, getCurrentCountry, enterCodeNumber, getPhoneCode, getEmailCode, createDemoAccount, profilePersonalProfileUserPersonly, profilePersonalProfileCompanyPersonly, userPersonalProfile, companyPersonalProfile,
    profilePersonalCompanyContactInformation, profilePersonalUserContactInformation,
    profilePersonalProfileUserHeadLines, profileBankAccount, profilePersonalProfileCompanyHeadLines, profilePersonalProfileChangePass,
    profilePersonalIdentificationConfirmation, profileIdentCheck, profileAddressCheck, profileFinancialInformation, profilePersonalFinancialInformation, recordClosedDeals, recordOpenDeals, userDemoAccount, userDemoAccountWithoutPagination, userRealAccount, allAccountsTypes, deleteDemoAccount,
    changeRealAccountSetting, changeDemoAccountSetting, createRealAccount, convertAccountToFixed, userRealAccountWithoutPagination, registerData, CheckUserData, sendRequest, depositDemoAccount, depositDrawRealAccount, getNotificationNumber, getAllNotification
    , loginGoogle1, loginGoogle2, loginFacebook1, loginFacebook2, getAllNotificationAsc, getRequest, convertNotificationToRead, filterCloseDeals, loginCompany, loginUser, successLogin, profileDocs, getActiveBonus
}
