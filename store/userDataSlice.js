import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { CheckUserData } from "utils/apiHandle"
import axios from 'utils/axios';


export const getUserData = createAsyncThunk(
    'userdata/getUserData',
    async (_, thunkAPI) => {
        const { rejectWithValue } = thunkAPI;
        try {
            const res = await axios.get(CheckUserData());
            return await res.data.data;
        } catch (demoError) {
            return rejectWithValue(demoError.message);
        }
    }
);
const userDataSlice = createSlice({
    name: 'userdata',
    initialState: { userData: [], loadingUserData :false, errorUserData : null },
    reducers: {},
    extraReducers: {
        //get userdata 
        [getUserData.pending]: (state, action) => {
            state.loadingUserData = true;
            state.errorUserData = null;
        },
        [getUserData.fulfilled]: (state, action) => {
            state.userData = action.payload;
            state.loadingUserData = false;
        },
        [getUserData.rejected]: (state, action) => {
            state.errorUserData = action.payload;
            state.loadingUserData = false;
        },

    },
 
});

export default userDataSlice.reducer;