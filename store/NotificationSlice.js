import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { notificationNumberUrl } from "utils/apiHandle"
import axios from 'utils/axios';


export const getNotificationNumber = createAsyncThunk(
    'notification/getNotificationNumber',
    async (_, thunkAPI) => {
        const { rejectWithValue } = thunkAPI;
        try {
            const res = await axios.get(notificationNumberUrl());
            return await res.data.data;
        } catch (demoError) {
            return rejectWithValue(demoError.message);
        }
    }
);
const NotificationSlice = createSlice({
    name: 'notification',
    initialState: { notificationNumber: [], loadingNotificationNumber :false, errorNotificationNumber : null },
    reducers: {},
    extraReducers: {
        //get notification number
        [getNotificationNumber.pending]: (state, action) => {
            state.loadingNotificationNumber = true;
            state.errorNotificationNumber = null;
        },
        [getNotificationNumber.fulfilled]: (state, action) => {
            state.notificationNumber = action.payload;
            state.loadingNotificationNumber = false;
        },
        [getNotificationNumber.rejected]: (state, action) => {
            state.errorNotificationNumber = action.payload;
            state.loadingNotificationNumber = false;
        },

    },
 
});

export default NotificationSlice.reducer;