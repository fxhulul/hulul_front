import React, { useState } from "react";
export const globalContext = React.createContext({
  prop: "",
  userInfo:[],
  func: () => {},
  changeUserInfo:()=>{},
  changeLastActivities:()=>{},
  lastActiveties:[],
  errorlastActiveties:false,
  changeErrorlastActiveties: () => { },
  theme : 'light'
});
//you can dont write initial obj just for ide
export const GlobalContextProvider = (props) => {
  const [notComplete, setNotComplete] = useState(0);
  const [theme, setTheme] = useState('light');
  const [userInfo, setUserInfo] = useState({});
  const [lastActiveties, setLastActiveties] = useState([]);
  const [errorlastActiveties, setErrorlastActiveties] = useState([]);
  const changeTheme=(data)=>{
    setTheme(data)
  }
  const changNotComplete =(data)=>{
    setNotComplete(data)
  }
  const changeUserInfo=(data)=>{
    setUserInfo(data)
  }
  const changeLastActivities=(data)=>{
    setLastActiveties(data)
  }
  const changeErrorlastActiveties=(data)=>{
    setErrorlastActiveties(data)
  }
 
  const func = () => {
  };
  return (
    <globalContext.Provider
      value={{
        prop: "001",
        func: func,
        changeUserInfo,
        userInfo,
        lastActiveties,
        changeLastActivities,
        errorlastActiveties,
        changeErrorlastActiveties,
        theme,
        changeTheme,
        changNotComplete,
        notComplete
      }}
    >
      {props.children}
    </globalContext.Provider>
  );
};
export default globalContext;
// note name of js       is cababe-case
//              function is UpperCase
