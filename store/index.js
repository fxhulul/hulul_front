import { configureStore } from '@reduxjs/toolkit';
import notification from './NotificationSlice';
import DemoAccounts from './DemoAccountsSlice';
import userdata from './userDataSlice';

export default configureStore({ reducer: { notification, DemoAccounts, userdata } });
