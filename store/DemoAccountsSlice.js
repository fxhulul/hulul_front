import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'utils/axios';
export const getDemoAccounts = createAsyncThunk(
    'demo_accounts/getDemoAccounts',
    async (_, thunkAPI) => {
        const { rejectWithValue } = thunkAPI;
        axios.get('/getUserDemoAccounts-WithOutPagination').then((res) => res.json()).catch(rejectWithValue(error.message))
    }
);
// export const getDemoAccounts = createAsyncThunk(
//     'demo_accounts/getDemoAccounts',
//     async (_, thunkAPI) => {
//         const { rejectWithValue } = thunkAPI;
//         try {
//             const res = await fetch(`http://localhost:3005/accounts`);
//             const data = await res.json();
//             console.log('data' ,data);
//             return data;
//         } catch (error) {
//             console.log(`%c error ${error}`, 'color:red')
//             return rejectWithValue(error.message);
//         }
//     }
// );
const DemoAccountsSlice = createSlice({
    name: 'demo_accounts',
    initialState: { accounts: [], loading: false, error: null },
    reducers: {},
    extraReducers: {
        //get demo_accounts 
        [getDemoAccounts.pending]: (state, action) => {
            state.loading = true;
            state.error = null;
        },
        [getDemoAccounts.fulfilled]: (state, action) => {
            state.accounts = action.payload;
            state.loading = false;
        },
        [getDemoAccounts.rejected]: (state, action) => {
            state.error = action.payload;
            state.loading = false;
        },
    },

});

export default DemoAccountsSlice.reducer;