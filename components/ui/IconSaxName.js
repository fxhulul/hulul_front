import React from 'react'
import * as icon from 'iconsax-react';
import {LeverageIcon} from "public/svg"
export default function IconSaxName({ name , size , ...props}) {
    let iconAfterSwitch;
    switch (name) {
        case "money-recive":
            iconAfterSwitch = <icon.MoneyRecive className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "wallet-add":
            iconAfterSwitch = <icon.WalletAdd className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "messages":
            iconAfterSwitch = <icon.Messages className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "money-send":
            iconAfterSwitch = <icon.MoneySend className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "trash":
            iconAfterSwitch = <icon.Trash className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "card-send":
            iconAfterSwitch = <icon.CardSend className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "profile-tick":
            iconAfterSwitch = <icon.ProfileTick className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "location-tick":
            iconAfterSwitch = <icon.LocationTick className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "location-cross":
            iconAfterSwitch = <icon.LocationCross className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "profile-delete":
            iconAfterSwitch = <icon.ProfileDelete className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "profile-remove":
            iconAfterSwitch = <icon.ProfileRemove className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "brush":
            iconAfterSwitch = <icon.Brush className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "import":
            iconAfterSwitch = <icon.Import className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "lock":
            iconAfterSwitch = <icon.Lock className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "call":
            iconAfterSwitch = <icon.Call className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "sms":
            iconAfterSwitch = <icon.Sms className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "edit-2":
            iconAfterSwitch = <icon.Edit2 className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "verify":
            iconAfterSwitch = <icon.Verify className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "forbidden-2":
            iconAfterSwitch = <icon.Forbidden2 className="[color:rgb(var(--primary-color))] " size={size}/>
            break;
        case "leverage":
            iconAfterSwitch = <LeverageIcon className="[color:rgb(var(--primary-color))] " width={size}/>
            break;
        default:
            iconAfterSwitch = <icon.Add className="[color:rgb(var(--primary-color))] " size={size}/>
    }

    return (
        <div className={props.className}>
            {iconAfterSwitch}
        </div>
    )
}
