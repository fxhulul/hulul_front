import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getDemoAccounts } from '../../store/DemoAccountsSlice';

export default function TestRedux() {
    const dispatch = useDispatch();
    const { accounts, loading, error } = useSelector((state) => state);
    useEffect(() => {
        dispatch(getDemoAccounts());
    }, [dispatch]);
    const handleAdd = () => {
        // dispatch(
        //     insertBook({
        //       name:'test'
        //     }).then()
        // );
    }
    const handleDelete = (id) => {
        // console.log('delete ', id);
        // dispatch(
        //     deleteBook(id)
        // );
    }
    return (
        <div >
            {/* {loading ? 'loading' : error ? 'error' :  !accounts.length ? 'no data' : accounts.map((accounts, index) => (
                <div className='flex' key={index}><p >{accounts.id}</p><button onClick={()=>handleDelete(accounts.id)}>delete</button></div>
            ))} */}
            <br/>
            <button onClick={handleAdd}>add</button>
        </div>
    )
}