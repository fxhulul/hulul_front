import React,{useState} from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Calendar } from 'iconsax-react';
import { DateRangePicker } from 'rsuite';
import { isAfter, format } from 'date-fns'

export default function CustomDateRangePicker(props) {
    const { t, lang } = useTranslation("record")
    return (
        <div className={`relative flex items-center gap-2 p-2 rounded-lg bg-secondary dark:bg-dark-secondary  ${props.className}`}>
            <DateRangePicker caretAs={"li"} character=" - " appearance="subtle" cleanable={false} isoWeek={true} showWeekNumbers={true} locale={{ today: t("the_today"), yesterday: t("yesterday"), last7Days: t("last7Days"), ok: t("ok") }}
                format={"MM/dd/yyyy"}
                placement={lang === "ar" ? "bottomEnd" : "bottomStart"}
                // value={props.value}
                onChange={(value) => {props.onChange(value ,JSON.stringify({start:format(value[0], 'MM/dd/yyyy'),end:format(value[1], 'MM/dd/yyyy')}));}}
            />
            <div className="p-2 rounded-lg pointer-events-none [background:rgba(var(--primary-color),1)] z-5">
                <Calendar className="text-white" size="30" />
            </div>
        </div>
    )
}
