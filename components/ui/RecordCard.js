import React from 'react'

export default function RecordCard({card}) {
  return (
    <div  className={`${card.class}  lg:p-6 p-4 rounded-xl w-full`}>
    <h2>{card.title}</h2>
    <strong className="mb-4 lg:text-3xl md:text-2xl text-xs font-black">{card.number}</strong>
  </div>
  )
}
