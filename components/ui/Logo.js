import React from 'react'
import Image from "next/image"
import logo from "public/images/placeholder/logo.png"

export default function Logo({big}) {
  return (
    // <Image src={logo} alt="logo" width={big ? "200" : "170"}  height={big ? "40" : "45"} />
    <div className="relative h-16 w-9/12 m-4 max-w-[12rem]">
      <Image src={logo} alt="logo" layout="intrinsic" />
    </div>
  )
}
