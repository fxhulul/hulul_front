import React from 'react';
import { VisaIcon } from "public/svg"
import useTranslation from 'next-translate/useTranslation'

const CardAccountVisa = ({ color }) => {
    const { t, lang } = useTranslation("common")
    return (
        <div className={`text-white relative select-none`}>
            <div className={`${color} py-5 px-6 rounded-t-xl`}>
                <div className="mb-8 flex justify-between items-center">
                    <VisaIcon />
                    <button className="bg-white dark:bg-dark-white text-[#E30000] rounded-xl px-2 py-4 text-5xl leading-1">&times;</button>
                </div>
                <span className="block   text-gray-400">{t("card_number")}</span>
                <bdi className="block  text-4xl ">5468   ****   **** 1234</bdi>
            </div>
            <ul className={`${color} opacity-75 flex justify-between py-5 px-6 rounded-b-xl flex-wrap`}>
                <li>
                    <h6 className="text-sm  text-slate-300">cvv</h6>
                    <bdi className="text-base ">**5</bdi>
                </li>

                <li>
                    <h6 className="text-sm  text-slate-300">{t("date")}</h6>
                    <span className="text-base ">09/23</span>
                </li>
                <li className='w-full text-left'>
                    <p>Mohammed ahmad</p>
                </li>
            </ul>

        </div>
    )
}
export default CardAccountVisa