import React, { useState, useEffect, useContext } from 'react'
import { Notification, Translate, Sun1, Moon, HambergerMenu, Add, Element, Personalcard, Refresh2, Star1, Setting4, Wallet3, Profile, LogoutCurve, ClipboardClose, ClipboardTick, ClipboardExport } from 'iconsax-react'
import { Logo, SmallAside, SearchSite, DarkThemeButton, ChangeLangButton, IconSaxName, BackOverlay } from "@/ui"
import useTranslation from 'next-translate/useTranslation'
import { useRouter } from 'next/router'
import { LogoutModel } from "@/modals"
import useAuth from 'hooks/useAuth'
import Image from "next/image"
import Link from "next/link"
import usePersistLocaleCookie from "hooks/use-persist-locale-cookie"
import globalContext from "store/global-context";
import useSWR from 'swr'
import { CheckUserData } from "utils/apiHandle"
import toast from "react-hot-toast";
import profile from "public/images/placeholder/profile.png"
import Pusher from 'pusher-js';
export default function TopNavAndAside() {
    const { t, lang } = useTranslation("aside");
    const [openAside, setOpenAside] = useState(false)
    const roots = [
        { title: t('control_board'), icon: <Element size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard", },
        { title: t('the_accounts'), icon: <Personalcard size="18" className="[color:rgb(var(--primary-color))] " />, href: "/", },
        {
            title: t('record'), icon: <Refresh2 size="18" className="[color:rgb(var(--primary-color))] " />, collapse: "/dashboard/record",
            list: [
                { title: t('closed_deals'), icon: <ClipboardClose size="18" className="text-inherit" />, href: "/dashboard/record/closed-deals?login=All" },
                { title: t('open_deals'), icon: <ClipboardTick size="18" className="text-inherit" />, href: "/dashboard/record/open-deals?login=All" },
                { title: t('deposit_and_clouds'), icon: <ClipboardExport size="18" className="text-inherit" />, href: "/dashboard/record/deposit-and-clouds" }
            ],
        },
        { title: t('promotion'), icon: <Star1 size="18" className="[color:rgb(var(--primary-color))] " />, href: "/", },
        { title: t('tools'), icon: <Setting4 size="18" className="[color:rgb(var(--primary-color))] " />, href: "/", },
        { title: t('money_management'), icon: <Wallet3 size="18" className="[color:rgb(var(--primary-color))] " />, href: "/", },
        { title: t('profile_personly'), icon: <Profile size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/personal/profile-personally", },

    ]
    const { logout } = useAuth({ middleware: 'auth' })
    const router = useRouter()
    const [openLogout, setOpenLogout] = useState(false)
    const [loadingLogout, setLoadingLogout] = useState(false)
    const handleLogout = () => {
        setLoadingLogout(true);
        logout({
            success: () => { setOpenLogout(false); setLoadingLogout(false); },
            error: () => { setLoadingLogout(false); setOpenLogout(false) }
        })
    }

    const ctx = useContext(globalContext);
    const { data, error } = useSWR(CheckUserData())
    const [openNotification, setOpenNotification] = useState(false);
    var userId;
    if (typeof window !== "undefined") {
        userId = localStorage.userId
    }

    useEffect(() => {
        if (data) {
            ctx.changeUserInfo({
                "email": data.values[0].email,
                "phone": data.values[0].phone,
                "FinancialProfile": data.values[0].financialProfile,
                "Documents": data.values[0].documents,
                "basicInfo": data.values[0].basicInfo,
                "userImage": data.values[0].image,
                "name": data.user.name,
                "email_field": data.user.email,
                "phone_field": data.user.phone,
                "country": data.user.country,
            })

        }
    }, [data]);

    useEffect(() => {
        Pusher.logToConsole = true;

        const pusher = new Pusher('c568dc6eb5ee1b3a2ad9', {
            cluster: 'ap2',
            encrypted: true

        });

        const channel = pusher.subscribe(`notifications_${userId}`);
        channel.bind('Notifications', function (data) {
            // allMessages.push(data);
            // setMessages(data);

            toast.custom((t) => (
                <div
                    className={`${t.visible ? 'animate-enter' : 'animate-leave'
                        } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5`}
                >
                    <div className="flex-1 w-0 p-4">
                        <div className="flex items-start gap-4">
                            <div className="flex-shrink-0 pt-0.5">
                                <Image
                                    className="h-10 w-10 rounded-full"
                                    src={profile}
                                    alt="Emilia Gates"
                                    width="70"
                                    height="70"
                                />
                            </div>
                            <div className="ml-3 flex-1">
                                <p className="text-sm font-medium text-gray-900">
                                    Emilia Gates
                                </p>
                                <p className="mt-1 text-sm text-gray-500">
                                    Sure! 8:30pm works great!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="flex border-l border-gray-200 justify-start">
                        <button
                            onClick={() => toast.dismiss(t.id)}
                            className="w-full border border-transparent rounded-none rounded-r-lg p-4 flex items-center justify-center font-medium text-danger text-4xl h-max"
                        >
                            <Add size="40" className="transform rotate-45" />
                        </button>
                    </div>
                </div>
            ))
        });
    }, [])
    return (
        <React.Fragment>
            {/* aside */}
            <div className={`bg-white dark:bg-dark-white lg:w-72 w-96 z-10 max-w-full ${openAside ? "block" : "hidden"}  xl:block grid-area-home-aside fixed h-full overflow-auto `}>
                <div className=" items-center justify-center p-4 mb-4 xl:flex hidden">
                    <Logo big />
                </div>
                <button className="xl:hidden icon-container  flex justify-center items-start p-0-important relative w-10 h-10 my-6 rtl:mr-auto ltr:mr-auto rtl:ml-4 ltr:ml-4" onClick={() => closeAsideHandler()}>
                    <Add size="40" className="transform rotate-45 text-danger text-2xl" />
                </button>
                <div className="p-4 m-4 bg-secondary dark:bg-dark-secondary rounded-xl xl:hidden">
                    <SearchSite className="xl:hidden" />
                </div>
                <SmallAside steps={2} type="main" roots={roots} />
                <br />
                <button onClick={() => setOpenLogout(true)} className={`w-full flex items-center gap-4  mb-2 py-4  ltr:pl-8  rtl:pr-8  hover:bg-secondary/80 dark:hover:bg-dark-secondary `}>
                    <div className="icon-container">
                        <LogoutCurve size="18" className="[color:rgb(var(--primary-color))] " />
                    </div>
                    <span className="text-lg text-black dark:text-white" >
                        {t('sign_out')}
                    </span>
                </button>
                <ul className="flex items-start gap-4 p-6">
                    <ChangeLangButton className="lg:hidden " />
                    <DarkThemeButton className="lg:hidden " />

                </ul>
            </div >

            <LogoutModel open={openLogout} onClose={() => setOpenLogout(false)} handleLogout={handleLogout} loading={loadingLogout} />
            {/* aside */}
            {/* top nav */}
            <div className="flex-grow bg-white dark:bg-dark-white dark:text-white grid-area-home-top">
                <div className="container2 p-4  mx-auto">
                    <div className="flex items-center xl:justify-between justify-end">
                        <SearchSite className="xl:block hidden" />
                        <ul className="flex items-start gap-4">
                            <DarkThemeButton className="xl:block hidden " />
                            {/* <ChangeLangButton className="xl:block hidden " /> */}
                            <li className="relative ">
                                <button className="icon-container  flex justify-center items-start  relative w-10 h-10" onClick={() => setOpenNotification(!openNotification)}>
                                    <Notification className="[color:rgb(var(--primary-color))] " />
                                </button>
                                {openNotification && <div className="absolute top-12 ltr:right-0 rtl:left-0 z-3 p-6 rounded-xl text-black dark:text-white max-h-[calc(100vh_-_5rem)] overflow-y-auto w-[25rem] bg-white dark:bg-dark-white">
                                    <div className="flex justify-between items-center mb-6">
                                        <h4 className="text-2xl ">{t("notifications")}</h4>
                                        <select className={`block w-[9.375rem]  px-4 py-4  rounded-md focus:outline-0 bg-secondary dark:bg-dark-secondary`}>
                                            <option>{t("the_most_recent")}</option>
                                            <option>{t("the_oldest")}</option>
                                        </select>
                                    </div>
                                    <ul className="p-0 m-0 text-black dark:text-white">
                                        {Array.from({ length: Number.parseInt(10) }, (item, index) => (
                                            <li className="flex items-start gap-2 mb-4" key={index}>
                                                <div className="mb-2 icon-container">
                                                    <IconSaxName name="leverage" size="18" />
                                                </div>
                                                <div>
                                                    <h3 className="mb-0 text-sm leading-none">ايداع مبلغ 320$ في حسابك</h3>
                                                    <span className="text-xs text-gray-400">منذ 2 ساعة</span>
                                                </div>
                                            </li>
                                        ))}
                                    </ul>
                                </div>}
                            </li>
                            <li className="leading-none xl:block hidden">
                                {ctx.userInfo.userImage ?
                                    <Image src={`${process.env.hostImage}/${ctx.userInfo.userImage}`} width="40" height="40" alt="profile" className="rounded-xl" />
                                    :
                                    <div className="[background:rgba(var(--primary-color),1)] rounded-xl w-10 h-10 flex justify-center items-center" >
                                        <Profile className="text-white " size="30" />
                                    </div>
                                }
                            </li>
                            <li className="leading-none xl:hidden">
                                <button className="icon-container  flex justify-center items-start  relative w-10 h-10" onClick={() => openAsideHandler()}>
                                    <HambergerMenu className="[color:rgb(var(--primary-color))] " />
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {openNotification && <BackOverlay onClick={() => setOpenNotification(false)} />}
            {/* top nav */}
        </React.Fragment>
    )
}
