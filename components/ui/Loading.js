import React from 'react'
import { Loader } from 'rsuite';
import useTranslation from 'next-translate/useTranslation'
export default function Loading({ page, ...props }) {
    const { t, lang } = useTranslation("common")
  return (
    page ?
      <div className="fixed top-0 right-0 w-full h-full z-10">
        <Loader backdrop size="md" vertical content={t("loading")} />
      </div>
      :
      <div className={`relative py-10 ${props.className}`}>
        <Loader backdrop size="md" vertical />
      </div>
  )
}
