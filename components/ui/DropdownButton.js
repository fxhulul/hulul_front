import React , { useState } from 'react'
import { ArrowUp2, ArrowDown2 } from 'iconsax-react';
import Router, { useRouter } from 'next/router';
import { useEffect } from 'react';
// import { Animation } from 'rsuite';

export default function DropdownButton({ open:openDropdown, onToggle, head, link, center, ...props }) {

    const router = useRouter();
    const [open, setOpen] = useState(false);
    useEffect(() => {
        setOpen(openDropdown)
    },[openDropdown, router])

    //open : open DropdownButton
    //onToggle : function
    // head : head of dropdown
    // link :if not dropdown it is just a link
    // center :center head text
    return (
        <div className={props.className} >
            <div className={`relative flex gap-2 cursor-pointer ${center && "justify-center"}`}onClick={link ? () => router.push(link) : () => setOpen(!open)}>
                {head}
                {(!link) &&
                    <ArrowUp2 size="20" className={`${open ? "rotate-180" : "rotate-0"}  transition ease-in-out delay-150 dark:text-white text-[#333333] absolute rtl:left-2 rtl:md:left-4 ltr:right-2 ltr:md:right-4 top-1/2 transform -translate-y-1/2`} /> }
            </div>
            <div className={`${open ?" max-h-80":"max-h-0"} overflow-hidden transition-[max-height] ease-in-out delay-150 duration-100	`}>
            {props.children}
            </div>
        </div>
    )
}
