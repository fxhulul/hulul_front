import React, { useEffect, useState, useContext } from 'react'
import { Sun1, Moon } from 'iconsax-react'
import globalContext from "store/global-context";
export default function DarkThemeButton({ className }) {
      const ctx = useContext(globalContext);
    const [dark, setDark] = useState(false);
    useEffect(() => {
        if (typeof window !== "undefined") {
            if (localStorage.getItem("theme") === "dark") {
                setDark(true);
                ctx.changeTheme('dark')
                document.documentElement.classList.add("dark");
            } else {
                ctx.changeTheme('light')
                setDark(false)
                document.documentElement.classList.remove("dark");
            }
        }
    }, []);
    const handleChangeDark = () => {
        setDark(!dark);
        document.documentElement.classList.toggle("dark");
        if(!dark){
            localStorage.setItem("theme", "dark");
            ctx.changeTheme('dark')
            
        }else{
            localStorage.setItem("theme", "light");
            ctx.changeTheme('light')
        }
    }
    return (
        <li className={className}>
            <button onClick={handleChangeDark} className={`${!dark ? 'bg-black-important' : ""} icon-container flex justify-center items-center  relative w-10 h-10`}>
                <div >
                    {dark ?
                        <Sun1 className="[color:rgb(var(--primary-color))]  w-7 h-7" />
                        :
                        <Moon className="text-white w-7 h-7" />
                    }
                </div>
            </button>
        </li>
    )
}
