import React, { useState, useEffect, useRef } from 'react'
import { ArrowLeft2, ArrowRight2 } from 'iconsax-react';
import { CardAccountTop } from "@/ui"
import useTranslation from 'next-translate/useTranslation'
import useWindowSize from "hooks/use-window";
import {  Navigation ,Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { useRouter } from 'next/router'
export default function Slider2({ data, type, chooseSlide: slide, singleSlide, handleChooseIndexSlide }) {
  const size = useWindowSize();
  const router = useRouter()
  const { t, lang } = useTranslation("common")
  const swiperRef = useRef();
  swiperRef?.current?.slideTo(slide);
  // console.log('slideToClickedSlide',swiperRef?.current.slideToClickedSlide())
  useEffect(() => {
    // if () {
    //   swiperRef?.current?.slideTo(slide);
    // }
  },[ slide])
  return (
    <div className="mb-6 relative h-52">
      <div className=" h-full absolute left-0 right-0 w-full mx-auto t-0">
        <Swiper
          grabCursor={true}
          // dir={lang === 'ar' ? "rtl" : "ltr"}
          dir={"rtl"}
          initialSlide={slide}
          onBeforeInit={(swiper) => {
            swiperRef.current = swiper;
          }}
          slidesPerView={!singleSlide ?  (size.width > process.env.md && data.length > 2)  ? 2 : 1 : 1}
          spaceBetween={30}          
          navigation={true}
          onSlideChange={(e) => { handleChooseIndexSlide(e.snapIndex);  console.log('change')}}
          modules={[ Navigation,Pagination]}
          className="mySwiper deposit-slider md:rtl:px-6 md:ltr:px-6 "
          pagination={size.width > process.env.md  ? false : {
            dynamicBullets: "true",
          }} 
         
        >
          {data.map((account, i) => (
            // <div className={` ${chooseSlide === i ? active : "opacity-70 transform scale-75  "} ${chooseSlide === i + 1 ? next : chooseSlide === i - 1 ? prev : "left-0 right-0 "} absolute 	 mx-auto h-full w-[20rem] transition duration-75	  `} onClick={() => handleCLickSlider(i)} key={i}>
            <SwiperSlide key={i} className="md:mb-0 mb-7">
              <CardAccountTop data={account} type={type} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  )
}