import React from 'react'
import { CardPos , Warning2} from 'iconsax-react';

export default function NoData({text, account}) {
    return (
        <div className="text-center py-20 col-span-2">
           {account ?  <CardPos  className="text-gray-400 mb-4 mx-auto lg:w-28 w-24 lg:h-28 h-24" />:<Warning2  className="text-gray-400 mb-4 mx-auto lg:w-28 w-24 lg:h-28 h-24" />}
            <p className="font-bold lg:text-3xl text-xl text-gray-400">{text}</p>
        </div>

    )
}
