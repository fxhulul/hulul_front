import React from 'react'

export default function BackOverlay(props) {
  return (
 <div className=" z-2 h-full w-full top-0 right-0 fixed" onClick={() => props.onClick()}></div>
  )
}
