import React, { useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { IconSaxName, NotificationItems } from "@/ui"
import toast from "react-hot-toast";
import Image from "next/image"
import profile from "public/images/placeholder/profile.png"
import { Add } from 'iconsax-react';
import useWindowSize from "hooks/use-window";
import globalContext from "store/global-context";
export default function LastActivity({ ...props }) {
  const size = useWindowSize();
  const { t } = useTranslation("dashboard")
  const ctx = useContext(globalContext);
  return (
    size.width > process.env.lg ?
      <div className={`hidden lg:block ${props.className}`} style={props.style}>
        <section className="mb-6 overflow-auto bg-white rounded-lg dark:bg-dark-white md:rounded-xl">
          <h2 className="p-4 text-sm text-gray-500">{t('your_last_activity')}</h2>
          <ul className="p-0 m-0 text-black dark:text-white">
            <NotificationItems allNotificationCollect={ctx.lastActiveties} errorAllNotification={ctx.errorlastActiveties} activity={true} />
          </ul>
        </section>
      </div>
      :
      <React.Fragment></React.Fragment>
  )
}
