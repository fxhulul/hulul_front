import React from 'react';
import { Location, ClipboardText, Lock1, Call, Add } from 'iconsax-react'
import Link from "next/link"

const CreateBankAccount = ({ text, href, onClick }) => {
    return (
        <div className="relative flex items-center justify-center border-4 border-dashed [border-color:rgba(var(--primary-color),1)] [background:rgba(var(--primary-color),0.3)] rounded-xl ">
            {href ?
                <Link href={href}>
                    <a className="relative flex flex-col items-center w-full p-4 text-xl rounded-xl [color:rgb(var(--primary-color))] ">
                        <Add
                            size="60"
                            className="mb-4 [color:rgb(var(--primary-color))] "
                        />
                        <span className="text-xl">
                            {text}
                        </span>
                    </a>
                </Link>
                :
                <button className="relative flex items-center justify-center w-full gap-2 p-4 text-xl md:flex-col lg:gap-0 rounded-xl [color:rgb(var(--primary-color))] " onClick={() => onClick()}>
                    <Add
                        className="[color:rgb(var(--primary-color))]w-7 h-7 md:h-16 md:w-16"
                    />
                    <span className="text-sm lg:text-xl">
                        {text}
                    </span>
                </button>
            }
        </div>
    )
}
export default CreateBankAccount