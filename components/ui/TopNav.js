import React, { useState, useEffect, useContext, useRef, useCallback, memo } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Notification, Translate, Sun1, Moon, Profile, HambergerMenu, Add, Warning2 } from 'iconsax-react'
import Image from "next/image"
import Link from "next/link"
import { Loader } from 'rsuite';
import globalContext from "store/global-context";
import {  getAllNotification, getAllNotificationAsc, convertNotificationToRead, getPhoneCode } from "utils/apiHandle"
import { IconSaxName, SearchSite, DarkThemeButton, ChangeLangButton, BackOverlay, NotificationItems, ButtonTheme } from "@/ui"
import toast from "react-hot-toast";
import Pusher from 'pusher-js';
import { generateLink, convertTime } from "utils/funcs"
import { useRouter } from 'next/router'
import logo from "public/images/placeholder/logo.png"
import { WarningModal } from "@/modals"
import useAuth from 'hooks/useAuth'
import { useDispatch, useSelector } from 'react-redux';
import { getNotificationNumber } from 'store/NotificationSlice';
import { getUserData } from 'store/userDataSlice';

function TopNav({ openAsideHandler }) {
  const { logout } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("common")
  const router = useRouter()
  const ctx = useContext(globalContext);
  const [pageNotification, setPageNotification] = useState(1);
  const [sort, setSort] = useState('dec');
  const [allNotification, setAllNotification] = useState([]);
  const [errorAllNotification, setErrorAllNotification] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);
  const [loadingNotification, setLoadingNotification] = useState(false);
  const [openNotification, setOpenNotification] = useState(false);
  const [allNotificationCollect, setAllNotificationCollect] = useState([]);
  const [total, setTotal] = useState(0);
  const [lastNotificationNumber, setLastNotificationNumber] = useState(0);
  const [noMore, setNoMore] = useState(false);
  const [anewPusher, setAnewPusher] = useState({});
  const [enterPhoneOrLogoutModel, setEnterPhoneOrLogoutModel] = useState(false);
  const [loadingLogout, setLoadingLogout] = useState(false);
  const [confirmAccount, setConfirmAccount] = useState(false);
  const notificationList = useRef(null);
  const dispatch = useDispatch();
  const { notificationNumber, loadingNotificationNumber, errorNotificationNumber } = useSelector((state) => state.notification);
  const { userData, loadingUserData,errorUserData} = useSelector((state) => state.userdata);
  useEffect(() => {
    dispatch(getNotificationNumber());
    dispatch(getUserData());
  }, [dispatch]);
  useEffect(() => {
    if (!loadingNotificationNumber && !errorNotificationNumber) {
      setLastNotificationNumber(notificationNumber.notifications_num)
    }
  }, [errorNotificationNumber, loadingNotificationNumber, notificationNumber])

  const updateData = (sort, pageNotification, success, error) => {
    if (sort == "asc") {
      getAllNotificationAsc({
        page: pageNotification,
        success: (res) => {
          setLoadingNotification(false);
          success(res.data.notifications.data, res.data.notifications.total)
        },
        error: (err) => { setLoadingNotification(false); setErrorAllNotification(err); error() },
      })
    } else {
      getAllNotification({
        page: pageNotification,
        success: (res) => { setLoadingNotification(false); success(res.data.notifications.data, res.data.notifications.total) },
        error: (err) => { setLoadingNotification(false); setErrorAllNotification(err); error() },
      })
    }
  }
  // get first notification in start page , change sort 
  useEffect(() => {
    setLoadingNotification(true);
    updateData(sort, pageNotification, (data, total) => {
      setAllNotification(data);
      setAllNotificationCollect(data);
      setTotal(total)
    }, () => { })
  }, [setAllNotification, sort]);


  // save ctx values
  useEffect(() => {
    if (!loadingUserData && !errorUserData && userData && userData.values) {
      console.log('userData', userData)
      // if user not verify return to verify phone if user and email if company
      if (userData.values[0]) {
        if (localStorage.userType === 'user' && !userData.values[0].phone) {
          setEnterPhoneOrLogoutModel(true)
        }
      }
      
      ctx.changeUserInfo({
        "email": userData && userData.values && userData?.values[0]?.email,
        "phone": userData && userData.values &&  userData.values[0]?.phone,
        "financialProfile": userData && userData.values &&  userData.values[0]?.financialProfile,
        "identity_confirmation": userData && userData.values && userData.values[0]?.identity_confirmation,
        "Address_confirmation": userData && userData.values && userData.values[0]?.Address_confirmation,
        "all_basicInfo": userData && userData.values && userData.values[0]?.all_basicInfo,
        "is_bank_account_filled": userData && userData.values && userData.values[0]?.is_bank_account_filled,
        "userImage": userData && userData.values && userData.values[0]?.image,
        "name": userData && userData.user && userData.user[0]?.name,
        "email_field": userData && userData.user && userData.user[0]?.email,
        "phone_field": userData && userData.user && userData.user[0]?.phone,
        "country": userData && userData.user && userData.user[0]?.country,
        "birth_date": userData && userData.user && userData.user[0]?.birth_date,
        "birth_location": userData && userData.user && userData.user[0]?.birth_location,
        "city": userData && userData.user && userData.user[0]?.city,
        "zip": userData && userData.user && userData.user[0]?.zip,
        "adders": userData && userData.user && userData.user[0]?.adders,
        "citizenship": userData && userData.user && userData.user[0]?.citizenship,
      })
       if (userData && userData.values && userData.user) {
        if (!(userData.values[0]?.financialProfile && userData.values[0]?.identity_confirmation && userData.values[0]?.Address_confirmation && userData?.values[0]?.email && userData.values[0]?.phone && userData.values[0]?.all_basicInfo && userData.values[0]?.is_bank_account_filled)) {
          ctx.changNotComplete(true);
        } else {
          ctx.changNotComplete(false);
        }
      }
      // maybe change in use auth to avoid duplicate api

    }
  }, [userData]);

  useEffect(() => {
    // get at more last 4 notification --done
    // always sort dec --done
    // if sort last keep dec and just push new --done
    // push last from pusher at more last 4 notification --done

    // first load with dec sort
    if (allNotification && sort == "dec" && Object.keys(anewPusher).length === 0) {
      ctx.changeLastActivities([...allNotification].slice(0, 4))
      // get new pusher 
    } else if (allNotification && sort == "dec" && Object.keys(anewPusher).length != 0) {
      ctx.changeLastActivities([anewPusher, ...allNotification].slice(0, 4))
      // setAnewPusher({})
    } else if (Object.keys(anewPusher).length != 0) {
      ctx.changeLastActivities([anewPusher, ...ctx.lastActiveties].slice(0, 4))
      // setAnewPusher({})
    }
    ctx.changeErrorlastActiveties(errorAllNotification)


  }, [allNotification, anewPusher]);

  useEffect(() => {
    // increase number
    if (Object.keys(anewPusher).length != 0) {
      setLastNotificationNumber(lastNotificationNumber + 1)
      if (sort == "dec") {
        setAllNotificationCollect([anewPusher, ...allNotificationCollect])
      }
    }

  }, [anewPusher])
  //for pusher 
  useEffect(() => {
    Pusher.logToConsole = true;

    const pusher = new Pusher('c568dc6eb5ee1b3a2ad9', {
      cluster: 'ap2',
      encrypted: true

    });

    const channel = pusher.subscribe(`notifications_${localStorage.userId}`);
    channel.bind('Notifications', function (data) {
      // alert("data")
      // allMessages.push(data);
      // setMessages(data);
      var newNotification = data;
      newNotification.created_at = "now";
      newNotification.body = Array.isArray(data.body) ? data.body[lang == "ar" ? 0 : 1][lang] : data.body;
      newNotification.notification_body = Array.isArray(data.body) ? data.body[lang == "ar" ? 0 : 1][lang] : data.body;
      newNotification.notification_image = data.image;
      newNotification.is_read = '0';
      setAnewPusher(newNotification)

      toast.custom((t) => (

        <div
          className={`${t.visible ? 'animate-enter' : 'animate-leave'
            } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5 overflow-hidden`}
        >
          <Link href={generateLink(newNotification)}>
            <div className="flex-1 w-0 p-4">
              <div className="flex items-start gap-4">
                <div className="flex-shrink-0 pt-0.5">
                  <IconSaxName name={newNotification.notification_image} className="w-10 h-10 rounded-full" />
                </div>
                <div className="flex-1 ml-3">
                  <p className="text-sm font-medium text-gray-900">
                    {newNotification.body}
                  </p>
                </div>
              </div>
            </div>
          </Link>
          <div className="flex justify-start border-l border-gray-200">
            <button
              onClick={() => toast.dismiss(t.id)}
              className="flex items-center justify-center w-full p-4 text-4xl font-medium border border-transparent rounded-none rounded-r-lg text-danger h-max"
            >
              <Add size="40" className="transform rotate-45" />
            </button>
          </div>
        </div>
      ))
    });
  }, []);


  useEffect(() => {
    if (+total <= allNotificationCollect.length && !loadingMore) {
      setNoMore(true)
    } else {
      setNoMore(false)
    }
  }, [loadingMore, allNotificationCollect, total])
  useEffect(() => {
    document.body.classList.remove("overflow-hidden")
    setOpenNotification(false)
  }, [router])
  const HandleUpdate = () => {
    if (!loadingMore && +total > allNotificationCollect.length) {
      setLoadingMore(true)
      updateData(sort, pageNotification + 1, (data, total) => {
        setLoadingMore(false)
        setAllNotificationCollect([...allNotificationCollect, ...data]);



        setPageNotification(pageNotification + 1);
        setTotal(total)

      }, () => {
        setLoadingMore(false);
        toast.error("errToast:sorry_a_problem_occurred")
      })
    }
  }
  const handleOpenNotification = () => {
    document.body.classList.toggle("overflow-hidden")
    setOpenNotification(!openNotification);
    if (sort == "dec") {
      convertNotificationToRead({
        success: (res) => {
          setLastNotificationNumber(0)
        },
        error: (err) => {

        },
      })
    }
  }
  const handleConfirmAccount = () => {
    setConfirmAccount(true);
    getPhoneCode({
      success: () => { router.push(`/auth/enter-phone-code?phone=${ctx.userInfo.phone_field}&verify=true`);setConfirmAccount(false); },
      error: () => { setConfirmAccount(false);},
      phone: `${ctx.userInfo.phone_field}`
    })

    
  }
  const handleLogout = () => {
    setLoadingLogout(true);
    logout({
      success: () => {  setLoadingLogout(false); },
      error: () => { setLoadingLogout(false);}
    })
  }
  return (
    <React.Fragment>
      <div className="flex-grow text-black bg-white dark:bg-dark-white dark:text-white grid-area-home-top items-center">
        <div className="p-4 mx-auto container2">
          <div className="flex items-center justify-end xl:justify-between">
            <div className=" items-center justify-center xl:hidden flex rtl:ml-auto ltr:mr-auto w-36">
              <Link href={"/dashboard"}>
                <a>
                  <Image src={logo} alt="logo" layout="intrinsic" className='object-contain  w-full' />
                </a>
              </Link>
            </div>
            <SearchSite className="hidden xl:block" />
            <ul className="flex items-start gap-4">
              <DarkThemeButton className="hidden xl:block " />
              <ChangeLangButton className="hidden xl:block " />
              <li className="relative ">
                <button className="relative flex items-start justify-center w-10 h-10 icon-container" onClick={handleOpenNotification}>
                  <Notification className="[color:rgb(var(--primary-color))] w-7 h-6" />
                  <span className={`${lastNotificationNumber === 0 && "hidden"} absolute flex items-center justify-center w-8 h-8 text-white transform rounded-full rtl:-right-3/4 ltr:-left-3/4 top-1/3 rtl:-translate-x-1/2 ltr:translate-x-1/2 bg-danger`}>
                    {loadingNotificationNumber ? <Loader size="xs" />
                      : errorNotificationNumber ? "!"
                        : notificationNumber && +lastNotificationNumber > 99 ? '+99' : lastNotificationNumber}
                  </span>
                </button>
                <div className={`${!openNotification ? " max-h-0" : " max-h-[calc(100vh_-_7rem)]"}  overflow-hidden transition-[max-height] ease-in-out delay-150 duration-100 md:absolute fixed md:top-12 top-16 ltr:right-0 rtl:left-0 z-10  rounded-xl  text-white  overflow-y-auto w-[25rem] bg-white dark:bg-dark-white`} ref={notificationList}>
                  <div className="flex items-center justify-between px-4 mb-6 text-black lg:px-6 dark:text-white">
                    <h4 className="text-2xl ">{t("notifications")}</h4>
                    <select className={`block w-[9.375rem]  px-4 py-4  rounded-md focus:outline-0 bg-secondary dark:bg-dark-secondary`} value={sort} onChange={(e) => { setSort(e.target.value); setPageNotification(1); setNoMore(false) }}>
                      <option value="dec">{t("the_most_recent")}</option>
                      <option value="asc">{t("the_oldest")}</option>
                    </select>
                  </div>
                  <ul className="p-0 m-0 text-black dark:text-white" >
                    <NotificationItems allNotificationCollect={allNotificationCollect} errorAllNotification={errorAllNotification} loading={loadingNotification} activity={false} />
                    {(!loadingNotification && !errorAllNotification) &&
                      <React.Fragment>
                        {!noMore ?
                          <ButtonTheme onClick={HandleUpdate} color="primary" as="button" type="submit" size="xs" className="mx-4 my-6 text-center xs:my-4 w-[calc(100%_-_2rem)]" loading={loadingMore}>
                            {t('loading_more')}
                          </ButtonTheme> :
                          allNotificationCollect.length > 10 && <span className="block text-center">{t("no_more_notifications")}</span>}
                      </React.Fragment>
                    }
                  </ul>
                </div>
              </li>
              <li className="hidden leading-none xl:block">
                {ctx.userInfo.userImage ?
                  <div className="relative w-10 h-10 rounded-xl bg-gray-400">
                    <Image src={`${process.env.hostImage}/${ctx.userInfo.userImage}`} layout="fill" alt="profile" className="rounded-xl object-cover" />
                  </div>
                  :
                  <div className="flex items-center justify-center w-10 h-10 [background:rgba(var(--primary-color),1)] rounded-xl" >
                    <Profile className="text-white " size="30" />
                  </div>
                }
              </li>
              <li className="leading-none xl:hidden">
                <button className="relative flex items-start justify-center w-10 h-10 icon-container" onClick={() => openAsideHandler()}>
                  <HambergerMenu className="[color:rgb(var(--primary-color))] " />
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {openNotification && <BackOverlay onClick={() => { setOpenNotification(false); document.body.classList.remove("overflow-hidden") }} />}
      <WarningModal closeButton={false} backdrop="static" open={enterPhoneOrLogoutModel} size="lg" onClose={() => { }} message={
        <React.Fragment>
          <p className="mb-2 text-xl font-bold text-black dark:text-white">{t("sorry_your_account_is_not_verified_yet")}</p>
          <div className="flex my-8 justify-between p-1 gap-4">
            <ButtonTheme as="button" onClick={handleConfirmAccount} color="primary" className="w-1/2 px-4 py-2" loading={confirmAccount}>{t("confirm_number")}</ButtonTheme>
            <ButtonTheme as="button" onClick={handleLogout} color="primary" outline className="w-1/2 px-4 py-2" loading={loadingLogout}>{t("aside:sign_out")}</ButtonTheme>
          </div>
        </React.Fragment>}
      />
    </React.Fragment>
  )
}
export default React.memo(TopNav);