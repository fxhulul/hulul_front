import React, { useState, useEffect } from 'react'
import { Field, ErrorMessage, useField } from "formik";
import PhoneInput from 'react-phone-input-2'
import useTranslation from 'next-translate/useTranslation'
import { Checkbox, CheckboxGroup, SelectPicker, Uploader, Loader, Avatar, DatePicker } from 'rsuite';
import { CopyToClip } from "@/ui"
import { getCurrentCountry } from "utils/apiHandle"
import { ArrowDown2, ArrowUp2, GalleryAdd, Calendar, DocumentUpload, Gallery } from 'iconsax-react';
import Image from "next/image"
import toast from "react-hot-toast";
import { dateFns } from "date-fns"
import { isAfter, format } from 'date-fns'
import useWindowSize from "hooks/use-window";
import { detectJsonLangForPhoneInput } from "utils/funcs"
import ReCAPTCHA from "react-google-recaptcha";
import axios from "utils/axios";
import { Global } from 'iconsax-react';

function Input({ noMarginBottom, border, ...props }) {
    const { t, lang } = useTranslation("")
    const [dir, setDir] = useState(props.dir ? props.dir : lang === "ar" ? "rtl" : "ltr")
    const autoDir = (e) => {
        if (e.target.value.charCodeAt(0) < 200) {
            //above 200 start english charackter
            if (!props.dir) {
                setDir("ltr")
            }
        } else {
            if (!props.dir) {
                setDir("rtl")
            }
            // this.style.direction = "rtl";
        }
    };
    return (
        <div className={`${!noMarginBottom && "mb-3 md:mb-6"} ${props.className}`} onKeyUp={autoDir}>
            <Field {...props} className={`block w-full  px-4 py-4  rounded-md ${!border ? "bg-secondary dark:bg-dark-secondary" : "bg-transparent border border-gray-600 text-xs"}  focus:outline-0 ${props.className}`} dir={dir}>
                {props.children}
            </Field>
            <ErrorMessage name={props.name} component="span" className="mt-2 text-sm text-danger md:mt-4 md:text-md" />
        </div>
    )
}
function InputCardNumber({ noMarginBottom, border, ...props }) {
    const [field, meta, helpers] = useField(props.name);
    const [value, setValue] = useState('')
    const handleChang = (e) => {
        if ((/[0-9]/.test(e.target.value))) {
            var joy = e.target.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1').replace(/\s/g, '').match(/.{1,4}/g);
            if (e.target.value.replace(/\s/g, '').length > 16) {
                joy = e.target.value.replace(/[^0-9.]/g, '').slice(0, 16).replace(/(\..*?)\..*/g, '$1').replace(/\s/g, '').match(/.{1,4}/g)
            }
            setValue(joy.join(' '))
        } else {
            setValue('')
        }
        helpers.setValue(e.target.value);
    }
    return (
        <div className={`${!noMarginBottom && "mb-3 md:mb-6"} ${props.className}`} >
            <input {...props} className={`block w-full  px-4 py-4  rounded-md ${!border ? "bg-secondary dark:bg-dark-secondary" : "bg-transparent border border-gray-600 text-xs"}  focus:outline-0 ${props.className}`} dir={'ltr'} onChange={(e) => handleChang(e)} value={value}>
                {props.children}
            </input>
            <ErrorMessage name={props.name} component="span" className="mt-2 text-sm text-danger md:mt-4 md:text-md" />
        </div>
    )
}
function InputIcon(props) {
    return (
        <div className={`relative input-with-icon ${props.className}`}>
            <div className="absolute w-5 rtl:right-4 ltr:left-4 top-4 z-1">
                {props.icon}
            </div>
            {props.children}
        </div>
    )
}
function InputDate(props) {
    const { t, lang } = useTranslation("record");
    const [field, meta, helpers] = useField(props.name);

    return (
        <div className={`relative input-with-icon ${props.className}`}>
            <div className="absolute w-5 rtl:right-4 ltr:left-4 top-4 z-1">
                {props.icon}
            </div>
            <div className={`mb-3 md:mb-6`}>
                <div className="relative flex items-center gap-2 p-2 rounded-lg bg-secondary dark:bg-dark-secondary  ">
                    <DatePicker oneTap disabledDate={date => isAfter(date, new Date())} caretAs={"span"} appearance="subtle" cleanable={false} isoWeek={true} showWeekNumbers={true} locale={{ today: t("the_today"), yesterday: t("yesterday"), last7Days: t("last7Days"), ok: t("ok") }} placement={lang === "ar" ? "bottomEnd" : "bottomStart"} onChange={(date) => { helpers.setValue(format(date, 'yyyy-MM-dd')) }} defaultValue={props.defaultValue} />
                    <div className="p-2 rounded-lg pointer-events-none [background:rgba(var(--primary-color),1)] z-5">
                        <Calendar className="text-white" size="30" />
                    </div>
                </div>
                <ErrorMessage name={props.name} component="span" className="mt-2 text-sm text-danger md:mt-4 md:text-md" />
            </div>
        </div>
    )
}
function InputDate2(props) {
    const { t, lang } = useTranslation("common");
    const [field, meta, helpers] = useField(props.name);
    const [daysOption, setDaysOption] = useState(Array.from({ length: 30 }, (_, i) => i + 1))
    const [day, setDay] = useState(props.defaultValue ? props.defaultValue.getDate() : "")
    const [month, setMonth] = useState(props.defaultValue ? props.defaultValue.getMonth() + 1 : "")
    const [year, setYear] = useState(props.defaultValue ? props.defaultValue.getFullYear() : "")
    useEffect(() => {
        if (day && month && year) {
            if (new Date(`${month}-${day}-${year}`) == "Invalid Date") {
                helpers.setError(t("invalid_date"))
            } else {
                helpers.setValue(format(new Date(`${month}-${day}-${year}`), 'yyyy-MM-dd'))
            }

        } else {
            helpers.setError(t("invalid_date"));
            helpers.setValue('')

        }
    }, [day, month, year, daysOption])
    useEffect(() => {
        if (month === '1' || month === '3' || month === '5' || month === '7' || month === '8' || month === '10' || month === '12') {
            setDaysOption(Array.from({ length: 31 }, (_, i) => i + 1))
        } else if (month === '4' || month === '6' || month === '9' || month === '11') {
            setDaysOption(Array.from({ length: 30 }, (_, i) => i + 1))
        } else if (month === '2') {
            if ((year % 4 === 0 && year % 100 !== 0) || (year % 4 === 0 && year % 100 === 0 && year % 400 === 0)) {
                setDaysOption(Array.from({ length: 29 }, (_, i) => i + 1))
            } else {
                setDaysOption(Array.from({ length: 28 }, (_, i) => i + 1))
            }
        } else {
            setDaysOption(Array.from({ length: 30 }, (_, i) => i + 1))

        }
    }, [month, year])
    useEffect(() => {
        if (day > daysOption.length) {
            setDay('')
        }
    }, [daysOption])
    return (
        <div className={`${!props.noMarginBottom && "mb-3 md:mb-6"} ${props.className}`} >
            <div className="relative flex items-center gap-2 p-2 rounded-lg bg-secondary dark:bg-dark-secondary justify-end  ">
                <span className="absolute text-gray-600 rtl:right-12 ltr:left-12 top-4">{props.placeholder}</span>
                <div className="grid-cols-3	grid gap-4 justify-between sm:justify-start w-full sm:w-[60%]		 mt-10 mb-2 sm:m-0">
                    <SelectDate placeHolder={t("day")} value={day} handleChange={(e) => setDay(e.target.value)} >
                        {daysOption.map((day, index) => (
                            <option key={day} value={day}>
                                {day}
                            </option>
                        ))}
                    </SelectDate>
                    <SelectDate placeHolder={t("month")} value={month} handleChange={(e) => setMonth(e.target.value)} >
                        {Array.from({ length: 12 }, (_, i) => i + 1).map((month, index) => (
                            <option key={month} value={month}>
                                {month}
                            </option>
                        ))}
                    </SelectDate>
                    <SelectDate placeHolder={t("year")} value={year} handleChange={(e) => setYear(e.target.value)} >
                        {Array.from({ length: new Date().getFullYear() - 1950 }, (_, i) => i + 1940).map((year, index) => (
                            <option key={year} value={year}>
                                {year}
                            </option>
                        ))}
                    </SelectDate>
                </div>
            </div>
            {meta.error && <span className="mt-2 text-sm text-danger md:mt-4 md:text-md">{meta.error}</span>}
        </div>

    )
}

function InputCheck({ name, text }) {
    const [field, meta, helpers] = useField(name);

    const handleCheckAll = (value, checked) => { helpers.setValue(checked); };
    return (
        <React.Fragment>
            <label className="flex gap-2">
                <Checkbox onChange={handleCheckAll}> {text}</Checkbox>
                {/* <Field type="checkbox" name={name} />
                <span className="text-xs">{text}</span> */}
            </label>
            <ErrorMessage name={name} component="span" className="mt-2 text-sm text-danger md:mt-4 md:text-md" />
        </React.Fragment>
    )
}
function CustumnCheckbox({ name, text, value, type, color, number, ...props }) {
    // name : name input
    // text : text inside (label)
    // value : value input
    // type : type input 
    // color : if input is color
    // number : if input is number
    return (
        <div className={`relative ${color && "aspect-square"} ${props.className}`}>
            <Field name={name} type={type} value={value} className="absolute top-0 right-0 w-full h-full opacity-0 peer text-black dark:text-white" />
            <div className={`${color ? "bg-color" : !number && 'bg-secondary dark:bg-dark-secondary '} text-black dark:text-white  rounded-lg flex items-center justify-center  font-bold border-2  ${color ? "peer-checked:ring-offset-2 peer-checked:ring-2 peer-checked:ring-[rgba(var(--primary-color),1)]" : number ? "[border-color:rgba(var(--primary-color),1)] border  [color:rgb(var(--primary-color))] peer-checked:[background:rgba(var(--primary-color),1)] peer-checked:text-white px-4 py-2" : "peer-checked:border-2 peer-checked:[border-color:rgba(var(--primary-color),1)] peer-checked:[color:rgb(var(--primary-color))] "} ${!number && "h-full p-6 border-transparent"}  `} style={{ "--color": color }}>
                {text ? text : number ? `${value}$` : ""}
            </div>
        </div >
    )
}
function InputRecaptcha({ name }) {
    const [field, meta, helpers] = useField(name);

    function onChange(value) {
        helpers.setValue(value);
    }
    return (
        <div className='mt-2'>
            <ReCAPTCHA
                sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
                // sitekey="6LdMTzoiAAAAABwKVOU7DXp4HNY6_pWcabIwXA6P"
                onChange={onChange}
            />
            <ErrorMessage name={name} component="span" className="block mb-4 text-danger " />
        </div>

    )
}
function SelectWIthHead({ name, head, options, defaultValue, optionsOutside, ...props }) {
    const [field, meta, helpers] = useField(name);
    const [defaultValueAfter, setDefaultValueAfter] = useState(defaultValue)
    useEffect(() => {
        setDefaultValueAfter(defaultValue);
    }, [name, defaultValue])
    const { t, lang } = useTranslation("dashboard");
    const detectData = (role) => {
        if (role === "currency") {

            return [{
                "value": "USD",
                "label": `${t("american_dollar")} (USD)`,
            },
                {
                "value": "EUR",
                "label": `${t("euro")} (EUR)`,
                },
                // {
                //     "value": "",
                //     "label": `جنيه استرالي`,
                // },
                // {
                //     "value": "",
                //     "label": `دولار كندي`,
                // },
                // {
                //     "value": "",
                //     "label": `دولار استرالي`,
                // },

            ]
        }
        if (role === "leverage") {
            return ['20', '25', '33', '40', '50', '66', '75', '80', '100', '125', '150', '175', '200', '300', '400', '500'].map(function (item) {
                return { "value": item, "label": `1:${item}` }
            })
        }
        if (role === "representative_position") {
            return [

                {
                    "value": "owner",
                    "label": t("profile:owner"),
                },
                {
                    "value": "Higher administration",
                    "label": t("profile:higher_administration"),
                },

                {
                    "value": "Medium management / head of the department",
                    "label": t("profile:medium_management_head_of_the_department"),
                },
                {
                    "value": "Executive Director / Deputy Director",
                    "label": t("profile:executive_director_deputy_director"),
                },
                {
                    "value": "employee",
                    "label": t("profile:employee"),
                },


            ]
        }
        if (role === "select-study") {
            return [...optionsOutside]
        }
        if (role === "select-study-level") {
            return [
                {
                    "label": t("profile:initials"),
                    "value": `initials`,
                },
                {
                    "label": t("profile:preparatory"),
                    "value": `preparatory`,
                },
                {
                    "label": t("profile:fetal"),
                    "value": `fetal`,
                },
                {
                    "label": t("profile:university"),
                    "value": `university`,
                },
                {
                    "label": t("profile:masters"),
                    "value": `masters`,
                },
                {
                    "label": t("profile:doctorate"),
                    "value": `doctorate`,
                },
                {
                    "label": t("profile:no_certificate"),
                    "value": `no_certificate`,
                },


            ]
        }


    }
    return (
        <React.Fragment>
            <div className={`relative flex justify-between py-3 mb-4 bg-secondary dark:bg-dark-secondary  rounded-xl ${props.className} ${!head && "select-with-head-no-head"}`}>
                {head && <span className="absolute font-bold transform -translate-y-1/2 pointer-events-none select-none z-6 top-1/2 right-4 text-black dark:text-white">{head}</span>}
                <SelectPicker name={name} data={detectData(options)} appearance="subtle" searchable={props.searchable ? true : false} cleanable={false} className="w-full" onSelect={(value) => helpers.setValue(value)} value={defaultValueAfter} locale={{ placeholder: t("common:choose"), noResultsText: t("common:there_are_no_results"), searchPlaceholder: t("common:search") }} />
            </div>
            <ErrorMessage name={name} component="span" className="block mb-4 text-danger " />
        </React.Fragment>

    )
}
function CustomnCheckColors({ name, more }) {
    const size = useWindowSize();
    const { t, lang } = useTranslation("dashboard");
    const [openMore, setOpenMore] = useState(more || false);
    const colors = [
        "#3498DB",
        "#8E44AD",
        "#2980B9",
        "#2ECC71",
        "#d6a913",
        "#290009",
        "#34495e",
        "#ff7675",
        "#e84393",
        "#00b894",
        "#6c5ce7"
    ]


    return (
        <React.Fragment>
            <h2 className="mb-2 text-lg text-gray-500">{t("choose_the_account_color")}</h2>
            <div className="grid items-center justify-between lg:grid-cols-6 grid-cols-4 gap-4 mb-4">
                {colors.slice(0, size.width > process.env.lg ? 5 : 3).map((color, index) => (
                    <CustumnCheckbox name={name} value={color} color={color} type="radio" key={index} />
                ))}
                <div className={`relative aspect-square`}>
                    <span tabIndex="-1" className={` rounded-xl  items-center justify-center border-2 h-full flex flex-col gap-2 w-full select-none cursor-pointer text-black dark:text-white`} onClick={() => setOpenMore(!openMore)}>
                        <span className="hidden md:block">{openMore ? t("less") : t("more")}</span>
                        {openMore ? <ArrowUp2 size="20" className="text-gray-600" /> : <ArrowDown2 size="20" className="text-gray-600" />}
                    </span>
                </div >
                {openMore && colors.slice(size.width > process.env.lg ? -6 : -8).map((color, index) => (
                    <CustumnCheckbox name={name} value={color} color={color} type="radio" key={index} />
                ))}

            </div>
            <ErrorMessage name={name} component="span" className="text-danger" />
        </React.Fragment>

    )
}
function InputPhone(props) {
    const { t, lang } = useTranslation("common")
    const [field, meta, helpers] = useField(props.name);
    const [initialCountryCode, setInitialCountryCode] = useState("")
    const [phone, setPhone] = useState("")
    // console.log('defaultValue', props.defaultValue)
    useEffect(() => {
        if (!props.defaultValue) {
            getCurrentCountry({
                success: (response) => {
                    setInitialCountryCode(response.data.CurrentCountry.countryCode);
                },
                error: () => { setInitialCountryCode("") }
            })
        } else {
            setPhone(props.defaultValue)
        }
    }, [props.defaultValue])
    return (
        <div className="mb-3 md:mb-6">
            <div className="relative">
                <PhoneInput
                    className={`block w-full md:p-4 px-4 py-4   rounded-md bg-secondary dark:bg-dark-secondary  ${props.className}`}
                    country={initialCountryCode.toLowerCase()}
                    enableSearch={true}
                    containerClass={'block w-full md:p-4 px-4 py-4  rounded-md bg-secondary dark:bg-dark-secondary  flex justify-between'}
                    placeholder={""}
                    localization={detectJsonLangForPhoneInput(lang)}
                    searchPlaceholder={t('write_the_name_of_the_state')}
                    value={phone}
                    onChange={phone => { helpers.setValue(phone); setPhone(phone) }}
                />
                {!props.defaultValue && <span className="select-none absolute top-1/2 right-[3rem] transform -translate-y-1/2 text-sm text-[#9ca3af]">{props.placeholder}</span>}
            </div>
            <ErrorMessage name={props.name} component="span" className="mt-2 text-sm text-danger md:mt-4 md:text-md" />
        </div>
    )
}
function InputCountry(props) {
    const { t, lang } = useTranslation("common")
    const [phone, setPhone] = useState("")
    const [country, setCountry] = useState("")
    const [countryNameToApi, setCountryNameToApi] = useState("")
    const [cityName, setCityName] = useState("")
    const [initialCountryCode, setInitialCountryCode] = useState("")
    const [field, meta, helpers] = useField(props.name);
    const [optionsCities, setOptionsCities] = useState([])
    useEffect(() => {
        if (!props.defaultValue) {
            if (!props.notGetApi) {
                getCurrentCountry({
                    success: (response) => {
                        const countryCode = response.data.CurrentCountry.countryCode
                        const countryName = response.data.CurrentCountry.countryName
                        setCountryNameToApi(response.data.CurrentCountry.countryName)
                        setInitialCountryCode(countryCode);
                        setCountry(detectJsonLangForPhoneInput(lang)[countryCode.toLowerCase()]);
                        !props.city && helpers.setValue(`${countryName}-${countryCode.toLowerCase()}`)
                    },
                    error: () => { setInitialCountryCode() }
                })
            }
        } else {
            setCountry(lang != 'en' ? detectJsonLangForPhoneInput(lang)[props.defaultValue.split("-")[1]] : props.defaultValue.split("-")[0]);
            setCountryNameToApi(props.defaultValue.split("-")[0]);
            setCityName(props.defaultValue.split("-")[2]);
            setOptionsCities([{ label: props.defaultValue.split("-").slice(2).join("-"), value: props.defaultValue.split("-").slice(2).join("-") }])
            props.defaultValue.split("-").length >= 2 && setInitialCountryCode(props.defaultValue.split("-")[1].toLowerCase())
        }
    }, [])
    useEffect(() => {
        if (initialCountryCode && props.city) {
            axios.post("https://countriesnow.space/api/v0.1/countries/cities", { "iso2": initialCountryCode.toLowerCase() }).then((res) => {
                setOptionsCities([...res.data.data.map(
                    item => ({ label: item, value: item })
                )])
            }
            )
        }
    }, [initialCountryCode])
    const handleOnChange = (value, data, event, formattedValue) => {
        setPhone(value.slice(data.dialCode.length));
        if (props.city) {
            helpers.setValue(``);
        } else {
            helpers.setValue(`${data.name}-${data.countryCode}`)
        }
        setCountryNameToApi(data.name)
        setInitialCountryCode(data.countryCode)
        setCountry(detectJsonLangForPhoneInput(lang)[data.countryCode])
    }
    const handleChangeCity = (value) => {
        helpers.setValue(`${countryNameToApi}-${initialCountryCode}-${value}`)
        setCityName(value)
    }
    return (
        <div className="mb-3 md:mb-6">
            <div className="relative">
                <PhoneInput
                    country={(props.defaultValue && props.defaultValue.split("-").length === 2) ? props.defaultValue.split("-")[1].toLowerCase() : initialCountryCode && initialCountryCode.toLowerCase()}
                    enableSearch={true}
                    containerClass={'block w-full md:p-4 px-4 py-4  rounded-md bg-secondary dark:bg-dark-secondary  flex justify-between city'}
                    placeholder={" "}
                    countryCodeEditable={false}
                    searchPlaceholder={t('write_the_name_of_the_state')}
                    value={phone}
                    onChange={handleOnChange}
                    localization={detectJsonLangForPhoneInput(lang)}
                />
                <span {...props} placeholder="" className={`absolute bg-transparent top-4 width-city focus:outline-0 rtl:right-0 ltr:left-0 rtl:text-left ltr:text-right`} dir="auto" onClick={(e) => e.target.parentElement.parentElement.querySelector(".selected-flag").click()} >
                    {country}
                </span>
                <span className={`select-none absolute top-1/2 rtl:right-[3rem] ltr:left-[3rem] transform -translate-y-1/2 text-sm text-[#9ca3af]`}>{props.placeholder}</span>
            </div>
            <ErrorMessage name={props.name} component="span" className="mt-2 text-sm text-danger md:mt-4 md:text-md" />
            {(props.city && optionsCities) &&
                <div>
                    <div className={`relative flex justify-between py-3 my-4 bg-secondary dark:bg-dark-secondary  rounded-xl`}>
                        <span className=" text-sm absolute  transform -translate-y-1/2 pointer-events-none select-none z-1 top-1/2 right-4 text-[#9ca3af]">{t("common:city")}</span>
                        <SelectPicker value={cityName} data={optionsCities} appearance="subtle" searchable={true} cleanable={false} className="w-full" onSelect={handleChangeCity} locale={{ placeholder: t("common:choose"), noResultsText: t("common:there_are_no_results"), searchPlaceholder: t("common:search") }} />
                    </div>
                </div>
            }
        </div >

    )
}
const CustomnBalance = ({ name }) => {
    const { t, lang } = useTranslation("dashboard")
    const [field, meta, helpers] = useField(name);
    const [input, setInput] = useState('')
    const [check, setCheck] = useState()
    const values = [
        '1000', '3000', '5000', '10000', '25000', '50000'
    ]
    const handleChange = (e) => {
        helpers.setValue(e.target.value);
        setInput(e.target.value)
        setCheck("")

    }
    const handleChangeCheck = (e) => {
        setInput("")
        setCheck(e.target.value)
        helpers.setValue(e.target.value);
    }
    return (
        <React.Fragment>
            <div className="p-4 border">
                <div className="grid md:grid-cols-3 grid-cols-2 gap-4 mb-4">
                    {values.map((value, index) => (
                        <div className={`relative `} key={index}>
                            <input name={name} type="radio" value={value} className="absolute top-0 right-0 w-full h-full opacity-0 peer" onChange={handleChangeCheck} checked={check === value} />
                            <div className={`rounded-lg flex items-center justify-center  font-bold border-2  [border-color:rgba(var(--primary-color),1)]   [color:rgb(var(--primary-color))] peer-checked:[background:rgba(var(--primary-color),1)] peer-checked:text-white px-4 py-2 `}>
                                {`${value}$`}
                            </div>
                        </div >
                    ))}
                </div>
                <input className={`block w-full  px-4 py-4  rounded-md bg-secondary dark:bg-dark-secondary  `} placeholder={t("another_sum")} name="Balance" type="number" onChange={handleChange} value={input} />

            </div>
            <ErrorMessage name={name} component="span" className="text-danger" />
        </React.Fragment>
    )
}
const UploadImage = ({ name, defaultImg }) => {
    const { t, lang } = useTranslation("dashboard")
    const [field, meta, helpers] = useField(name);
    const [uploading, setUploading] = useState(false);
    const [fileInfo, setFileInfo] = useState(null);
    function previewFile(file, callback) {
        const reader = new FileReader();
        reader.onloadend = () => {
            callback(reader.result);
        };
        reader.readAsDataURL(file);
    }
    return (
        <div className="relative mx-auto upload-image w-max mb-16">
            <Uploader
                fileListVisible={false}
                draggable
                maxPreviewFileSize={5242880}
                listType="picture"
                action={`${process.env.host}/uploading-file-api`}
                onUpload={file => {
                    setUploading(true);
                    previewFile(file.blobFile, value => {
                        setFileInfo(value);
                    });
                }}
                onSuccess={(response) => {
                    setUploading(false);
                    helpers.setValue(response.data.file);

                }}
                onError={() => {
                    setFileInfo(null);
                    setUploading(false);
                    toast.error(t("errToast:sorry_a_problem_has_occurred_in_downloading_the_image"))
                }}
                accept="image/*"
            >
                <span >
                    {uploading && <Loader backdrop center />}
                    {fileInfo ?
                        <div className="relative w-full h-full">
                            <Image src={fileInfo} layout="fill" className="object-cover w-full h-full " alt='image user' />
                        </div>
                        :
                        defaultImg ?
                            <div className="relative w-full h-full">
                                <Image src={`${process.env.hostImage}/${defaultImg}`} layout="fill" className="object-cover w-full h-full " alt='image user default' />
                            </div>
                            :
                            <div className="bg-[#c4c4c4] rounded w-full h-full"></div>

                    }
                </span>
            </Uploader>
            <div className="absolute p-3 border-4 border-white -bottom-4 -right-4 [background:rgba(var(--primary-color),1)] rounded-xl z-3">
                <GalleryAdd className="text-white" size="20" />
            </div>
        </div>
    )
}
const UploadDraggableImage = ({ name, fileName, notDefaultRender, dirty }) => {
    const { t, lang } = useTranslation("common")
    const [field, meta, helpers] = useField(name);
    const [once, setOnce] = useState(false)
    return (
        <div className={`draggable-upload ${once && "pointer-events-none-upload"}`}>
            <Uploader
                action={`${process.env.host}/uploading-file-api`}
                draggable
                accept="image/*"
                maxPreviewFileSize={10485760} //10Mb
                onRemove={() => {
                    helpers.setValue("");
                    setOnce(false)

                }}
                onUpload={() => {
                    setOnce(true)
                }}
                onSuccess={(response) => {
                    helpers.setValue(response.data.file);
                }}
                renderFileInfo={(file, fileElement) => {
                    return (
                        <div className="flex gap-2">
                            <Gallery className="[color:rgb(var(--primary-color))] " />
                            <span>{file.name}</span>
                        </div>
                    );
                }}
                locale={{ error: t("the_lifting_is_not_complete"), complete: t("the_lifting_is_completed") }}
            >
                <div>
                    {!notDefaultRender ?
                        <React.Fragment>
                            <h2 className="font-bold mb-4 text-xl">{fileName}</h2>
                            <DocumentUpload className="[color:rgb(var(--primary-color))]mb-6 lg:w-16 lg:h-16 w-10 h-10" />
                            <p className="text-xs hidden lg:block" >{t("drag_the_file_and_see_it_here_or")}</p>
                            <p className="text-xs" >{t("review_a_search_for_a_file_for_download")}</p>
                        </React.Fragment>
                        :
                        fileName
                    }
                </div>
            </Uploader>
        </div>
    )
}
const InputShow = ({ title, value, clip, gray }) => {
    return (
        <div className="flex justify-between p-4 mb-4 bg-secondary dark:bg-dark-secondary  rounded-xl font-bold text-black dark:text-white">
            {title}
            <span className={`flex items-center gap-2 text-base font-bold ${gray && "text-gray-400 tracking-widest2"}`}>
                {clip ?
                    <React.Fragment>
                        <span>{clip}</span>
                        <CopyToClip text={clip} />
                    </React.Fragment>
                    :
                    <span>{value}</span>
                }
            </span>
        </div>
    )
}
const SelectDate = ({ placeHolder, value, handleChange, ...props }) => {
    return (
        <div className="flex justify-center w-full">
            <div className="relative  [background:rgba(var(--primary-color),0.2)] w-full rounded">
                <select className="px-1 form-select appearance-none block w-full py-1.5 bg-transparent text-black text-base bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:[border-color:rgba(var(--primary-color),1)] focus:outline-none" aria-label="Default select example" value={value} onChange={handleChange}>
                    <option value="">
                        {placeHolder}
                    </option>
                    {props.children}
                </select>
                <span className="absolute top-0 flex items-center justify-start h-full gap-2 rtl:left-1  ltr:right-1">
                    <ArrowDown2 size="18" />
                </span>
            </div>
        </div>
    )
}
export { Input, InputCardNumber, InputIcon, InputPhone, InputCountry, InputCheck, CustumnCheckbox, SelectWIthHead, CustomnCheckColors, CustomnBalance, UploadImage, InputDate, InputDate2, UploadDraggableImage, InputShow, InputRecaptcha }
