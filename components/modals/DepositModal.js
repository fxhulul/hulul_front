import React from 'react'
import { MoneyRecive, Wallet3, CardReceive, MoneySend } from 'iconsax-react'
import useTranslation from 'next-translate/useTranslation'
import { Modal } from 'rsuite';
import ButtonTheme from '@/ui/ButtonTheme';
export default function DepositModal({ open, onClose, id , type }) {
  const { t, lang } = useTranslation("dashboard")

  return (
    <Modal backdrop={true} keyboard={true} open={open} onClose={() => onClose()} className="less-size-modal">
      <Modal.Header>
        <Modal.Title></Modal.Title>
      </Modal.Header>
      <Modal.Body >
        <div className="text-center px-4">
          {type ==="deposit" ?
          <MoneyRecive  className="mx-auto mb-6 [color:rgb(var(--primary-color))] more-linear w-20 h-20 lg:w-[7.5rem] lg:h-[7.5rem]" /> :
          <MoneySend className="mx-auto mb-6 [color:rgb(var(--primary-color))] more-linear w-20 h-20 lg:w-[7.5rem] lg:h-[7.5rem]" /> 
          }
          <p className="mb-8 font-bold text-black dark:text-white">{type ==="deposit" ? t("do_you_want_to_deposit_now") : t("do_you_want_to_draw_now")}</p>
          <ButtonTheme color="primary" size="xs" as="link" href={`/dashboard/real/${id}/${type}`} block className="mb-4 py-3 ">
            <div className="flex gap-2 justify-center items-center ">
              <Wallet3 />
              {type ==="deposit" ?t("deposit_from_my_wallet") : t("draw_from_my_wallet")}
            </div>
          </ButtonTheme>
          <ButtonTheme outline size="xs" color="primary" as="link" href={`/dashboard/${type}/choose-method?account=${id}`} block className="mb-4 py-3 ">
            <div className="flex gap-2 justify-center items-center ">
              <CardReceive />
              {type ==="deposit" ?t("live_deposit"):t("live_draw")}
            </div>
          </ButtonTheme>
          {/* <ButtonTheme color="primary" as="link" href="/" outline block className="py-3 ">
            <div className="flex gap-2 justify-center items-center ">
              <Clock />
              {t("deposit_from_my_wallet")}
            </div>
          </ButtonTheme> */}

        </div>
      </Modal.Body>
    </Modal>

  )
}
