import React from 'react'
import { Information } from 'iconsax-react'
import useTranslation from 'next-translate/useTranslation'
import { Modal } from 'rsuite';

export default function WarningModal({open ,onClose ,message , size,backdrop,closeButton}) {

  return (
    <Modal backdrop={closeButton === false ? "static" :true} keyboard={true} open={open}  onClose={()=>onClose()} className={size =="sm" ? "less-size-modal" : "more-size-modal"}>
    <Modal.Header closeButton={closeButton}>
       <Modal.Title></Modal.Title>
     </Modal.Header>
   <Modal.Body>
   <div className="text-center px-4">
        <Information  className="mx-auto mb-6 text-orange-500 more-linear w-20 h-20 lg:w-36 lg:h-36"/>
       {message}
    </div>
   </Modal.Body>
   <Modal.Footer>
    
   </Modal.Footer>
 </Modal>
    
  )
}
