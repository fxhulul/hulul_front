import React from 'react'
import { Verify } from 'iconsax-react'
import useTranslation from 'next-translate/useTranslation'
import { Modal } from 'rsuite';
export default function DoneModal({open ,onClose , message}) {
  const { t  , lang} = useTranslation("dashboard")
  return (
    <Modal backdrop={true} keyboard={true} open={open} onClose={()=>onClose()} className="more-size-modal">
    <Modal.Header>
       <Modal.Title></Modal.Title>
     </Modal.Header>
   <Modal.Body>
   <div className="text-center">
        <Verify  className="mx-auto mb-6 text-success more-linear w-20 h-20 lg:w-44 lg:h-44"/>
      {message}
    </div>
   </Modal.Body>
   <Modal.Footer>
    
   </Modal.Footer>
 </Modal>
   
  )
}
