import React from 'react'
import { Timer } from 'iconsax-react'
import useTranslation from 'next-translate/useTranslation'
import { Modal } from 'rsuite';
export default function WaitModal({ open, onClose  , message }) {
  const { t  , lang} = useTranslation("dashboard")
  return (
    <Modal backdrop={true} keyboard={true} open={open} onClose={()=>onClose()} className="more-size-modal">
       <Modal.Header>
          <Modal.Title></Modal.Title>
        </Modal.Header>
      <Modal.Body>
        <div className="text-center">
          <Timer className="mx-auto mb-6 text-gray-800 dark:text-white w-20 h-20 lg:w-28 lg:h-28" />
         {message}
        </div>
      </Modal.Body>
      <Modal.Footer>
       
      </Modal.Footer>
    </Modal>

  )
}
