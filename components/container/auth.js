import React, { useEffect } from 'react'
import { LoginBackground } from "public/svg"
import { Swiper, SwiperSlide } from "swiper/react";
import Link from "next/link"
import img0 from "public/images/placeholder/auth-slider-0.png"
import img1 from "public/images/placeholder/auth-slider-1.png"
import img2 from "public/images/placeholder/auth-slider-2.png"
import img3 from "public/images/placeholder/auth-slider-3.png"
import img4 from "public/images/placeholder/auth-slider-4.png"
import img5 from "public/images/placeholder/auth-slider-5.png"
import svgLogoLayer from "public/images/placeholder/svg-logo-layer.svg"
import { Autoplay, Navigation, Pagination } from "swiper";
import useTranslation from 'next-translate/useTranslation'
import SelectLangs from "@/ui/SelectLangs"
import LoginLinks from "@/ui/LoginLinks"
import Image from "next/image"
import logo from "public/images/placeholder/logo.png"
export default function Login({ slider, noLinksButton, noRiskWarning, contactUs, ...props }) {
    //slider props to collect two view for layouts slider & img
    //noLinksButton props to hide LoginLinks
    //noRiskWarning props to hide RiskWarning
    //contactUs props to show contactUs
    const { t, lang } = useTranslation("auth")
    useEffect(() => {
        document.documentElement.classList.remove("dark")
    }, [])

    const swipperInfo = [
        { text: t('sliderText1'), img: img1 },
        { text: t('sliderText1'), img: img2 },
        { text: t('sliderText1'), img: img3 },
        { text: t('sliderText1'), img: img4 },
        { text: t('sliderText1'), img: img5 },
    ]
    return (
        <div className="grid h-screen grid-cols-12 gap-4 auth-slider">
            <div className={`${slider ? "md:col-span-8" : "md:col-span-8"} col-span-12 px-4 sm:px-6 lg:px-20 py-4 md:py-10 flex flex-col justify-between max-w-full m-auto w-[37.5rem] min-h-[calc(100vh_-_1rem)]`}>
                <div className='flex justify-between items-center' >
                    <Link href="/">
                        <a >
                            <Image src={logo} alt="logo" layout="intrinsic" width={150} className="object-contain max-w-[30vw]" />
                        </a>
                    </Link>
                    <SelectLangs />
                </div>
                <div className={props.className}>
                    {props.children}
                </div>
                {!noLinksButton ?
                    <React.Fragment>
                        <LoginLinks />
                        {!noRiskWarning && <p className="mt-2 text-xs text-gray-500">{t('risk_warning_trading_using_financial_lifters_in_the_financial_markets_involved_in_a_very_high_risk_that_does_not_suit_all_types_of_investors_you_must_accommodate_the_size_of_the_risk_that_your_money_may_be_exposed_to')}</p>}
                    </React.Fragment>
                    :
                    <div></div>

                }
                {contactUs && <div className="mt-10 text-center ">
                    <h6>{t('if_you_face_any_difficulty_contact_us')}</h6>
                    <a target="_blank" rel="noreferrer" href="mailto:info@hululfx.com" className="block [color:rgb(var(--primary-color))] ">info@hululfx.com</a>
                    <a target="_blank" rel="noreferrer" href="tel:0092178945612" className="block [color:rgb(var(--primary-color))] ">+92178945612</a>
                </div>}
            </div>
            <div className={`${slider ? "md:col-span-4" : "md:col-span-4"}`}></div>

            <div className={`${slider ? "w-1/3" : "w-1/3"} [background:rgba(var(--primary-color),1)]  hidden md:block h-screen fixed top-0 rtl:left-0 ltr:right-0`}>
                <LoginBackground className="absolute w-full top-[-10%] right-1/3 transform translate-x-1/2 hidden" />
                {slider ?
                    <React.Fragment>
                        <Swiper
                            grabCursor={true}
                            style={{
                                "--swiper-pagination-color": "#fff",
                                "--swiper-pagination-bullet-horizontal-gap": "3px"
                            }}
                            dir={lang === 'ar' ? "rtl" : "ltr"}
                            loop="true"
                            autoplay={{
                                delay: 2500
                            }}
                            navigation={true}
                            pagination={{
                                clickable: true,
                            }}
                            modules={[Autoplay, Navigation, Pagination]}
                            className="mySwiper"
                        >
                            {swipperInfo.map((sw, i) => (
                                <SwiperSlide key={i} className="relative w-100 ">
                                    <div className="before:[background:rgba(var(--primary-color),0.5)] before:top-0 before:right-0 before:w-full before:h-full before:absolute before:z-3"></div>
                                    <Image layout="fill" src={svgLogoLayer} alt="svg logo layer" className="w-100 h-full object-cover opacity-50 z-2" />
                                    <Image layout="fill" src={sw.img} alt={sw.text} className="w-100 h-full object-cover" />
                                    <div className=" z-4 text-base absolute bottom-20 right-1/2 w-[90%] transform translate-x-1/2 text-white">
                                        <span className="text-right w-full block px-4 ">{i + 1 < 10 ? `0${i + 1}` : i + 1}</span><br />
                                        <p className="">
                                            {sw.text}
                                        </p>

                                    </div>
                                </SwiperSlide>
                            ))}
                        </Swiper>

                    </React.Fragment>
                    : <React.Fragment>
                        <div className="before:[background:rgba(var(--primary-color),0.5)] before:top-0 before:right-0 before:w-full before:h-full before:absolute before:z-3">
                            <Image layout="fill" src={svgLogoLayer} alt="svg logo layer" className="w-100 h-full object-cover opacity-50 z-2" />
                            <Image layout="fill" src={img0} alt={"text"} className="w-100 h-full object-cover" />
                        </div>
                    </React.Fragment>}
            </div>
        </div>
    )
}
