import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import DepositDrawContainer from "@/container/deposit-and-draw/DepositDrawContainer"
import { Call, Profile, SearchNormal1 } from 'iconsax-react'
import { InputIcon, Input } from "@/form"

import { AddNewBankModal } from '@/modals'
import { Formik } from "formik";
import { ButtonTheme, Error, Loading, } from "@/ui"
import { CheckUserData, profilePersonalIdentificationConfirmation, profileAddressCheck } from "utils/apiHandle"
import Head from 'next/head'
import profile from "public/images/placeholder/profile.png"
import useSWR, { useSWRConfig } from 'swr'
import { NetellerText } from "public/svg"
import Image from "next/image"
import useAuth from 'hooks/useAuth'
import useWindowSize from "hooks/use-window";
export default function BrokerDepositDraw({ type }) {
    const { user, isLoading } = useAuth({ middleware: 'auth' })
    const { t, lang } = useTranslation("depositAndDraw")
    const [choose, setChoose] = useState(-1)
    const [chooseValue, setChooseValue] = useState()
    const [loadingButton, setLoadingButton] = useState(false)
    const [openAddAccount, setOpenAddAccount] = useState(false)
    const { mutate } = useSWRConfig();
    const size = useWindowSize();
    const onSubmit = (values) => {
        setLoadingButton(true);
       
    }
    const brokers = [
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/sy.svg", country: "سوريا", city: "حلب", number: '+963-998877665', image: "" },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
        { name: "سعيد علي القتابي", flag: "https://flagcdn.com/jo.svg", country: "الأردن", city: "الزرقاء", number: '+963-998877665', image: profile },
    ]
    if (isLoading || !user) {
        return <Loading page={true} />
    }
    return (
        <React.Fragment>
            <Head>
                <title>{type === "deposit" ? t("deposit_from_the_broker") : t("draw_from_the_broker")} | {t("common:website_name")}</title>
            </Head>
            <DepositDrawContainer type={type}>
                <div className="grid gap-8 mb-8 lg:grid-cols-3 sm:grid-cols-2">
                    <select className={`block w-full  px-4 py-4  rounded-md focus:outline-0 bg-secondary dark:bg-dark-secondary`}>
                        <option>Jordan</option>
                    </select>
                    <select className={`block w-full  px-4 py-4  rounded-md focus:outline-0 bg-secondary dark:bg-dark-secondary`}>
                        <option>الزرقاء</option>
                    </select>
                    <InputIcon icon={<SearchNormal1 className="text-gray-400 rounded-lg " />}>
                        <input type="search" placeholder={t('search')} className={`block w-full  px-4 py-4  rounded-md bg-secondary dark:bg-dark-secondary  focus:outline-0 `} />
                    </InputIcon>
                </div>
                <div className="grid gap-3 lg:grid-cols-3 sm:grid-cols-2 lg:gap-6">
                    {brokers.map((broker, index) => (
                        <div className="px-2 py-4 text-center text-black border border-gray-600 rounded-xl lg:px-6 lg:py-8 dark:text-white" key={index}>
                            {broker.image ?
                                <Image src={broker.image} alt={broker.country} width={size.width > process.env.lg ? "70" : "30"} height={size.width > process.env.lg ? "70" : "30"} className="rounded-full" />
                                :
                                <div className={`[background:rgba(var(--primary-color),1)] rounded-full  flex justify-center items-center  mx-auto w-max p-[0.313rem] lg:p-2`} >
                                    <Profile className="text-white" size={size.width > process.env.lg ? "45" : "20"} />
                                </div>
                            }
                            <h3 className="text-sm font-bold">{broker.name}</h3>
                            <div className="flex justify-center gap-2 mb-2 lg:mb-8">
                                <div className="relative w-8 h-6 rounded-lg">
                                    <Image src={broker.flag} alt={broker.country} layout="fill" />
                                </div>
                                <bdi className="text-xs">
                                    <span>{broker.country}</span>
                                    {" "}-{" "}
                                    <span>{broker.city}</span>
                                </bdi>
                            </div>
                            <a target="_blank" href={`http://wa.me/${broker.number}`} rel="noreferrer" >
                                <bdi className="flex items-center justify-center gap-1 text-xs lg:gap-2" >
                                    {broker.number}
                                    <span className="block p-1 bg-white rounded">
                                        <Call className="[color:rgb(var(--primary-color))] " size="15" />
                                    </span>
                                </bdi>
                            </a>
                        </div>
                    ))}
                </div>
            </DepositDrawContainer >
        </React.Fragment>
    )
}
