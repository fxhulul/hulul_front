import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import DepositDrawContainer from "@/container/deposit-and-draw/DepositDrawContainer"
import { Gift } from 'iconsax-react'
import { ErrorMessage } from "formik";
import { Input, InputShow, UploadDraggableImage } from '@/form'
import { Formik } from "formik";
import { ButtonTheme, Error, Loading, } from "@/ui"
import { CheckUserData, depositDrawRealAccount, profileAddressCheck } from "utils/apiHandle"
import Head from 'next/head'
import useSWR, { useSWRConfig } from 'swr'
import { BankAccountStatement } from "public/svg"
import useAuth from 'hooks/useAuth'
import { useRouter } from 'next/router'
import * as Yup from "yup";
import { DoneModal } from "@/modals"
import { getActiveBonus } from "utils/apiHandle"
import validate from 'utils/validate';
export default function BankTransforDepositDraw({ type }) {
    const { user, isLoading } = useAuth({ middleware: 'auth' })
    const { t, lang } = useTranslation("depositAndDraw")
    const [openSuccessModal, setOpenSuccessModal] = useState(false)
    const [loadingButton, setLoadingButton] = useState(false)
    const router = useRouter()
    const onSubmit = (values) => {
        let newValues = values;
        newValues.amount_transferred = +values.amount_transferred + ((1 / +data.bonus.quantity) * values.amount_transferred)
        setLoadingButton(true);
        depositDrawRealAccount({
            values: type === "deposit" ? newValues : values,
            success: () => { setLoadingButton(false); setOpenSuccessModal(true); },
            error: () => setLoadingButton(false)
        })
    }
    const { data, error } = useSWR(getActiveBonus())
    const bankInfo = {
        bank_name: "بنك السعدي الفرنسي",
        the_recipients_name: "مؤسسة حلول للتداول",
        account_number: "01254687988",
        conversion: "دولار امريكي (USD)",
    }
    if (isLoading || !user || !data) {
        return <Loading page={true} />
    }
    return (
        <React.Fragment>
            <Head>
                <title>{type === "deposit" ? t("deposit_to_bank_transfer") : t("draw_to_bank_transfer")} | {t("common:website_name")}</title>
            </Head>
            <DepositDrawContainer type={type}>
                <Formik

                    initialValues={
                        type === "deposit" ?
                            { account_id: router.query.account, type: "Deposit", bank_name: bankInfo.bank_name, recipient_name: bankInfo.the_recipients_name, account_number: bankInfo.account_number, transfer_currency: bankInfo.conversion, Remittance_notices: "", amount_transferred: "" }
                            :
                            { account_id: router.query.account, type: "Withdraw", bank_name: "", recipient_name: "", account_number: "", transfer_currency: "", amount_transferred: "" }
                    }
                    validationSchema={() => Yup.object().shape({
                        bank_name: validate.Bank_name,
                        recipient_name: validate.recipient_name,
                        account_number: validate.account_number,
                        transfer_currency: validate.transfer_currency,
                        amount_transferred: validate.amount_transferred,
                        Remittance_notices: type === "deposit" && validate.Remittance_notices,
                    })}
                    onSubmit={onSubmit} >
                    {(props) => {
                        return (
                            <form onSubmit={props.handleSubmit}>
                                <div className="mb-8 lg:px-8">

                                    {type === "deposit" ?
                                        <React.Fragment>
                                            <InputShow title={t("bank_name")} value={bankInfo.bank_name} />
                                            <InputShow title={t("the_recipients_name")} clip={bankInfo.the_recipients_name} />
                                            <InputShow title={t("account_number")} clip={bankInfo.account_number} />
                                            <InputShow title={t("the_recipients_name")} value={bankInfo.conversion} />
                                            <UploadDraggableImage name="Remittance_notices" fileName={<div className="flex items-center gap-4 [color:rgb(var(--primary-color))] "><BankAccountStatement border="rgb(var(--primary-color))" className="w-10 h-10 text-secondary dark:text-dark-secondary lg:w-20 lg:h-20" />{t("raise_the_hawk_poems")}</div>} dirty={props.dirty} notDefaultRender />
                                            <ErrorMessage name="Remittance_notices" component="span" className="mt-2 text-sm text-danger md:mt-4 md:text-md" />

                                        </React.Fragment>
                                        :
                                        <React.Fragment>
                                            <Input name="bank_name" type="text" placeholder={t("bank_name")} />
                                            <Input name="recipient_name" type="text" placeholder={t("the_recipients_name")} />
                                            <Input name="account_number" type="text" placeholder={t("account_number")} />
                                            <Input name="transfer_currency" type="text" placeholder={t("conversion")} />

                                        </React.Fragment>
                                    }
                                </div>
                                <h3 className="mb-2 text-base text-gray-600">{type === "deposit" ? t("enter_the_amount_converted") : t("enter_the_amount_you_want_to_draw")}</h3>
                                <div className="flex py-1 mb-4 rounded-xl ltr:pr-1 rtl:pl-1">
                                    <Input name="amount_transferred" type="number" dir={lang === "ar" ? "rtl" : "ltr"} noMarginBottom className="relative mr-2 grow" />
                                    {/* <span className="grid items-center justify-center grid-cols-1 px-6 text-center text-gray-400 bg-white dark:bg-dark-white lg:grid-cols-2 rtl:rounded-l-xl ltr:rounded-r-xl "><span className="whitespace-nowrap ">{t("you_will_get")}</span> <bdi>{props.values.amount_transferred && props.values.amount_transferred - +data.bonus.quantity}$</bdi></span> */}
                                </div>
                                {(type === "deposit" && props.values.amount_transferred) && <bdi className="flex items-center gap-2 mb-4 text-xs">
                                    <Gift className="[color:rgb(var(--primary-color))] " size="20" />
                                    <span className=" font-bold text-[0.875rem]">{props.values.amount_transferred && +props.values.amount_transferred + ((1 / +data.bonus.quantity) * props.values.amount_transferred)}</span>
                                    <span>+</span>
                                    <span>{+data.bonus.quantity}% {t("gift")}</span>
                                </bdi>}
                                <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-8 text-center" loading={loadingButton} disabled={!props.dirty} >
                                    {t('send_order')}
                                </ButtonTheme>
                            </form>
                        )
                    }}
                </Formik>
            </DepositDrawContainer >
            <DoneModal open={openSuccessModal} onClose={() => setOpenSuccessModal(false)} message={<React.Fragment>
                <p className="mb-8 text-lg md:px-8">{type === "deposit" ? t("al_adman_will_be_reviewed_within_a_few_hours_if_the_operation_is_successful_the_amount_will_be_deposited_in_your_portfolio") : t("al_adman_will_be_reviewed_within_a_few_hours_if_the_operation_is_successful_the_amount_will_be_drawed_in_your_portfolio")}</p>
                <p className="text-lg">{t("and_if_there_is_a_problem_we_will_communicate_with_you")}</p>
            </React.Fragment>
            } />

        </React.Fragment>
    )
}
