import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ArrangeHorizontal, ArrowLeft, ArrowRight2 } from 'iconsax-react';
import Link from "next/link"
import { ButtonTheme, Slider2, Error, Loading, NoData } from "@/ui"
import { CustomnBalance } from "@/form"
import * as Yup from "yup";
import { Formik, Form } from "formik";
import { useRouter } from "next/router"
import validate from 'utils/validate';

export default function ConvertToContainer({ type, data, error }) {
    const { t, lang } = useTranslation("dashboard")
    const [currentFirstAccount, setCurrentFirstAccount] = useState()
    const [currentFirstId, setCurrentFirstId] = useState()
    const [allFirstAccounts, setAllFirstAccounts] = useState()
    const [currentSecondAccount, setCurrentSecondAccount] = useState()
    const [currentSecondId, setCurrentSecondId] = useState()
    const [allSecondAccounts, setAllSecondAccounts] = useState()
    const [loadingButton, setLoadingButton] = useState(false)
    const router = useRouter();
    const onSubmit = (values) => {
        setLoadingButton(true);
        setTimeout(() => {
            setLoadingButton(false);
        }, [1000])
        // changeDemoAccountSetting({
        //     values: values,
        //     success: (response) => {
        //         setLoadingButton(false);
        //         setAllAccounts(allAccounts.map((account) => +account.id === +currentId ? response.data.accountInfo : account))
        //     },
        //     error: () => setLoadingButton(false)
        // })
    }
    useEffect(() => {
        if (data) {
            setCurrentFirstId(router.query.account ? router.query.account : router.query.id)
            setCurrentFirstAccount(data.filter((account) => +account.id === +(router.query.account ? router.query.account : router.query.id))[0] || { error: "there is no account" })
            setAllFirstAccounts(data)
        }
    }, [currentFirstId, data, router])
    useEffect(() => {
        if (data) {
            setCurrentSecondId(router.query['second-account'] ? router.query['second-account'] : router.query.id)
            setCurrentSecondAccount(data.filter((account) => +account.id === +(router.query['second-account'] ? router.query['second-account'] : router.query.id))[0] || { error: "there is no account" })
            setAllSecondAccounts(data)
        }
    }, [currentSecondId, data, router])
    const handleChooseFirstIndexSlide = (i) => {
        router.push({
            pathname: router.asPath.split("?")[0],
            query: { account: data[i].id, 'second-account': router.query['second-account'], },

        }, undefined, { scroll: false })
        setCurrentFirstAccount(data[i])
        setCurrentFirstId(data[i].id)
    }
    const handleChooseSecondIndexSlide = (i) => {
        router.push({
            pathname: router.asPath.split("?")[0],
            query: { account: router.query.account, 'second-account': data[i].id },

        }, undefined, { scroll: false })
        setCurrentSecondAccount(data[i])
        setCurrentSecondId(data[i].id)
    }
    return <div className="lg:p-8  sm:p-4 p-3 bg-white dark:bg-dark-white rounded-lg md:rounded-xl">
        <div className="flex items-center justify-between mb-6">
            <div className="flex items-center gap-2">
                <div className=" icon-container">
                    <ArrangeHorizontal className="[color:rgb(var(--primary-color))] -400 lg:w-8 w-4 lg:h-8 h-4" />
                </div>
                <h1 className="block lg:text-3xl text-lg font-bold text-black dark:text-white">{t("convert_between_accounts")}</h1>
            </div>
            <Link href="/dashboard" >
                <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl"><ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
                </a>
            </Link>
        </div>
        <div className="max-w-full py-4 mx-auto ">
            {error ? <Error apiMessage={error} />
                : (!data || !currentFirstAccount || !currentSecondAccount) ? <div className="flex justify-center items-center min-h-[calc(100vh_-_20rem)]">< Loading /></div>
                    : <>
                        <Formik
                            validationSchema={Yup.object({
                                convert: validate.convert
                            })}
                            initialValues={{ convert: '' }}
                            onSubmit={onSubmit}
                        >
                            {({ }) => (
                                <Form>
                                    <div className="grid lg:grid-cols-10 grid-cols-5 gap:lg-16 gap-6 relative">
                                        <div className="col-span-5 ">
                                            {currentFirstAccount.error === "there is no account" ? <NoData text={t("sorry_the_account_is_not_present")} />
                                                :
                                                <div className=" mb-8 lg:mb-0 w-full lg:w-11/12 mx-auto">
                                                    <Slider2 data={allFirstAccounts} currentAccount={currentFirstAccount} type="demo" chooseSlide={allFirstAccounts.indexOf(currentFirstAccount)} handleChooseIndexSlide={handleChooseFirstIndexSlide} singleSlide />
                                                    <div className="px-4">
                                                        <h2 className="mb-2 text-lg text-black dark:text-white">{t("enter_the_transfer_amount")}</h2>
                                                        <CustomnBalance name="convert" />
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                        <div className='lg:h-full w-full lg:w-auto flex justify-center items-center lg:absolute col-span-5  lg:right-1/2 lg:top-1/2 transform lg:-translate-y-1/2 lg:translate-x-1/2 before:lg:top-0 before:lg:w-[1px] before:w-[calc(50%_-_1.5rem)] before:lg:h-[calc(50%_-_1.5rem)] before:h-[1px] before:bg-gray-400 before:absolute before:lg:right-1/2 before:right-0 before:transform before:lg:-translate-x-1/2 after:lg:bottom-0 after:lg:w-[1px] after:w-[calc(50%_-_1.5rem)] after:lg:h-[calc(50%_-_1.5rem)] after:h-[1px] after:bg-gray-400 after:absolute after:lg:right-1/2  after:lg:left-auto after:left-0  after:transform after:lg:-translate-x-1/2 gap-1'> {t('to')} <ArrowRight2
                                            size="16"
                                            className='transform rtl:lg:rotate-180 lg:rotate-0 rotate-90'
                                        /></div>

                                        <div className="col-span-5 ">
                                            {currentSecondAccount.error === "there is no account" ? <NoData text={t("sorry_the_account_is_not_present")} />
                                                :
                                                <div className=" mb-8 lg:mb-0 w-full lg:w-11/12	mx-auto rtl:mr-auto">
                                                    <Slider2 data={allSecondAccounts} currentAccount={currentSecondAccount} type="demo" chooseSlide={allSecondAccounts.indexOf(currentSecondAccount)} handleChooseIndexSlide={handleChooseSecondIndexSlide} singleSlide />
                                                </div>
                                            }
                                        </div>

                                    </div>
                                    <ButtonTheme as='button' type="submit" color="primary" block size="xs" className="mt-8 mx-auto w-[37.5rem] max-w-full" loading={loadingButton} >{t("convert_the_money")}</ButtonTheme>
                                </Form>
                            )}
                        </Formik>
                    </>

            }
        </div>
    </div >
}