import React, { useState, useEffect, useMemo } from 'react';
import Aside from "@/ui/Aside"
import TopNav from "@/ui/TopNav"
import Warring from "@/ui/Warring"
import useTranslation from 'next-translate/useTranslation'

import { Element, Personalcard, Refresh2, Star1, Setting4, Wallet3, Profile, LogoutCurve, ClipboardClose, ClipboardTick, ClipboardExport, CalendarSearch } from 'iconsax-react'
import TestRedux from 'components/ui/TestRedux'
function Layout({ children }) {
    const { t, lang } = useTranslation("aside");
    const [openAside, setOpenAside] = useState(false)
    const roots = [
        { title: t('control_board'), icon: <Element size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard", },
        { title: t('the_accounts'), icon: <Personalcard size="18" className="[color:rgb(var(--primary-color))] " />, href: "#", },
        {
            title: t('record'), icon: <Refresh2 size="18" className="[color:rgb(var(--primary-color))] " />, collapse: "/dashboard/record",
            list: [
                { title: t('closed_deals'), icon: <ClipboardClose size="18" className="text-inherit" />, href: "/dashboard/record/closed-deals" },
                { title: t('open_deals'), icon: <ClipboardTick size="18" className="text-inherit" />, href: "/dashboard/record/open-deals" },
                { title: t('deposit_and_clouds'), icon: <ClipboardExport size="18" className="text-inherit" />, href: "/dashboard/record/deposit-and-clouds" }
            ],
        },
        { title: t('promotion'), icon: <Star1 size="18" className="[color:rgb(var(--primary-color))] " />, href: "#", },
        {
            title: t('tools'), icon: <Setting4 size="18" className="[color:rgb(var(--primary-color))] " />, collapse: "/dashboard/tools",
            list: [
                { title: t('economic_calendar'), icon: <CalendarSearch size="18" className="text-inherit" />, href: "/dashboard/tools/economic-calendar" }
            ],
        },
        { title: t('money_management'), icon: <Wallet3 size="18" className="[color:rgb(var(--primary-color))] " />, href: "#", },
        { title: t('profile_personly'), icon: <Profile size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/personal/profile-personally", },

    ]
    return (
        <div className="grid grid-area-home">
            <Aside roots={roots} openAside={openAside} closeAsideHandler={() => setOpenAside(false)} />
            <TopNav openAsideHandler={()=>setOpenAside(!openAside)}/>
            <div className="py-8 bg-secondary dark:bg-dark-secondary grid-area-home-page rtl:rounded-tr-xl ltr:rounded-tl-xl">
                <div className="px-4 mx-auto container2">
                    <Warring />
            {/* <TestRedux></TestRedux> */}
                    {children}
                </div>
            </div>
        </div>
    )
}
export default Layout