// /** @type {import('next').NextConfig} */
// const nextConfig = {
//   reactStrictMode: true,
//   images: {
//     domains: ['localhost' , 'placehold.jp'],
//     deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
//   },
// }
const nextTranslate = require('next-translate')

module.exports = nextTranslate({
  
  basePath: '',
  images: {
    domains: ['localhost',"www.hululmfx.com","flagcdn.com"],
    deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
  },
  env: {
    host: 'https://www.hululmfx.com/api',
    hostImage: 'https://www.hululmfx.com/public/files',
    lg:1024,
    md:768,
    PUSHER_APP_ID:'YOUR_APP_ID',
    PUSHER_APP_KEY:'YOUR_APP_KEY',
    PUSHER_APP_SECRET:'YOUR_APP_SECRET',
    PUSHER_APP_CLUSTER:'YOUR_APP_CLUSTER',
    companyName : "حلول",
    NEXT_PUBLIC_GOOGLE_ANALYTICS: 'G-4E7YBCGLYC'
    
  },
})
// module.exports = nextConfig

